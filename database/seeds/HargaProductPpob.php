<?php

use Illuminate\Database\Seeder;
use App\DataHargaPpob;

class HargaProductPpob extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach ($variable = DataHargaPpob::get() as $key => $value) {
									DataHargaPpob::find($value->id)->update([
										'markup'=>200
									]);
								}
    }
}
