<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PeringkatBerita extends Model
{
    protected $fillable = [
        'berita_id','anggota_id'
    ];
}
