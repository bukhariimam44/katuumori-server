<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Keranjang extends Model
{
    protected $fillable = [
        'key_cookies','data_product_toko','jumlah'
    ];
    public function productId(){
        return $this->belongsTo('App\DataProductToko','data_product_toko');
    }
}
