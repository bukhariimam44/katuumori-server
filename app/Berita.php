<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Berita extends Model
{
    protected $fillable = [
        'id','judul','isi','kategori_id','tgl_posting','gambar', 'anggota_id','aktif'
    ];
    public function jumlahKomentar(){
        return $this->hasMany('App\Komentar','berita_id');
    }
    public function anggotaId(){
        return $this->belongsTo('App\Anggota','anggota_id');
    }
    public function jumlahSukai(){
        return $this->hasMany('App\PeringkatBerita','berita_id');
    }
}
