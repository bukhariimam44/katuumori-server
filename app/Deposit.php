<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Deposit extends Model
{
	protected $fillable = [
		'anggota_id','no_trx','tgl_trx','nominal','bank_id','status','admin_id','created_at','updated_at'
];
public function anggotaId(){
	return $this->belongsTo('App\Anggota','anggota_id');
}
public function bankId(){
	return $this->belongsTo('App\Bank','bank_id');
}
public function adminId(){
	return $this->belongsTo('App\Admin','admin_id');
}
}
