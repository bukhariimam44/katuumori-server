<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

use App\Admin;
use App\JenisKelamin;
use App\Anggota;
use App\Gallery;
use App\Berita;
use App\Komentar;
use App\Helpers\PropinsiCity;
use App\DataProductToko;
use App\Keranjang;
use App\Tentang;
use JavaScript;
use DB;
use Log;
use Image;
class LoginController extends Controller
{
    public function __construct()
    {
        $this->url=config('api-config')['url_rajaongkir'];
    }
    public function index()
    {
        return view('welcome');
    }
    public function tentang()
    {
      $data = Tentang::find(1);
        return view('tentang',compact('data'));
    }
    public function visi_misi()
    {
      $data = Tentang::find(2);
        return view('visi_misi',compact('data'));
    }
    public function galleri()
    {
        $datas = Gallery::where('aktif','yes')->get();
        return view('galleri',compact('datas'));
    }
    
    public function berita()
    {
      $datas = Berita::where('aktif','yes')->orderBy('id','DESC')->paginate(12);
      return view('berita',compact('datas'));
    }
    public function detail_berita($judul){
      $edit = str_replace('_',' ',$judul);
      $detail = Berita::where('judul',$edit)->first();
      return view('detail_berita',compact('detail'));
    }
    public function tambah_komntar(Request $request,$id){
      $message = [
        'nama.required'=>'Tidak boleh kosong',
        'no_hp.required'=>'Tidak boleh kosong',
        'no_hp.min'=>'Minimal 10 digit',
        'komentar.required'=>'Tidak boleh kosong',
        'komentar.min'=>'Minimal 3 digit',
      ];
      $this->validate($request, [
          'nama' => 'required',
          'no_hp' => 'required|min:10|numeric',
          'komentar' => 'required|min:3|string',
      ],$message );
      DB::beginTransaction();
      try {
        Komentar::create([
          'nama'=>$request->nama,
          'hp'=>$request->no_hp,
          'komentar'=>$request->komentar,
          'berita_id'=>$id,
          'foto'=>'user_komen.png',
          'aktif'=>'yes'
        ]);
      } catch (\Throwable $th) {
        Log::info('Gagal Komentar:'.$th->getMessage());
          DB::rollback();
          flash('Maaf! Komentar gagal, silahkan ulangi kembali.')->error();
          return redirect()->back();
      }
      DB::commit();
      flash('Komentar Berhasil')->important();
      return redirect()->back();
    }
    public function kontak()
    {
        return view('kontak');
				}
				public function getDaftar(){
					$jenkels = JenisKelamin::get();
					return view('auth.register',compact('jenkels'));
				}
				public function postDaftar(Request $request){
								$message = [
										'nama.required'=>'Tidak boleh kosong',
										'no_hp.required'=>'Tidak boleh kosong',
										'no_hp.unique'=>'Nomor Sudah digunakan',
										'tempat_lahir.required'=>'Tidak boleh kosong',
										'tgl_lahir.required'=>'Tidak boleh kosong',
										'jenis_kelamin.required'=>'Tidak boleh kosong',
										'foto.required'=>'Tidak boleh kosong',
										'foto.image'=>'Format Harus gambar',
										'foto.max'=>'Maksimal ukuran gambar 1 MB',
										'pekerjaan.required'=>'Tidak boleh kosong',
										'alamat.required'=>'Tidak boleh kosong',
										'ikut_arisan.required'=>'Tidak boleh kosong',
										'password.required'=>'Tidak boleh kosong',
										'password.min'=>'Minimal 6 Digit',
								];
						$this->validate($request, [
										'nama' => 'required|min:3|string',
										'no_hp' => 'required|min:10|numeric|unique:anggotas|unique:admins',
										'tempat_lahir' => 'required|min:3|string',
										'tgl_lahir' => 'required',
										'jenis_kelamin' => 'required|numeric',
										'alamat'=>'required',
										'ikut_arisan'=>'required',
										'foto' => 'required|image|mimes:jpeg,png,jpg|max:1000',
										'pekerjaan' => 'required',
										'kerja_di' => 'required',
										'password' => 'required|min:6|string'
						],$message );
						DB::beginTransaction();
						$tgl_lahir = date('Y-m-d', strtotime($request->tgl_lahir));
						try {
										$image = $request->file('foto');
										$imageName = $image->getClientOriginalName();
										$fileName = date('YmdHis')."_".$imageName;
										$directory = public_path('/images/anggota/');
										$imageUrl = $directory.$fileName;
										Image::make($image)->resize(200, 230)->save($imageUrl);

										$data = Anggota::create([
														'nama'=>$request->nama,
														'no_hp'=>$request->no_hp,
														'tempat_lahir'=>$request->tempat_lahir,
														'email'=>$request->email,
														'tgl_lahir'=>$tgl_lahir,
														'jenis_kelamin_id'=>$request->jenis_kelamin,
														'pekerjaan'=>$request->pekerjaan,
														'kerja_di'=>$request->kerja_di,
														'ikut_arisan'=>$request->ikut_arisan,
														'foto'=>$fileName,
														'alamat_lengkap'=>$request->alamat,
														'password'=>Hash::make($request->password),
														'aktif'=>'yes',
														'type'=>'anggota'
										]);
						} catch (\Throwable $th) {
										Log::info('Gagal Daftar:'.$th->getMessage());
										DB::rollback();
										flash('Maaf! Pendaftaran gagal, silahkan ulangi kembali.')->error();
										return redirect()->back();
						}
						DB::commit();
						if (Admin::where('no_hp',$request->no_hp)->first()) {
								if (Auth::guard('admin')->attempt(['no_hp' => $request->no_hp, 'password' => $request->password])) {
										flash('Selamat Datang '.Auth::guard('admin')->user()->nama.'...')->important();
										return redirect()->intended('/admin');
								}else {
										flash('Nomor Hp atau Kata Sandi Salah, silahkan ulangi kembali.')->error();
										return redirect()->route('masuk');
								}
						}elseif (Anggota::where('no_hp',$request->no_hp)->first()) {
								if (Auth::guard('anggota')->attempt(['no_hp' => $request->no_hp, 'password' => $request->password])) {
										flash('Selamat Datang '.Auth::guard('anggota')->user()->nama.'...')->important();
										return redirect()->intended('/anggota');
								}else {
										flash('Nomor Hp atau Kata Sandi Salah, silahkan ulangi kembali.')->error();
										return redirect()->route('masuk');
								}
						}else {
								flash('Nomor HP belum terdaftar. Silahkan Daftar dahulu')->error();
								return redirect()->route('daftar');
						}
				}
    public function getLogin()
    {
						if (Auth::guard('anggota')->viaRemember())
						{
							return redirect()->route('index');
						}
						return view('login');
    }
    public function postLogin(Request $request)
    {
						$message = [
								'no_hp.required'=>'Tidak boleh kosong',
								'no_hp.min'=>'Minimal 10 Digit',
								'no_hp.numeric'=>'Harus Angka',
								'password.required'=>'Tidak boleh kosong',
								'password.min'=>'Minimal 6 Digit',
						];
						$this->validate($request, [
								'no_hp' => 'required|min:10|numeric',
								'password' => 'required|min:6|string'
						],$message );

						$remember = ($request->has('remember')) ? true : false;
						if (Admin::where('no_hp',$request->no_hp)->first()) {
								if (Auth::guard('admin')->attempt(['no_hp' => $request->no_hp, 'password' => $request->password],$remember)) {
										flash('Selamat Datang '.Auth::guard('admin')->user()->nama.'...')->important();
										return redirect()->route('admin');
								}else {
										flash('Kata Sandi Salah, silahkan ulangi kembali.')->error();
										return redirect()->route('masuk');
								}
						}elseif (Anggota::where('no_hp',$request->no_hp)->first()) {
								if (Auth::guard('anggota')->attempt(['no_hp' => $request->no_hp, 'password' => $request->password],$remember)) {
										flash('Selamat Datang '.Auth::guard('anggota')->user()->nama.'...')->important();
										return redirect()->route('anggota');
								}else {
										flash('Kata Sandi Salah, silahkan ulangi kembali.')->error();
										return redirect()->route('masuk');
								}
						}elseif (User::where('no_hp',$request->no_hp)->first()) {
								if (Auth::guard('user')->attempt(['no_hp' => $request->no_hp, 'password' => $request->password],$remember)) {
										flash('Selamat Datang '.Auth::guard('user')->user()->nama.'...')->important();
										return redirect()->route('user');
								}else {
										flash('Kata Sandi Salah, silahkan ulangi kembali.')->error();
										return redirect()->route('masuk');
								}
						}else {
								flash('Nomor HP belum terdaftar. Silahkan Daftar dahulu')->error();
								return redirect()->route('daftar');
						}
    }
    public function logout($guards)
    {
      if ($guards == 'komen') {
        Auth::guard('komen')->logout();
        return redirect()->back();
      }else {
        Auth::guard($guards)->logout();
      }
      return redirect('/');
    }
    public function belanja(Request $request,$id){
      if (!isset($_COOKIE['key_cart'])) {
        setcookie('key_cart',md5(date('ymdhis')+rand(100,10000)),strtotime("+10 year"));
      }
      $datas = DataProductToko::where('kategori_product',$id)->where('open','yes')->get();
      return view('shop',compact('datas'));
    }
    public function detail_product($id){
      $datas = DataProductToko::find($id);
      if (!$datas) {
        return redirect()->route('belanja');
      }
      return view('detail_product',compact('datas'));
    }
    public function add_cart(Request $request){
      return $request->all();
    }
    public function keranjang(){
      if (isset($_COOKIE['key_cart'])) {
        $datas = Keranjang::where('key_cookies',$_COOKIE['key_cart'])->get();
        return view('keranjang',compact('datas'));
      }
      return redirect()->route('belanja');
    }
    public function cek_ongkir(Request $request){
    }
    public function propinsi(Request $request){
      // $request->province = 2;
      $response = Propinsicity::get($this->url.'province?id='.$request->province)->get();
      $resarr = json_decode($response,true);
      if ($resarr['rajaongkir']['status']['code'] == 200) {
        return response()->json([
          'code'=>200,
          'message'=>'Data Propinsi',
          'data'=>$resarr['rajaongkir']['results']
        ]);
      }
      return $resarr;
    }
    public function city(Request $request){
      $request->province = 22;
      $request->city = 68;
      $response = Propinsicity::get($this->url.'city?id='.$request->city.'&province='.$request->province)->get();
      $resarr = json_decode($response,true);
      if ($resarr['rajaongkir']['status']['code'] == 200) {
        return response()->json([
          'code'=>200,
          'message'=>'Data City',
          'data'=>$resarr['rajaongkir']['results']
        ]);
      }
      
      return $resarr;
    }
}
