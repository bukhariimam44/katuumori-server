<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Image;
use App\Gallery;
use App\AnggotaArisan;
use Illuminate\Support\Facades\Crypt;
use Log;
class PengurusController extends Controller
{
				public function __construct()
							{
											$this->middleware('pengurus');
											$this->url=config('api-config')['api_url'];
											$this->hsl = '';
							}
				public function gallery(Request $request){
					$message = [
									'gambar.required'=>'Tidak boleh kosong',
									'gambar.min'=>'Minimal 3 Digit',
									'kategori.required'=>'Tidak boleh kosong',
									'kategori.numeric'=>'Harus Angka',
							];
					$this->validate($request, [
									'gambar' => 'required|min:3',
									'kategori' => 'required|numeric',
					],$message );
					DB::beginTransaction();
					try {
									$image = $request->file('gambar');
									$imageName = $image->getClientOriginalName();
									$fileName = date('ymdHis')."_".$imageName;
									$directory = public_path('/images/gallery/');
									$imageUrl = $directory.$fileName;
									Image::make($image)->resize(1294, 857)->save($imageUrl);
									Gallery::create([
													'gambar'=>$fileName,
													'kategori'=>$request->kategori,
													'aktif'=>'yes'
									]);
					} catch (\Throwable $th) {
									Log::info('Gagal tambah gallery:'.$th->getMessage());
									DB::rollback();
									flash('Maaf! Tambah gallery gagal, silahkan ulangi kembali.')->error();
									return redirect()->back();
					}
					DB::commit();
					flash('Tambah gallery Berhasil')->important();
					return redirect()->back();
			}
			public function data_anggota_arisan(Request $request){
				if ($request->action == 'add') {
					$message = [
						'nama.required'=>'Tidak boleh kosong',
						'foto.image'=>'Harus Format Gambar',
						'foto.required'=>'Tidak boleh kosong',
						'status.required'=>'Tidak boleh kosong',
						'hp.required'=>'Tidak boleh kosong',
						'hp.numeric'=>'Harus Angka',
				];
		$this->validate($request, [
						'nama' => 'required|min:3|string',
						'hp' => 'required|numeric',
						'foto' => 'required|image',
						'status' => 'required|min:200',
		],$message );
					try {
						$data = AnggotaArisan::create([
							'nama'=>$request->nama,
							'hp'=>$request->no_hp,
							'foto'=>$fileName,
							'status'=>$request->status,
							'aktif'=>'yes',
							'admin'=>$request->user()->nama
						]);
					} catch (\Throwable $th) {
						Log::info('Gagal Tambah anggota:'.$th->getMessage());
						DB::rollback();
						flash('Maaf! Tambah Keluarga gagal, silahkan ulangi kembali.')->error();
						return redirect()->back();
					}
					DB::commit();
					flash('Tambah anggota Berhasil')->success();
					return redirect()->back();
				}elseif ($request->action =='edit') {
					$message = [
						'nama.required'=>'Tidak boleh kosong',
						'status.required'=>'Tidak boleh kosong',
						'hp.required'=>'Tidak boleh kosong',
						'hp.numeric'=>'Harus Angka',
				];
				$this->validate($request, [
						'nama' => 'required|min:3|string',
						'hp' => 'required|numeric',
						'status' => 'required|min:200',
				],$message );
					$decrypted = Crypt::decrypt($request->kode);
					$data = AnggotaArisan::find($decrypted)->update([
						'nama'=>$request->nama,
						'hp'=>$request->no_hp,
						'status'=>$request->status,
						'admin'=>$request->user()->nama
					]);
					if ($data) {
						flash('edit anggota Berhasil')->success();
						return redirect()->back();
					}
					flash('Maaf! edit gagal.')->error();
					return redirect()->back();
				}elseif ($request->action =='hapus') {
					$decrypted = Crypt::decrypt($request->kode);
					$data = AnggotaArisan::find($decrypted)->update([
						'aktif'=>'no',
						'admin'=>$request->user()->nama
					]);
					if ($data) {
						flash('edit anggota Berhasil')->success();
						return redirect()->back();
					}
					flash('Maaf! edit gagal.')->error();
					return redirect()->back();
				}
			

			}
}
