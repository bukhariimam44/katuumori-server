<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\PeringkatBerita;
use App\Keluarga;
use App\Anggota;
use App\AnggotaArisan;
use App\JenisKelamin;
use App\Berita;
use App\Komentar;
use App\DataHargaPpob;
use App\HistoryTransaksi;
use App\BukuSaldo;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;
use App\Helpers\Req;
use App\Helpers\Transaksi;
use App\Bank;
use App\Deposit;
use App\Toko;
use App\Sumbangan;
use App\DataProductToko;
use Auth;
use DB;
use Log;
use Image;
use JavaScript;
use Mail;

class AnggotaController extends Controller
{
    public function __construct()
    {
        $this->url=config('api-config')['api_url'];
    }
    public function index(){
        return view('welcome');
    }
    public function profil_saya(Request $request){
        if ($request->action == 'keluarga') {
            $message = [
                'nama.required'=>'Tidak boleh kosong',
                'no_hp.required'=>'Tidak boleh kosong',
                'tempat_lahir.required'=>'Tidak boleh kosong',
                'tgl_lahir.required'=>'Tidak boleh kosong',
                'jenis_kelamin.required'=>'Tidak boleh kosong',
                'status_keluarga.required'=>'Tidak boleh kosong',
                'foto.required'=>'Tidak boleh kosong',
                'foto.image'=>'Harus Format Gambar',
                'alamat.required'=>'Tidak boleh kosong',
                'alamat.min'=>'Minimal 10 Karakter',
              ];
            $this->validate($request, [
                'nama' => 'required|min:3|string',
                'no_hp' => 'required|min:10|numeric',
                'tempat_lahir' => 'required|min:3|string',
                'tgl_lahir' => 'required',
                'jenis_kelamin' => 'required|numeric',
                'status_keluarga' => 'required|numeric',
                'alamat'=>'required|min:10',
                'pekerjaan'=>'required',
                'foto' => 'required|image',
            ],$message );
            DB::beginTransaction();
            $tgl_lahir = date('Y-m-d', strtotime($request->tgl_lahir));
            try {
                $image = $request->file('foto');
                $imageName = $image->getClientOriginalName();
                $fileName = date('YmdHis')."_".$imageName;
                $directory = public_path('/images/keluarga/');
                $imageUrl = $directory.$fileName;
                Image::make($image)->resize(200, 230)->save($imageUrl);

                Keluarga::create([
                    'anggota_id'=>$request->user()->id,
                    'nama'=>$request->nama,
                    'no_hp'=>$request->no_hp,
                    'tempat_lahir'=>$request->tempat_lahir,
                    'tgl_lahir'=>$tgl_lahir,
                    'jenis_kelamin'=>$request->jenis_kelamin,
                    'pekerjaan'=>$request->pekerjaan,
                    'foto'=>$fileName,
                    'status_keluarga'=>$request->status_keluarga,
                    'alamat'=>$request->alamat,
                    'aktif'=>'yes',
                ]);
            } catch (\Throwable $th) {
                Log::info('Gagal Tambah Keluarga:'.$th->getMessage());
                DB::rollback();
                flash('Maaf! Tambah Keluarga gagal, silahkan ulangi kembali.')->error();
                return redirect()->back();
            }
            DB::commit();
            flash('Tambah Keluarga Berhasil')->success();
            return redirect()->back();
        }elseif($request->action == 'edit_password'){
									$message = [
													'ps_lm.min'=>'Password Lama Minimal 6 Karakter',
													'password.min'=>'Password Baru Minimal 6 Karakter',
													'password.confirmed'=>'Ulangi Password tidak sama',
									];
									$this->validate($request, [
												'ps_lm'  => 'required|min:6',
												'password'  => 'required|min:6|confirmed'
									],$message );
									$anggota = Anggota::where('no_hp',$request->user()->no_hp)->first();
									if ($anggota) {
										if (Hash::check($request->ps_lm, $anggota->password)) {
											$anggota->password = Hash::make($request->password);
											$anggota->update();
											flash('Ganti Password Berhasil')->success();
											return redirect()->back();
										}else {
											flash('Maaf! Password Lama Salah.')->error();
											return redirect()->back();
										}
									}
									flash('Maaf! Gagal ganti password.')->error();
									return redirect()->back();
								}elseif ($request->action == 'edit_profil') {
            $message = [
                'nama.required'=>'Tidak boleh kosong',
                'no_hp.required'=>'Tidak boleh kosong',
                'tempat_lahir.required'=>'Tidak boleh kosong',
                'tgl_lahir.required'=>'Tidak boleh kosong',
                'jenis_kelamin.required'=>'Tidak boleh kosong',
                'status_keluarga.required'=>'Tidak boleh kosong',
                'alamat.required'=>'Tidak boleh kosong',
                'alamat.min'=>'Minimal 10 Karakter',
              ];
            $this->validate($request, [
                'nama' => 'required|min:3|string',
                'no_hp' => 'required|min:10|numeric',
                'tempat_lahir' => 'required|min:3|string',
                'tgl_lahir' => 'required',
                'jenis_kelamin' => 'required|numeric',
                'alamat'=>'required|min:10',
                'pekerjaan'=>'required',
                'ids' => 'required|numeric',
            ],$message );
            DB::beginTransaction();
            $tgl_lahir = date('Y-m-d', strtotime($request->tgl_lahir));
            try {

                $edit = Anggota::find($request->user()->id);
                if ($request->foto) {
                    if ($edit->foto =='null' || $edit->foto =='') {
                        $image = $request->file('foto');
                        $imageName = $image->getClientOriginalName();
                        $fileName = date('ymdHis')."_".$imageName;
                        $edit->foto = $fileName;
                        $directory = public_path('/images/anggota/');
                        $imageUrl = $directory.$fileName;
                        Image::make($image)->resize(200, 230)->save($imageUrl);
                    }else {
                        $destination_foto = public_path('images/anggota/'.$edit->foto);
                        if(file_exists($destination_foto)){
                                    unlink($destination_foto);
                        }
                        $image = $request->file('foto');
                        $imageName = $image->getClientOriginalName();
                        $fileName = date('ymdHis')."_".$imageName;
                        $edit->foto = $fileName;
                        $directory = public_path('/images/anggota/');
                        $imageUrl = $directory.$fileName;
                        Image::make($image)->resize(200, 230)->save($imageUrl);
                    }
                }
                if ($request->email) {
                    $edit->email = $request->email;
                }
                $edit->nama = $request->nama;
                $edit->no_hp = $request->no_hp;
                $edit->tempat_lahir = $request->tempat_lahir;
                $edit->tgl_lahir = $tgl_lahir;
                $edit->jenis_kelamin_id = $request->jenis_kelamin;
                $edit->pekerjaan = $request->pekerjaan;
                $edit->alamat_lengkap = $request->alamat;
                $edit->update();
            } catch (\Throwable $th) {
                Log::info('Gagal Edit Profil:'.$th->getMessage());
                DB::rollback();
                flash('Maaf! Edit Profil gagal, silahkan ulangi kembali.')->error();
                return redirect()->back();
            }
            DB::commit();
            flash('Edit Profil Berhasil')->success();
            return redirect()->back();
        }elseif ($request->action == 'edit_keluarga') {
            $message = [
                'nama.required'=>'Tidak boleh kosong',
                'no_hp.required'=>'Tidak boleh kosong',
                'tempat_lahir.required'=>'Tidak boleh kosong',
                'tgl_lahir.required'=>'Tidak boleh kosong',
                'jenis_kelamin.required'=>'Tidak boleh kosong',
                'status_keluarga.required'=>'Tidak boleh kosong',
                'alamat.required'=>'Tidak boleh kosong',
                'alamat.min'=>'Minimal 10 Karakter',
              ];
            $this->validate($request, [
                'nama' => 'required|min:3|string',
                'no_hp' => 'required|min:10|numeric',
                'tempat_lahir' => 'required|min:3|string',
                'tgl_lahir' => 'required',
                'jenis_kelamin' => 'required|numeric',
                'alamat'=>'required|min:10',
                'status_keluarga.required'=>'Tidak boleh kosong',
                'pekerjaan'=>'required',
                'ids' => 'required|numeric',
            ],$message );
            DB::beginTransaction();
            $tgl_lahir = date('Y-m-d', strtotime($request->tgl_lahir));
            try {

                $edit = Keluarga::where('id',$request->ids)->where('anggota_id',$request->user()->id)->first();
                if ($request->foto) {
                    if ($edit->foto =='null' || $edit->foto =='') {
                        $image = $request->file('foto');
                        $imageName = $image->getClientOriginalName();
                        $fileName = date('ymdHis')."_".$imageName;
                        $edit->foto = $fileName;
                        $directory = public_path('/images/keluarga/');
                        $imageUrl = $directory.$fileName;
                        Image::make($image)->resize(200, 230)->save($imageUrl);
                    }else {
                        $destination_foto = public_path('images/keluarga/'.$edit->foto);
                        if(file_exists($destination_foto)){
                                    unlink($destination_foto);
                        }
                        $image = $request->file('foto');
                        $imageName = $image->getClientOriginalName();
                        $fileName = date('ymdHis')."_".$imageName;
                        $edit->foto = $fileName;
                        $directory = public_path('/images/keluarga/');
                        $imageUrl = $directory.$fileName;
                        Image::make($image)->resize(200, 230)->save($imageUrl);
                    }
                }
                if ($request->email) {
                    $edit->email = $request->email;
                }
                $edit->nama = $request->nama;
                $edit->no_hp = $request->no_hp;
                $edit->tempat_lahir = $request->tempat_lahir;
                $edit->tgl_lahir = $tgl_lahir;
                $edit->jenis_kelamin = $request->jenis_kelamin;
                $edit->status_keluarga = $request->status_keluarga;
                $edit->pekerjaan = $request->pekerjaan;
                $edit->alamat = $request->alamat;
                $edit->update();
            } catch (\Throwable $th) {
                Log::info('Gagal Edit Keluarga:'.$th->getMessage());
                DB::rollback();
                flash('Maaf! Edit Keluarga gagal, silahkan ulangi kembali.')->error();
                return redirect()->back();
            }
            DB::commit();
            flash('Edit Keluarga Berhasil')->success();
            return redirect()->back();
        }
        return view('anggota.profil');
    }
    public function like($id){
        $dt = PeringkatBerita::create([
            'berita_id'=>$id,
            'anggota_id'=>Auth::guard('anggota')->user()->id,
        ]);
        if ($dt) {
            return redirect()->back();
        }
        return redirect()->back();
    }
    public function hapus_keluarga($id){
        DB::beginTransaction();
        try {
            $update = Keluarga::where('id',$id)->where('anggota_id',Auth::guard('anggota')->user()->id)->first();
            $destination_foto = public_path('images/keluarga/'.$update->foto);
            if(file_exists($destination_foto)){
                        unlink($destination_foto);
            }
            $update->delete();
        } catch (\Throwable $th) {
            Log::info('Gagal Tambah Keluarga:'.$th->getMessage());
            DB::rollback();
            flash('Maaf! Hapus Keluarga gagal, silahkan ulangi kembali.')->error();
            return redirect()->back();
        }
        DB::commit();
        flash('Hapus Keluarga Berhasil')->success();
        return redirect()->back();
    }
    public function data_anggota_arisan(Request $request){
        $datas = AnggotaArisan::where('aktif','yes')->get();
        return view('anggota.data_anggota',compact('datas'));
    }
    public function detail_anggota($id){
        $datas = Anggota::find($id);
        return view('anggota.detail_anggota',compact('datas'));
    }
    public function form_berita(Request $request){
        if ($request->judul) {
            $message = [
                'judul.required'=>'Tidak boleh kosong',
                'gambar.image'=>'Harus Format Gambar',
                'gambar.required'=>'Tidak boleh kosong',
                'isi.required'=>'Tidak boleh kosong',
                'kategori.required'=>'Tidak boleh kosong',
                'kategori.numeric'=>'Harus Angka',
              ];
            $this->validate($request, [
                'judul' => 'required|min:3|string',
                'kategori' => 'required|numeric',
                'gambar' => 'required|image',
                'isi' => 'required|min:200',
            ],$message );
            DB::beginTransaction();
            try {
                $image = $request->file('gambar');
                $imageName = $image->getClientOriginalName();
                $fileName = date('ymdHis')."_".$imageName;
                $directory = public_path('/images/berita/');
                $imageUrl = $directory.$fileName;
                Image::make($image)->resize(1920, 1280)->save($imageUrl);

                Berita::create([
                    'judul'=>$request->judul,
                    'isi'=>$request->isi,
                    'tgl_posting'=>date('Y-m-d'),
                    'gambar'=>$fileName,
                    'kategori_id'=>$request->kategori,
                    'anggota_id'=>$request->user()->id,
                    'aktif'=>'yes'
                ]);
            } catch (\Throwable $th) {
                Log::info('Gagal Tambah Berita:'.$th->getMessage());
                DB::rollback();
                flash('Maaf! Tambah Berita gagal, silahkan ulangi kembali.')->error();
                return redirect()->back();
            }
            DB::commit();
            flash('Tambah Berita Berhasil')->success();
            return redirect()->route('berita');
        }
        return view('anggota.form_berita');
    }
    public function tambah_komentar(Request $request, $id){
        $message = [
            'komentar.required'=>'Tidak boleh kosong',
            'komentar.min'=>'Minimal 3 digit',
          ];
          $this->validate($request, [
              'komentar' => 'required|min:3|string',
          ],$message );
          DB::beginTransaction();
          try {
            Komentar::create([
              'nama'=>$request->user()->nama,
              'hp'=>$request->user()->no_hp,
              'komentar'=>$request->komentar,
              'berita_id'=>$id,
              'anggota_id'=>$request->user()->id,
              'aktif'=>'yes'
            ]);
          } catch (\Throwable $th) {
            Log::info('Gagal Komentar:'.$th->getMessage());
              DB::rollback();
              flash('Maaf! Komentar gagal, silahkan ulangi kembali.')->error();
              return redirect()->back();
          }
          DB::commit();
          flash('Komentar Berhasil')->success();
          return redirect()->back();
    }
    public function transaksi_pulsa(Request $request){
        JavaScript::put([
            'url_provider'=>route('provider'),
            'url_operator'=>route('operator'),
            'url_proses'=>route('proses')
		]);
        return view('anggota.transaksi_pulsa');
    }
    public function provider(Request $request){
        $datas = DataHargaPpob::where('operator_sub',$request['provider'])->where('status','normal')->orderBy('price','ASC')->get();
        return response()->json([
            'code'=>200,
            'data'=>$datas
        ]);
    }
    public function operator(Request $request){
        // return $request->all();
        $datas = DataHargaPpob::where('code',$request['code'])->where('status','normal')->first();
        return response()->json([
            'code'=>200,
            'data'=>$datas
        ]);
    }
    public function proses(Request $request){
        $message = [
            'code.required'=>'URL tdk boleh kosong',
            'no_hp.required'=>'Nama Product tdk boleh kosong',
        ];
        $response = array('response' => '', 'success'=>false);
        $validator = Validator::make($request->all(), [
            'no_hp'  => 'required|min:10',
            'code'  => 'required',
        ],$message);
        if ($validator->fails()) {
            $response['response'] = $validator->messages();
            Log::info('Gagal Proses Pulsa :'.$validator->messages());
            $respon = [
                'code'=>401,
                'message'=>'Gagal Proses Pulsa',
                'status'=>'gagal',
                'error'=>$response
            ];
            return response()->json($respon);
        }
        $no_trx = date('ymdhis').$request->user()->id;
        $params['code'] = $request->code;
        $params['no_hp'] = $request->no_hp;
        $params['no_trx'] = $no_trx;
								$product = DataHargaPpob::where('code',$params['code'])->where('status','normal')->first();
								if ($product) {
									if ($request->user()->saldo < ($product['price']+$product['markup'])) {
													return response()->json([
																	'code'=>400,
																	'message'=>'Saldo Anda Kurang',
																	'data'=>''
													]);
									}else {
													DB::beginTransaction();
													try {
																	$saldo_akhir = (int)$request->user()->saldo - ($product['price']+$product['markup']);
																	HistoryTransaksi::create([
																					'anggota'=>$request->user()->id,
																					'no_trx'=>$no_trx,
																					'data_harga_ppob_id'=>$product['id'],
																					'code'=>$product['code'],
																					'provider_sub'=>$product['provider_sub'],
																					'mutasi'=>'Debet',
																					'harga'=>(int)$product['price']+$product['markup'],
																					'untung'=>$product['markup'],
																					'nomor'=>$request->no_hp,
																					'ket'=>'.',
																					'status_ppob_id'=>1,
																					'saldo'=>$saldo_akhir
																	]);
																	BukuSaldo::create([
																					'anggota_id'=>$request->user()->id,
																					'no_trx'=>$no_trx,
																					'mutasi'=>'Debet',
																					'nominal'=>(int)$product['price']+$product['markup'],
																					'saldo_akhir'=>$saldo_akhir,
																					'keterangan'=>'Transaksi '.$product['description']
																	]);
																	$anggota = Anggota::find($request->user()->id)->update([
																					'saldo'=>$saldo_akhir
																	]);
																	$response = Req::post($params, $this->url.'proses-pulsa')->get();
													} catch (\Throwable $th) {
																	Log::info('Gagal proses Transaksi:'.$th->getMessage());
																	DB::rollback();
																	return response()->json([
																					'code'=>400,
																					'message'=>'Gagal ada kesalahan',
																					'data'=>$th
																	]);
													}
													if ($response['code'] == 200) {
																	DB::commit();
																	return response()->json([
																					'code'=>200,
																					'message'=>'Respon transaksi pembelian',
																					'data'=>$response['data']
																	]);
													}
													DB::rollback();
													return response()->json([
																	'code'=>400,
																	'message'=>$response['message'],
																	'data'=>$response['data']
													]);
									}
								}
								return response()->json([
												'code'=>400,
												'message'=>'Product tidak ditemukan',
								]);

        
    }
    public function laporan_pulsa(Request $request){
        $datas = HistoryTransaksi::where('provider_sub','REGULER')->where('anggota',$request->user()->id)->get();
        return view('anggota.laporan_pulsa',compact('datas'));
    }
    //DEPOSIT
    public function bank(Request $request){
								$bank = Bank::where('aktif','Yes')->get();
								$transaksi = Deposit::whereBetween('tgl_trx',[date('Y-m-01'),date('Y-m-t')])->where('anggota_id',$request->user()->id)->orderBy('created_at','DESC')->get();
								$dt = [];
								foreach ($transaksi as $key => $value) {
									$dt[]=[
										'id'=>$value['id'],
										'anggota_id'=>$value['anggota_id'],
										'no_trx'=>$value['no_trx'],
										'bank_id'=>$value->bankId,
										'nominal'=>$value['nominal'],
										'status'=>$value['status'],
										'tgl_trx'=>date('d-m-Y H:i', strtotime($value['created_at'])),
									];
								}
								$datas = [
									'bank'=>$bank,
									'transaksi'=>$dt
								];
        return response()->json($datas);
    }
    public function isi_saldo(Request $request){
        JavaScript::put([
            'url_bank'=>route('anggota-bank'),
            'url_proses_deposit'=>route('proses-deposit')
								]);

        return view('anggota.form_isi_saldo');
				}
				public function proses_deposit(Request $request){
					$total = (int)rand(100,500)+$request->user()->id + $request->nominal;
					$bank = Bank::find($request->bank_id);
					$add = Deposit::create([
						'anggota_id'=>$request->user()->id,
						'no_trx'=>date('ymdhis').$request->user()->id,
						'tgl_trx'=>date('Y-m-d'),
						'nominal'=>(int)$total,
						'bank_id'=>$request->bank_id,
						'status'=>'menunggu',
						'admin_id'=>0
					]);
					if ($add) {
						Mail::send('emails.deposit', ['nama' => $request->user()->nama,'id' => $request->user()->id,'bank' => $bank->nama, 'rekening' => $bank->no_rek, 'total' => $total], function ($message) use ($request)
						{
										$message->subject('Deposit Anggota');
										$message->from('daft4r5ek0lah@gmail.com', 'Katuumori');
										$message->to('bukhariimam44@gmail.com');
						});
						return response()->json([
							'code'=>200,
							'message'=>'Berhasil',
							'nominal'=>number_format($total),
							'bank'=>Bank::find($request->bank_id)
						]);
					}
					return response()->json([
						'code'=>400,
						'message'=>'Gagal'
					]);
				}
				public function laporan_deposit(Request $request){
					$datas = Deposit::whereBetween('tgl_trx',[date('Y-m-01'),date('Y-m-t')])->where('anggota_id',$request->user()->id)->get();
					return view('anggota.laporan_deposit',compact('datas'));
				}
				public function ganti_password(Request $request){
					return view('anggota.ganti_password');
				}
				public function cek_status_ppob(Request $request){
					$check = HistoryTransaksi::where('no_trx',$request->ids)->first();
					$params['trxid_api'] = $request->ids;
					$params['status'] = $check->status_ppob_id;
					$respon = Req::post($params, $this->url.'cek-status-ppob')->get();
					if ($respon['code'] == 200) {
						if ($respon['data']['status'] == 2 || $respon['data']['status'] == 3) {
							DB::beginTransaction();
							try {
								$update = HistoryTransaksi::where('no_trx',$request->ids)->first();
								$update->status_ppob_id = $respon['data']['status'];
								$update->sn_token = $respon['data']['sn'];
								$update->update();
								$cek = Anggota::find($update->anggota);
								$saldo_akhir = (int)$cek->saldo + $update['harga'];
								$cek->saldo = $saldo_akhir;
								$cek->update();
								BukuSaldo::create([
												'anggota_id'=>$update->anggota,
												'no_trx'=>date('ymdhis').$update->anggota,
												'mutasi'=>'Kredit',
												'nominal'=>$update['harga'],
												'saldo_akhir'=>$saldo_akhir,
												'keterangan'=>'Gagal Transaksi '.$request->ids
								]);
							} catch (\Throwable $th) {
									Log::info('Gagal Update transaksi:'.$th->getMessage());
									DB::rollback();
									return redirect()->back();
							}
							DB::commit();
							return redirect()->back();
						}else{
							DB::beginTransaction();
							try {
								$update = HistoryTransaksi::where('no_trx',$request->ids)->first();
								$update->status_ppob_id = $respon['data']['status'];
								$update->sn_token = $respon['data']['sn'];
								$update->update();
								$sumbangan = new Sumbangan;
								$sumbangan->tahun_bulan = date('Y-m');
								$sumbangan->trnsaksi_id = $request->ids;
								$sumbangan->nominal = 300;
								$sumbangan->aktif = 'yes';
								$sumbangan->anggota_id = $request->user()->id;
								$sumbangan->save();
							} catch (\Throwable $th) {
									Log::info('Gagal cek status transaksi:'.$th->getMessage());
									DB::rollback();
									return redirect()->back();
							}
							DB::commit();
							return redirect()->back();
						}
					}
					return redirect()->back();
				}
				public function buku_saldo(Request $request){
					$datas = BukuSaldo::where('anggota_id',$request->user()->id)->orderBy('created_at','DESC')->get();
					return view('anggota.buku_saldo',compact('datas'));
				}
				public function transaksi_pln_pra(Request $request){
					JavaScript::put([
											'url_provider'=>route('provider-pln'),
											'url_komisi'=>route('operator'),
											'url_proses'=>route('proses-pln')
					]);
					return view('anggota.transaksi_pln_pra');
				}
				public function provider_pln(Request $request){
					$datas = DataHargaPpob::where('operator_sub',$request['provider'])->where('status','normal')->orderBy('price','ASC')->get();
					return response()->json([
									'code'=>200,
									'data'=>$datas
					]);
				}
				public function proses_pln(Request $request){
					$message = [
									'code.required'=>'Code tdk boleh kosong',
									'no_hp.required'=>'No HP tdk boleh kosong',
									'no_meteran.required'=>'No meteran tdk boleh kosong',
					];
					$response = array('response' => '', 'success'=>false);
					$validator = Validator::make($request->all(), [
									'code'  => 'required',
									'no_hp'  => 'required|min:10',
									'no_meteran'  => 'required|min:10',
					],$message);
					
					if ($validator->fails()) {
									$response['response'] = $validator->messages();
									Log::info('Gagal Proses Token Listrik :'.$validator->messages());
									$respon = [
													'code'=>401,
													'message'=>'Gagal Proses Token Listrik',
													'status'=>'gagal',
													'error'=>$response
									];
									return response()->json($respon);
					}
					$no_trx = date('ymdhis').$request->user()->id;
					$params['code'] = $request->code;
					$params['no_hp'] = $request->no_hp;
					$params['no_meteran'] = $request->no_meteran;
					$params['no_trx'] = $no_trx;
					$product = DataHargaPpob::where('code',$params['code'])->where('status','normal')->first();
					if ($product) {
						if ($request->user()->saldo < ($product['price']+$product['markup'])) {
										return response()->json([
														'code'=>400,
														'message'=>'Saldo Anda Kurang',
														'data'=>''
										]);
						}else {
										DB::beginTransaction();
										try {
														$response = Req::post($params, $this->url.'proses-pln')->get();
														$saldo_akhir = $request->user()->saldo - ($product['price']+$product['markup']);
														HistoryTransaksi::create([
																		'anggota'=>$request->user()->id,
																		'no_trx'=>$no_trx,
																		'data_harga_ppob_id'=>$product['id'],
																		'code'=>$product['code'],
																		'provider_sub'=>$product['provider_sub'],
																		'mutasi'=>'Debet',
																		'harga'=>(int)$product['price']+$product['markup'],
																		'untung'=>$product['markup'],
																		'nomor'=>$request->no_meteran,
																		'ket'=>'.',
																		'status_ppob_id'=>1,
																		'saldo'=>$saldo_akhir
														]);
														BukuSaldo::create([
																		'anggota_id'=>$request->user()->id,
																		'no_trx'=>$no_trx,
																		'mutasi'=>'Debet',
																		'nominal'=>(int)$product['price']+$product['markup'],
																		'saldo_akhir'=>$saldo_akhir,
																		'keterangan'=>'Transaksi '.$product['description']
														]);
														$anggota = Anggota::find($request->user()->id)->update([
																		'saldo'=>$saldo_akhir
														]);
														
										} catch (\Throwable $th) {
														Log::info('Gagal proses Transaksi:'.$th->getMessage());
														DB::rollback();
														return response()->json([
																		'code'=>400,
																		'message'=>'Gagal ada kesalahan',
																		'data'=>$th
														]);
										}
										if ($response['code'] == 200) {
														DB::commit();
														return response()->json([
																		'code'=>200,
																		'message'=>'Respon transaksi pembelian',
																		'data'=>$response['data']
														]);
										}
										DB::rollback();
										return response()->json([
														'code'=>400,
														'message'=>$response['message'],
														'data'=>$response['data']
										]);
						}
					}
					return response()->json([
									'code'=>400,
									'message'=>'Product tidak ditemukan',
					]);

					
	}
	public function laporan_token_listrik(Request $request){
		$datas = HistoryTransaksi::where('provider_sub','PLN')->where('anggota',$request->user()->id)->get();
		return view('anggota.laporan_token_listrik',compact('datas'));
	}
	public function toko_saya(Request $request){
		$datas = Toko::where('anggota',$request->user()->id)->get();
		return view('anggota.toko_saya',compact('datas'));
	}
	public function detail_toko_saya(Request $request,$id){
		$toko = Toko::where('anggota',$request->user()->id)->where('id',$id)->first();
		$datas = DataProductToko::where('toko_id',$id)->where('anggota_id',$request->user()->id)->where('open','yes')->get();
		return view('anggota.detail_toko_saya',compact('datas','toko'));
	}
	public function sumbangan_terkumpul(Request $request){
		$datas = DB::table('sumbangans')
								->select('tahun_bulan', DB::raw('sum(sumbangans.nominal) as total'))
								->groupBy('tahun_bulan')
								->where('aktif','yes')
								->get();
		return view('anggota.sumbangan_terkumpul',compact('datas'));
	}
	public function panduan_transaksi(Request $request){
		$datas = [];
		return view('anggota.panduan_transaksi',compact('datas'));
	}
}
