<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class UmumController extends Controller
{
    public function index(){
					return view('shop.index');
				}
				public function produk(){
					return view('shop.semua_produk');
				}
				public function detail_produk($name){
					return view('shop.detail_produk');
				}
				public function blog(){
					return view('shop.blog');
				}
				public function detail_blog($name){
					return view('shop.detail_blog');
				}
}
