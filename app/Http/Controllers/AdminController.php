<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\JenisKelamin;
use App\Anggota;
use Illuminate\Support\Facades\Hash;
use App\Gallery;
use App\Helpers\Req;
use App\Helpers\Transaksi;
use App\DataHargaPpob;
use App\Tentang;
use App\Deposit;
use App\BukuSaldo;
use App\Bank;
use DB;
use Log;
use Image;
use JavaScript;


class AdminController extends Controller
{
    public function __construct()
    {
        $this->url=config('api-config')['api_url'];
        $this->hsl = '';
    }
    public function index(){
        return view('welcome');
    }
    public function data_anggota($ikut){
        $jenkels = JenisKelamin::get();
        if ($ikut == 'ikut-arisan') {
            $i = 'Ya';
            $ikut = 'Ikut Arisan';
        }else{
            $i = 'Tidak';
            $ikut = 'Tidak Ikut Arisan';
        }
        $datas = Anggota::where('ikut_arisan',$i)->where('aktif','yes')->get();
        return view('admin.data_anggota',compact('jenkels','datas','ikut'));
    }
    public function tambah_anggota(Request $request){
        $message = [
            'nama.required'=>'Tidak boleh kosong',
            'no_hp.required'=>'Tidak boleh kosong',
            'tempat_lahir.required'=>'Tidak boleh kosong',
            'tgl_lahir.required'=>'Tidak boleh kosong',
            'jenis_kelamin.required'=>'Tidak boleh kosong',
            'foto.required'=>'Tidak boleh kosong',
            'alamat.required'=>'Tidak boleh kosong',
            'password.required'=>'Tidak boleh kosong',
            'password.min'=>'Minimal 6 Digit',
          ];
        $this->validate($request, [
            'nama' => 'required|min:3|string',
            'no_hp' => 'required|min:10|numeric',
            'tempat_lahir' => 'required|min:3|string',
            'tgl_lahir' => 'required',
            'jenis_kelamin' => 'required|numeric',
            'alamat'=>'required',
            'foto' => 'required',
            'password' => 'required|min:6|string'
        ],$message );
        DB::beginTransaction();
        $tgl_lahir = date('Y-m-d', strtotime($request->tgl_lahir));
        try {
            $image = $request->file('foto');
            $imageName = $image->getClientOriginalName();
            $fileName = date('YmdHis')."_".$imageName;
            $directory = public_path('/images/anggota/');
            $imageUrl = $directory.$fileName;
            Image::make($image)->resize(180, 210)->save($imageUrl);

            $data = Anggota::create([
                'nama'=>$request->nama,
                'no_hp'=>$request->no_hp,
                'tempat_lahir'=>$request->tempat_lahir,
                'tgl_lahir'=>$tgl_lahir,
                'jenis_kelamin_id'=>$request->jenis_kelamin,
                'foto'=>$fileName,
                'alamat_lengkap'=>$request->alamat,
                'password'=>Hash::make($request->password),
                'aktif'=>'yes',
                'type'=>'anggota'
            ]);
        } catch (\Throwable $th) {
            Log::info('Gagal Daftar:'.$th->getMessage());
            DB::rollback();
            flash('Maaf! Pendaftaran gagal, silahkan ulangi kembali.')->error();
            return redirect()->back();
        }
        DB::commit();
        flash('Pendaftaran Berhasil')->important();
        return redirect()->back();

    }
    public function galleri(Request $request){
        $message = [
            'gambar.required'=>'Tidak boleh kosong',
            'gambar.min'=>'Minimal 3 Digit',
            'kategori.required'=>'Tidak boleh kosong',
            'kategori.numeric'=>'Harus Angka',
          ];
        $this->validate($request, [
            'gambar' => 'required|min:3',
            'kategori' => 'required|numeric',
        ],$message );
        DB::beginTransaction();
        try {
            $image = $request->file('gambar');
            $imageName = $image->getClientOriginalName();
            $fileName = date('ymdHis')."_".$imageName;
            $directory = public_path('/images/gallery/');
            $imageUrl = $directory.$fileName;
            Image::make($image)->resize(1294, 857)->save($imageUrl);
            Gallery::create([
                'gambar'=>$fileName,
                'kategori'=>$request->kategori,
                'aktif'=>'yes'
            ]);
        } catch (\Throwable $th) {
            Log::info('Gagal tambah gallery:'.$th->getMessage());
            DB::rollback();
            flash('Maaf! Tambah gallery gagal, silahkan ulangi kembali.')->error();
            return redirect()->back();
        }
        DB::commit();
        flash('Tambah gallery Berhasil')->important();
        return redirect()->back();
    }
    public function profil_saya(Request $request){
        return view('admin.profil_saya');
    }
    public function data_harga(Request $request,$id){
        if ($request->action == 'update') {
            if ($request->user()->no_hp != '082312543000') {
                flash('Belum ada ijin Update.')->error();
                return redirect()->back();
            }
            DB::beginTransaction();
            try {
                foreach ($request->code as $key => $data) {
                    DataHargaPpob::where('code',$data)->first()->update([
                        'markup'=>str_replace(",","",$request->markup[$key])
                    ]);
                }
            } catch (\Throwable $th) {
                Log::info('Gagal Update Jual:'.$th->getMessage());
                DB::rollback();
                flash('Gagal Update Harga Jual.')->error();
                $this->hsl = 'gagal';
            }
            if ($this->hsl == '') {
                DB::commit();
                flash('Berhasil Update Harga Jual')->success();
            }
            
        }
        $params = [];
        $response = Req::get($params, $this->url.'data-harga')->get();
        foreach ($response['data'] as $key => $value) {
            DataHargaPpob::where('code',$value['code'])->where('operator_sub',$value['operator_sub'])->first()->update([
                'price'=>$value['price'],
                'status'=>$value['status']
            ]);
        }
        if ($id == 'pulsa') {
            $product = 'Pulsa';
            $datas = DataHargaPpob::where('provider_sub','REGULER')->get();
        }elseif ($id == 'pln') {
            $product = 'Token Listrik';
            $datas = DataHargaPpob::where('provider_sub','PLN')->get();
        }elseif ($id == 'game') {
            $product = 'Voucher Game';
            $datas = DataHargaPpob::where('provider','GAME')->get();
        }elseif ($id == 'paket-internet') {
            $product = 'Paket Internet';
            $datas = DataHargaPpob::where('provider_sub','INTERNET')->get();
        }elseif ($id == 'paket-sms') {
            $product = 'Paket SMS';
            $datas = DataHargaPpob::where('provider_sub','SMS')->get();
        }elseif ($id == 'paket-telpon') {
            $product = 'Paket Telpon';
            $datas = DataHargaPpob::where('provider_sub','TELPON')->get();
        }elseif ($id == 'saldo-OVO') {
            $product = 'Saldo OVO';
            $datas = DataHargaPpob::where('provider','OVO')->orderBy('price','ASC')->get();
        }elseif ($id == 'saldo-Gojek') {
            $product = 'Saldo Gojek';
            $datas = DataHargaPpob::where('provider','GOJEK')->orderBy('price','ASC')->get();
        }elseif ($id == 'saldo-Gojek-Driver') {
            $product = 'Saldo Gojek Driver';
            $datas = DataHargaPpob::where('provider','GOJEK DRIVER')->orderBy('price','ASC')->get();
        }elseif ($id == 'saldo-Grab') {
            $product = 'Saldo Grab';
            $datas = DataHargaPpob::where('provider','GRAB')->orderBy('price','ASC')->get();
        }elseif ($id == 'saldo-Grab-Driver') {
            $product = 'Saldo Grab Driver';
            $datas = DataHargaPpob::where('provider','GRAB DRIVER')->orderBy('price','ASC')->get();
        }elseif ($id == 'saldo-Linkaja') {
            $product = 'Saldo LinkAja';
            $datas = DataHargaPpob::where('provider','LINKAJA')->orderBy('price','ASC')->get();
        }elseif ($id == 'saldo-Shopeepay') {
            $product = 'Saldo Shopeepay';
            $datas = DataHargaPpob::where('provider','SHOPEEPAY')->orderBy('price','ASC')->get();
        }elseif ($id == 'wifi-id') {
            $product = 'Wifi ID';
            $datas = DataHargaPpob::where('provider','SPEEDY')->orderBy('price','ASC')->get();
        }elseif ($id == 'google-play') {
            $product = 'Google Play';
            $datas = DataHargaPpob::where('provider','GOOGLE PLAY')->orderBy('price','ASC')->get();
        }
        $ids = $id;
        return view('admin.data_harga_ppob',compact('datas','ids','product'));
       
        
    }
    public function toko_online(Request $request){
        return view('admin.product_toko');
    }
    public function detail_product($id){
        return view('admin.detail_product');
    }
    public function edit_profil(Request $request){
        if ($request->isi) {
            $add = Tentang::find(1)->update([
                'isi'=>$request->isi,
                'menu'=>'profile'
            ]);
            if ($add) {
                flash('Berhasil Update')->success();
                return redirect()->route('tentang');
            }
            flash('Gagal Update.')->error();
            return redirect()->back();
        }
        $data = Tentang::find(1);
        // return $data;
        return view('admin.form_edit_profil',compact('data'));
    }
    public function edit_visi_misi(Request $request){
        if ($request->isi) {
									DB::beginTransaction();
									try {
										$datas = Tentang::find(2);
										$dom = new \DomDocument();
										libxml_use_internal_errors(true);
										$dom->loadHtml($request->isi, LIBXML_HTML_NOIMPLIED | LIBXML_HTML_NODEFDTD);
										
										$images = $dom->getElementsByTagName('img');
										foreach($images as $img){
											$src = $img->getAttribute('src');
									
											if(preg_match('/data:image/', $src)){
														preg_match('/data:image\/(?<mime>.*?)\;/', $src, $groups);
														$mimetype = $groups['mime'];
														if ($mimetype == 'png' || $mimetype == 'jpg' || $mimetype == 'jpeg' || $mimetype == 'gif') {
																	$filename = uniqid();
																	$filepath = "images/berita/$filename.$mimetype";
																	$image = \Intervention\Image\Facades\Image::make($src)
																					->encode($mimetype, 100)  // encode file to the specified mimetype
																					->save(public_path($filepath));
																	$new_src = asset($filepath);
																	$img->removeAttribute('src');
																	$img->setAttribute('src', $new_src);
														}else {
																	flash('Gagal! Format gambar harus PNG, JPG, JPEG.')->error();
																	return redirect()->back();
														}
														
											} // <!--endif
										} // <!--endforeach
								
										$datas->isi = $dom->saveHTML();
										$datas->update();
									} catch (\Throwable $th) {
										Log::info('Gagal edit visi-misi:'.$th->getMessage());
										DB::rollback();
										flash('Gagal Update visi-misi.')->error();
										return redirect()->back();
									}
									DB::commit();
									flash('Berhasil Update')->success();
									// return redirect()->route('visi-misi');
        }
        $data = Tentang::find(2);
        // return $data;
        return view('admin.edit_visi_misi',compact('data'));
				}
				public function permintaan_topup(Request $request){
					$from = date('Y-m-d');
					$to = date('Y-m-d');
					if ($request->action == 'cari') {
						$from = $request->from;
						$to = $request->to;
					}
					$datas = Deposit::whereBetween('tgl_trx',[$from,$to])->get();
					return view('admin.permintaan_topup',compact('datas'));
				}
				public function proses_topup(Request $request){
					$dep = Deposit::where('id',$request->ids)->where('status','menunggu')->first();
					if ($dep) {
						DB::beginTransaction();
						try {
							Deposit::find($dep->id)->update([
								'status'=>'berhasil',
								'admin_id'=>$request->user()->id
							]);
							$saldo = Anggota::find($dep->anggota_id);
							$saldo_akhir = (int)$saldo->saldo + $dep->nominal;
							$saldo->saldo = $saldo_akhir;
							$saldo->update();
							BukuSaldo::create([
								'anggota_id'=>$dep->anggota_id,
								'no_trx'=>$dep->no_trx,
								'mutasi'=>'Kredit',
								'nominal'=>$dep->nominal,
								'saldo_akhir'=>$saldo_akhir,
								'keterangan'=>'Tambah saldo'
							]);
						} catch (\Throwable $th) {
							Log::info('Gagal Proses Topup:'.$th->getMessage());
							DB::rollback();
							flash('Gagal')->error();
							return redirect()->back();
						}
						DB::commit();
						flash('Proses Topup Berhasil')->important();
						return redirect()->back();
					}
					flash('Proses Topup Gagal')->error();
					return redirect()->back();
				}
				public function batalkan_topup(Request $request){
					$dep = Deposit::where('id',$request->ids)->where('status','menunggu')->first();
					if ($dep) {
						$up = Deposit::find($dep->id)->update([
							'status'=>'batal',
							'admin_id'=>$request->user()->id
						]);
						if ($up) {
							flash('Pembatalan Berhasil')->important();
							return redirect()->back();
						}
						flash('Pembatalan Gagal')->error();
						return redirect()->back();
					}
					flash('Tidak dapat dibatalkan')->error();
					return redirect()->back();

				}
				public function pengaturan(Request $request){
					return view('admin.pengaturan');
				}
				public function topup_supplier(Request $request){
					JavaScript::put([
									'url_bank_supplier'=>route('admin-bank'),
									'url_proses_topup_supplier'=>route('admin-proses-deposit')
					]);
					return view('admin.topup_supplier');
				}
				public function bank(){
					$params = [];
					$banks = Req::get($params, $this->url.'data-bank')->get();
					return response()->json($banks);
				}
				public function proses_deposit(Request $request){
					$params['bank'] = $request->bank_id;
					$params['nominal'] = $request->nominal;
					// Log::info('RES SERVER BANK : '.$params['bank'].'-'.$params['nominal']);
					$respon = Req::post($params, $this->url.'proses-topup')->get();
					return response()->json($respon);
				}
				public function buku_saldo_supplier(Request $request){
					$params = [];
					$datas = Req::post($params,$this->url.'buku-saldo')->get();
					return view('admin.buku_saldo_supplier',compact('datas'));
				}
				public function topup_manual(Request $request){
					if ($request->bank) {
						DB::beginTransaction();
						try {
							$user = Anggota::where('no_hp',$request->hp)->first();
							if ($user) {
								$saldo_akhir = (int)$user->saldo + $request->nominal;
								$user->saldo = $saldo_akhir;
								$user->update();
								$deposit = new Deposit;
								$deposit->anggota_id = $user->id;
								$deposit->no_trx = date('YmdHis').$user->id;
								$deposit->tgl_trx = date('Y-m-d');
								$deposit->nominal = $request->nominal;
								$deposit->bank_id = $request->bank;
								$deposit->status = 'berhasil';
								$deposit->admin_id = $user->id;
								$deposit->save();
								$buku = new BukuSaldo;
								$buku->anggota_id = $user->id;
								$buku->no_trx = date('YmdHis').$user->id;
								$buku->mutasi = 'Kredit';
								$buku->nominal = $request->nominal;
								$buku->saldo_akhir = $saldo_akhir;
								$buku->keterangan = 'Tambah Saldo Manual';
								$buku->save();
							}
						} catch (\Throwable $th) {
							Log::info('Gagal Topup Manual:'.$th->getMessage());
							DB::rollback();
							return redirect()->back();
						}
						DB::commit();
						flash('Topup Manual Berhasil')->success();
						return redirect()->back();
						
					}
					$banks = Bank::where('aktif','Yes')->get();
					return view('admin.topup_manual',compact('banks'));
				}
}
