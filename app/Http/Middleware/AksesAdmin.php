<?php

namespace App\Http\Middleware;
use Illuminate\Support\Facades\Auth;


use Closure;

class AksesAdmin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (auth()->check() && $request->user()->type=='admin' && $request->user()->aktif =='yes' && Auth::guard('admin')->check()){
												// return redirect()->route('admin');
												return $next($request);
        }
        return redirect()->route('masuk');
    }
}
