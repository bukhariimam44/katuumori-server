<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;


class AksesUser
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (auth()->check() && $request->user()->type=='user' && $request->user()->is_active =='yes' && Auth::guard('user')->check()){
            return $next($request);
        }
        return redirect()->guest('/masuk');
    }
}
