<?php

namespace App\Http\Middleware;
use Illuminate\Support\Facades\Auth;

use Illuminate\Auth\Middleware\Authenticate as Middleware;

class Authenticate extends Middleware
{
    /**
     * Get the path the user should be redirected to when they are not authenticated.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return string
     */
    protected function redirectTo($request)
    {
        // if (! $request->expectsJson()) {
        //     return route('login');
        // }
        if (Auth::guard('admin')->check()) {
            return redirect('/admin');
          }else if (Auth::guard('anggota')->check()) {
            return redirect('/anggota');
          }else if (Auth::guard('user')->check()) {
            return redirect('/');
          }
    }
}
