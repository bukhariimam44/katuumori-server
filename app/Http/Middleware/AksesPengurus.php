<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
class AksesPengurus
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
					if (auth()->check() && $request->user()->type=='anggota' && $request->user()->sebagai=='pengurus' && $request->user()->aktif =='yes'){
									return $next($request);
					}
					return redirect()->route('masuk');
    }
}
