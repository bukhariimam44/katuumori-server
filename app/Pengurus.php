<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Pengurus extends Authenticatable
{
	use Notifiable;

	protected $table = 'penguruses';
	protected $guard = 'pengurus';
	protected $primaryKey = "id";
	
	protected $fillable = [
					'nama', 'no_hp','foto','email','type','aktif','sebagai','password','created_at','updated_at'
	];
	protected $hidden = [
		'password', 'remember_token',
];

/**
* The attributes that should be cast to native types.
*
* @var array
*/
protected $casts = [
		'email_verified_at' => 'datetime',
];
}
