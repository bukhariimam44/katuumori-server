<?php
namespace App\Helpers\API;

use JavaScript;
use Illuminate\Support\Facades\Auth; 
use Illuminate\Support\Facades\Log;

/**
 *
 */
class Send extends Api
{
  public function __construct($params, $url, $headers, $method)
  {
      parent::__construct();
      $this->url = $url;
      $this->method = $method;
      $params['url_agen'] = route('callback-server');
      $this->parameters = json_encode($params);
      $this->overWriteOptions = [
        'headers'=> $this->getHeaders($headers),
        'body'=>$this->parameters,
        'timeout'=> 120
      ];
      $this->getData();
  }

  private function getHeaders($headers){
    $head['Accept'] = 'application/json';
    $head['Content-Type'] = 'application/json';
    $head['Authorization'] = config('api-config')['api_token'];
    foreach ($headers as $key => $header) {
      $name = array_search($header, $headers);
      $head[$name] = $header;
    }
    return $head;
  }
}