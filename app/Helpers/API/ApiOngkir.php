<?php

namespace App\Helpers\API;

use GuzzleHttp\Client;
use GuzzleHttp\RequestOptions;
use GuzzleHttp\Exception\ClientException;
use Illuminate\Support\Facades\Log;

abstract class ApiOngkir
{
    protected $url;
    protected $userid;
    protected $key;
    protected $method;
    protected $data;
    protected $header;
    

    public function __construct()
    {
   
    }

    public function get(){
        return $this->data;
    }

    protected function getData(){
        try {
            $curl = curl_init();
            curl_setopt_array($curl, array(
                CURLOPT_URL => "http://api.rajaongkir.com/starter/cost",
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => "",
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 30,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => $method,
                CURLOPT_POSTFIELDS => "origin=$asal&destination=$tujuan&weight=$berat&courier=jne",
                CURLOPT_HTTPHEADER => $header
            ));
            $response = curl_exec($curl);
            $err = curl_error($curl);
            curl_close($curl);
            $resarr =json_decode($response,true); 
        } catch (ClientException $e) {
            $response = curl_exec($curl);
            $err = curl_error($curl);
            curl_close($curl);
            $resarr =json_decode($response,true); 
            $this->data = $response;
            return $this;
        }
        

        $this->data= $response;
        return $this;
    }
}