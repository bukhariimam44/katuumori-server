<?php
namespace App\Helpers\API;

use JavaScript;
use Illuminate\Support\Facades\Auth; 
use Illuminate\Support\Facades\Log;

/**
 *
 */
class Ongkir extends ApiOngkir
{
  public function __construct($params,$data,$method,$headers=[])
  {
      parent::__construct();
      $this->url=config('api-config')['url_rajaongkir'];
      $this->key = config('api-config')['key_rajaongkir'];
      $this->data = $data;
      $this->method = $method;
      $this->header = $this->getHeaders($headers);
      $this->getData();
  }

  private function getHeaders($headers){
     $head = array(
        'content-type: application/x-www-form-urlencoded',
        'key: '.$this->key, // lihat hasil autogenerate di member area
      );
    return $head;
  }
}