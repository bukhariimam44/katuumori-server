<?php

namespace App\Helpers;
use App\HistoryTransaksi;
use Log;

class Transaksi
{
    public static function post($datas){
        Log::info('DATAS : '.json_encode($datas));

        $add = HistoryTransaksi::create([
            'anggota'=>$datas['user_id'],
            'no_trx'=>$datas['no_trx'],
            'code'=>$datas['code'],
            'mutasi'=>'Debet',
            'harga'=>$datas['jual'],
            'untung'=>$datas['jual'] - $datas['price'],
            'ket'=>'.',
            'status_ppob_id'=>1,
            'saldo'=>$datas['saldo']
        ]);
        if ($add) {
            return $datas;
        }
        return error;
    }
}

