<?php

namespace App\Helpers;


use App\Helpers\API\Send;
use App\Helpers\API\Notification;

class Req
{
    public static function get($params, $url, $headers = []){
      return new Send($params, $url, $headers, 'GET');
    }

    public static function post($params, $url, $headers = []){
      return new Send($params, $url, $headers, 'POST');
    }

    public static function notif($params){
      return new Notification($params);
    }
}

