<?php
namespace App\Helpers\Rajaongkir;

use JavaScript;
use Illuminate\Support\Facades\Auth; 
use Illuminate\Support\Facades\Log;

/**
 *
 */
class Send extends Api
{
  public function __construct($url, $methods)
  {
    Log::info('URL 2= '.$url);
      parent::__construct();
      $this->url = $url;
      $this->key = config('api-config')['key_rajaongkir'];
      $this->methods = $methods;
      $this->getData();
  }
}