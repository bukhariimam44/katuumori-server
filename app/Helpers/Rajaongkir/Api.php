<?php

namespace App\Helpers\Rajaongkir;

use GuzzleHttp\Client;
use GuzzleHttp\RequestOptions;
use GuzzleHttp\Exception\ClientException;
use Illuminate\Support\Facades\Log;

abstract class Api
{
    protected $url;
    protected $key;
    protected $methods;
    protected $data;
    

    public function __construct()
    {

    }

    public function get(){
        return $this->data;
    }

    protected function getData(){
        Log::info('URL 3= '.$this->url);
            $curl = curl_init();
                curl_setopt_array($curl, array(
                CURLOPT_URL => $this->url,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => "",
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 30,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => $this->methods,
                CURLOPT_HTTPHEADER => array(
                    "key: ".$this->key
                ),
            ));

            $response = curl_exec($curl);
            $err = curl_error($curl);
            curl_close($curl); 
       
            if ($err) {
                $response = $err;
                $this->data= $response;
                return $this;
            }else {
                Log::info('$resarr berhasil = '.$response);
                $this->data= $response;
                return $this;
            }
        
    }
}