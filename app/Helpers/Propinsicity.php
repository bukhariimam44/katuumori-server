<?php

namespace App\Helpers;


use App\Helpers\Rajaongkir\Send;
use Log;

class Propinsicity
{
    public static function get($url, $methods='GET'){
      Log::info('URL = '.$url);
      return new Send($url,$methods);
    }

    public static function post($url,$methods='POST'){
      return new Send($url, $methods);
    }
}

