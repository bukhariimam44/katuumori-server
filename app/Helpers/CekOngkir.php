<?php

namespace App\Helpers;


use App\Helpers\API\Ongkir;
use App\Helpers\API\Propinsi;

use App\Helpers\API\Notification;

class CekOngkir
{
    public static function get($params,$data,$method='GET'){
        return new Ongkir($params,$data,$method);
      }
  
      public static function post($params,$data,$method='POST'){
        return new Ongkir($params,$data,$method);
      }
      public static function propinsi($params){
        return new Propinsi($params);
      }
}

