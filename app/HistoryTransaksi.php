<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HistoryTransaksi extends Model
{
    protected $fillable = [
        'id','anggota', 'no_trx','data_harga_ppob_id','code','provider_sub','mutasi','nomor','harga','untung','ket','sn_token','status_ppob_id','saldo'
    ];
    public function productId(){
        return $this->belongsTo('App\DataHargaPpob','data_harga_ppob_id');
    }
    public function statusPpob(){
        return $this->belongsTo('App\StatusPpob','status_ppob_id');
    }
}
