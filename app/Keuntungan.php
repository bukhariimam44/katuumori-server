<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Keuntungan extends Model
{
    protected $fillable = [
        'id','history_transaksi', 'nominal'
    ];
}
