<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Komentar extends Model
{
    protected $fillable = [
        'id','nama','hp','komentar','anggota_id','berita_id', 'created_at','updated_at','aktif','foto'
    ];
    public function anggotaKomenId(){
        return $this->belongsTo('App\Anggota','anggota_id');
    }
}
