<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AnggotaArisan extends Model
{
	protected $fillable = [
		'nama', 'hp','foto','status','aktif','admin'
];
}
