<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Sumbangan extends Model
{
	protected $fillable = [
		'trnsaksi_id','anggota_id','tahun_bulan','nominal','aktif'
];
}
