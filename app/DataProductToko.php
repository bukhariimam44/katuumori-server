<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DataProductToko extends Model
{
    protected $fillable = [
        'nama_barang','kode_barang', 'gambar','toko_id','anggota_id','kategori_product','stok','harga','sumbangan','keterangan','open'
    ];
    public function kategoriId(){
        return $this->BelongsTo('App\KategoriProduct','kategori_product');
    }
    public function tokoId(){
        return $this->BelongsTo('App\Toko','toko_id');
    }
}
