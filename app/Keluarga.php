<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Keluarga extends Model
{
    protected $fillable = [
        'anggota_id','nama','no_hp', 'tempat_lahir','tgl_lahir','jenis_kelamin','pekerjaan','foto','status_keluarga','alamat','aktif'
    ];
    public function statusId(){
        return $this->belongsTo('App\StatusKeluarga','status_keluarga');
    }
}
