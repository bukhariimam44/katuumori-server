<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Anggota extends Authenticatable
{
    use Notifiable;
    protected $guard = 'anggota';
    protected $primaryKey = "id";
    
    protected $fillable = [
        'nama', 'no_hp', 'jenis_kelamin_id','tempat_lahir','tgl_lahir','pekerjaan','kerja_di','ikut_arisan','alamat_lengkap','foto','type','saldo','aktif','password','pesan'
    ];
    public function keluargaId(){
        return $this->hasMany('App\Keluarga','anggota_id');
    }
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];
}
