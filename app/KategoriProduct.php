<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class KategoriProduct extends Model
{
    protected $fillable = [
        'name'
    ];
}
