<li class="{{setActive(['index','admin','anggota'])}}"><a href="{{route('index')}}">Beranda</a></li>
<li class="{{setActive(['tentang','visi-misi','galleri','berita','kontak'])}}"><a href="#">Tentang</a>
    <ul class="dropdown">
        <li><a href="{{route('tentang')}}">Profile</a></li>
        <li><a href="{{route('visi-misi')}}">Visi & Misi</a></li>
        <li><a href="{{route('galleri')}}">Galleri</a></li>
        <li><a href="{{route('berita')}}">Berita</a></li>
        <li><a href="{{route('kontak')}}">Kontak</a></li>
    </ul>
</li>
<li class="{{setActive(['belanja','umum-detail-product'])}}"><a href="#">Belanja</a>
    <ul class="dropdown">
        @foreach(App\KategoriProduct::get() as $key => $kt)
        <li><a href="{{route('belanja',$kt->id)}}">{{$kt->name}}</a></li>
        @endforeach
    </ul>
</li>
<li class="{{setActive(['admin-data-anggota','admin-detail-anggota'])}}"><a href="#">Data Anggota</a>
    <ul class="dropdown">
        <li><a href="{{route('admin-data-anggota','ikut-arisan')}}">Ikut Arisan</a></li>
        <li><a href="{{route('admin-data-anggota','tidak-ikut-arisan')}}">Tidak Ikut Arisan</a></li>
    </ul>
</li>
<li class="{{setActive('admin-data-harga')}}"><a href="#">Harga PPOB</a>
<ul class="dropdown">
    <li><a href="{{route('admin-data-harga','pulsa')}}">Pulsa Elektrik</a></li>
    <li><a href="{{route('admin-data-harga','pln')}}">Token Listrik</a></li>
    <li><a href="{{route('admin-data-harga','game')}}">Voucher Game</a></li>
    <li><a href="{{route('admin-data-harga','paket-internet')}}">Paket Internet</a></li>
    <li><a href="{{route('admin-data-harga','paket-sms')}}">Paket SMS</a></li>
    <li><a href="{{route('admin-data-harga','paket-telpon')}}">Paket Telpon</a></li>
    <li><a href="{{route('admin-data-harga','saldo-OVO')}}">Saldo OVO</a></li>
    <li><a href="{{route('admin-data-harga','saldo-Gojek')}}">Saldo Gojek</a></li>
    <li><a href="{{route('admin-data-harga','saldo-Gojek-Driver')}}">Saldo Gojek Driver</a></li>
    <li><a href="{{route('admin-data-harga','saldo-Grab')}}">Saldo Grab</a></li>
    <li><a href="{{route('admin-data-harga','saldo-Grab-Driver')}}">Saldo Grab Driver</a></li>
    <li><a href="{{route('admin-data-harga','saldo-Linkaja')}}">Saldo Link Aja</a></li>
    <li><a href="{{route('admin-data-harga','saldo-Shopeepay')}}">Saldo ShopeePay</a></li>
    <li><a href="{{route('admin-data-harga','wifi-id')}}">Wifi ID</a></li>
    <li><a href="{{route('admin-data-harga','google-play')}}">Google Play</a></li>
</ul>
</li>
<li class=""><a href="#">Toko Online</a>
<ul class="dropdown">
    <li><a href="{{route('product-toko-online')}}">Product Toko</a></li>
    <li><a href="">Pesanan Barang</a></li>
    <li><a href="">Laporan Penjualan</a></li>
    <li><a href="">Laporan Pembatalan</a></li>
</ul>
</li>
<li class=""><a href="#">Deposit Anggota</a>
<ul class="dropdown">
    <li><a href="{{route('admin-permintaan-topup')}}">Permintaan Topup</a></li>
    <li><a href="">Data Topup Anggota</a></li>
				<li><a href="">Buku Saldo Anggota</a></li>
				<li><a href="{{route('admin-topup-manual')}}">Topup Manual</a></li>
</ul>
</li>
<li class=""><a href="#">Data Supplier</a>
    <ul class="dropdown">
								<li><a href="{{route('admin-topup-supplier')}}">Topup Supplier</a></li>
        <li><a href="{{route('admin-buku-saldo-supplier')}}">Buku Saldo</a></li>
        <li><a href="">Data Transaksi</a></li>
    </ul>
</li>
<li><a href="{{route('admin-pengaturan')}}">Pengaturan</a></li>