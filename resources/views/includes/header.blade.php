<div id="wrapper" class="clearfix">
  <!-- preloader -->
  <!-- <div id="preloader">
    <div id="spinner">
      <div class="preloader-dot-loading">
        <div class="cssload-loading"><i></i><i></i><i></i><i></i></div>
      </div>
    </div>
    <div id="disable-preloader" class="btn btn-default btn-sm">Disable Preloader</div>
  </div>  -->
  
  <!-- Header -->
  <header id="header" class="header">
    <div class="header-top bg-theme-color-2 sm-text-center p-0">
      <div class="container">
        <div class="row">
          <div class="col-md-4">
            <div class="widget no-border m-0">
              <ul class="list-inline font-13 sm-text-center mt-5 hilang">
                <!-- <li>
                  <a class="text-white" href="#">FAQ</a>
                </li>
                <li class="text-white">|</li> -->
                @if(Auth::guard('admin')->check())
                <li>
                  <a class="text-white" href="{{route('admin-profil-saya')}}">{{Auth::guard('admin')->user()->nama}}</a>
                </li>
                <li class="text-white">|</li>
                <li>
                  <a class="text-white" href="{{url('/logout/admin')}}">Keluar</a>
                </li>
                @elseif(Auth::guard('anggota')->check())
                <li>
                  <a class="text-white" href="{{route('anggota-profil-saya')}}">Saldo Rp {{number_format(Auth::guard('anggota')->user()->saldo,0,0,".")}}</a>
                </li>
                <li class="text-white">|</li>
                <li>
                  <a class="text-white" href="{{url('/logout/anggota')}}">Keluar</a>
                </li>
                @else
                <li>
                  <a class="text-white" href="{{route('daftar')}}">Daftar</a>
                </li>
                <li class="text-white">|</li>
                <li>
                  <a class="text-white" href="{{route('masuk')}}">Login</a>
                </li>
                @endif
              </ul>
            </div>
          </div>
          <div class="col-md-8">
            <div class="widget m-0 pull-right sm-pull-none sm-text-center">
              <ul class="list-inline pull-right hilang">
                <li class="mb-0 pb-0">
                  <div class="top-dropdown-outer pt-5 pb-10">
                    <a class="top-cart-link has-dropdown text-white text-hover-theme-colored">
                      <?php  $keranjang =[];
                      if(isset($_COOKIE['key_cart'])){
                        $keranjang = App\Keranjang::where('key_cookies',$_COOKIE['key_cart'])->get();
                      }
                      ?>
                    <i class="fa fa-shopping-cart font-13"></i> ({{count($keranjang)}})
                    </a>
                    <ul class="dropdown hilang">
                      <li>
                        <!-- dropdown cart -->
                        <div class="dropdown-cart">
                          <table class="table cart-table-list table-responsive">
                            <tbody>
                              <?php if (isset($_COOKIE['key_cart'])) { $tot=0;?>
                              @foreach($keranjang as $key => $kr)
                              <?php $tot+=$kr->productId->harga*$kr->jumlah;?>
                              <tr>
                                <td><a href="#"><img alt="" src="{{asset('images/product/'.$kr->productId->gambar)}}"></a></td>
                                <td><a href="#"> {{$kr->productId->nama_barang}}</a></td>
                                <td>X{{$kr->jumlah}}</td>
                                <td>Rp {{number_format($kr->productId->harga*$kr->jumlah)}}</td>
                                <td><a class="close" href="#"><i class="fa fa-close font-13"></i></a></td>
                              </tr>
                              @endforeach
                              <?php } ?>
                              
                            </tbody>
                          </table>
                          <?php if (isset($_COOKIE['key_cart'])) { ?>
                          <div class="total-cart text-right">
                            <table class="table table-responsive">
                              <tbody>
                                <tr>
                                  <td>Belanja</td>
                                  <td>Rp {{number_format($tot)}}</td>
                                </tr>
                                <tr>
                                  <td>Ongkos Kirim</td>
                                  <td>Rp </td>
                                </tr>
                                <tr>
                                  <td>Total Order</td>
                                  <td>Rp </td>
                                </tr>
                              </tbody>
                            </table>
                          </div>
                          <?php } ?>
                          <div class="cart-btn text-right">
                              <a class="btn btn-theme-colored btn-xs" href="{{route('keranjang')}}"> Lihat Keranjang</a>
                              <a class="btn btn-dark btn-xs" href="shop-checkout.html"> Checkout</a>
                          </div>
                        </div>
                        <!-- dropdown cart ends -->
                      </li>
                    </ul>
                  </div>
                </li>
                <li class="mb-0 pb-0">
                  <div class="top-dropdown-outer pt-5 pb-10">
                    <a class="top-search-box has-dropdown text-white text-hover-theme-colored"><i class="fa fa-search font-13"></i> &nbsp;</a>
                    <ul class="dropdown hilang">
                      <li>
                        <div class="search-form-wrapper">
                          <form method="get" class="mt-10">
                            <input type="text" onfocus="if(this.value =='Enter your search') { this.value = ''; }" onblur="if(this.value == '') { this.value ='Enter your search'; }" value="Enter your search" id="searchinput" name="s" class="">
                            <label><input type="submit" name="submit" value=""></label>
                          </form>
                        </div>
                      </li>
                    </ul>
                  </div>
                </li>
              </ul>
            </div>
            <div class="widget no-border m-0 mr-15 pull-right flip sm-pull-none sm-text-center">
              <ul class="styled-icons icon-circled icon-sm pull-right flip sm-pull-none sm-text-center mt-sm-15 hilang">
                <li><a href="#"><i class="fa fa-facebook text-white"></i></a></li>
                <li><a href="#"><i class="fa fa-twitter text-white"></i></a></li>
                <!-- <li><a href="#"><i class="fa fa-google-plus text-white"></i></a></li> -->
                <li><a href="#"><i class="fa fa-instagram text-white"></i></a></li>
                <!-- <li><a href="#"><i class="fa fa-linkedin text-white"></i></a></li> -->
              </ul>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="header-middle p-0 bg-lightest xs-text-center">
      <div class="container pt-0 pb-0">
        <div class="row">
          <div class="col-xs-12 col-sm-4 col-md-5">
            <div class="widget no-border m-0">
              <a class="menuzord-brand pull-left flip xs-pull-center mb-15" href="javascript:void(0)"><img src="{{asset('images/katuumori.png')}}" alt=""></a>
            </div>
          </div>
          
          <div class="col-xs-12 col-sm-4 col-md-3 hidden">
            <div class="widget no-border pull-right sm-pull-none sm-text-center mt-10 mb-10 m-0">
              <ul class="list-inline hilang">
                <li><i class="fa fa-clock-o text-theme-colored font-36 mt-5 sm-display-block"></i></li>
                <li><a href="#" class="font-12 text-gray text-uppercase">We are open!</a>
                  <h5 class="font-13 text-black m-0"> Mon-Fri 8:00-16:00</h5>
                </li>
              </ul>
            </div>
          </div>
          <div class="col-xs-12 col-sm-4 col-md-4 pull-right">
            <div class="widget no-border pull-right sm-pull-none sm-text-center mt-10 mb-10 m-0">
              <ul class="list-inline hilang">
                <li><i class="fa fa-phone-square text-theme-colored font-36 mt-5 sm-display-block"></i></li>
                <li>
                  <a href="#" class="font-12 text-gray text-uppercase">Nomor Telpon</a>
                  <h5 class="font-14 m-0"> 0851-5503-0013</h5>
                </li>
              </ul>
            </div>
          </div>  
        </div>
      </div>
    </div>
    <div class="header-nav">
      <div class="header-nav-wrapper navbar-scrolltofixed bg-theme-colored border-bottom-theme-color-2-1px">
        <div class="container">
          <nav id="menuzord" class="menuzord bg-theme-colored pull-left flip menuzord-responsive">
            <ul class="menuzord-menu hilang">
              @if(Auth::guard('admin')->check())
              @include('includes.admin.menu')
              @elseif(Auth::guard('anggota')->check())
              @include('includes.anggota.menu')
              @else
              @include('includes.menu')
              @endif
            </ul>
            <ul class="pull-right flip hidden-sm hidden-xs hilang">
              <li>
                <!-- Modal: Book Now Starts -->
                @if(Auth::guard('admin')->check())
                <a class="btn btn-colored btn-flat bg-theme-color-2 text-white font-14 bs-modal-ajax-load mt-0 p-25 pr-15 pl-15" href="{{url('/logout/admin')}}">Keluar</a>
                @elseif(Auth::guard('anggota')->check())
                <a class="btn btn-colored btn-flat bg-theme-color-2 text-white font-14 bs-modal-ajax-load mt-0 p-25 pr-15 pl-15" href="{{url('/logout/anggota')}}">Keluar</a>
                @else
                <a class="btn btn-colored btn-flat bg-theme-color-2 text-white font-14 bs-modal-ajax-load mt-0 p-25 pr-15 pl-15" href="{{route('masuk')}}">Login</a>
                @endif
                <!-- Modal: Book Now End -->
              </li>
            </ul>
            <div id="top-search-bar" class="collapse">
              <div class="container">
                <form role="search" action="#" class="search_form_top" method="get">
                  <input type="text" placeholder="Type text and press Enter..." name="s" class="form-control" autocomplete="off">
                  <span class="search-close"><i class="fa fa-search"></i></span>
                </form>
              </div>
            </div>
          </nav>
        </div>
      </div>
    </div>
  </header>