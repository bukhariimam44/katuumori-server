<li class="{{setActive(['index','admin','anggota'])}}"><a href="{{route('index')}}">Beranda</a></li>
<li class="{{setActive(['tentang','visi-misi','galleri','berita','kontak'])}}"><a href="#">Tentang</a>
    <ul class="dropdown">
        <li><a href="{{route('tentang')}}">Profile</a></li>
        <li><a href="{{route('visi-misi')}}">Visi & Misi</a></li>
        <li><a href="{{route('galleri')}}">Galleri</a></li>
        <li><a href="{{route('berita')}}">Berita</a></li>
        <li><a href="{{route('kontak')}}">Kontak</a></li>
    </ul>
</li>
<li class="{{setActive(['shopping'])}}"><a href="{{route('shopping')}}">Belanja</a></li>
<!-- <li class="{{setActive(['belanja','umum-detail-product'])}}"><a href="#">Belanja</a>
    <ul class="dropdown">
        @foreach(App\KategoriProduct::get() as $key => $kt)
        <li><a href="{{route('belanja',$kt->id)}}">{{$kt->name}}</a></li>
        @endforeach
    </ul>
</li> -->
<li><a href="#">Transaksi</a>
<ul class="dropdown">
				<li><a href="{{route('transaksi-pulsa')}}">Transaksi Pulsa</a></li>
				<li><a href="{{route('transaksi-pln-pra')}}">Token Listrik</a></li>
    <li><a href="{{route('isi-saldo')}}">Isi Saldo</a></li>
				<!-- <li><a href="{{route('anggota-panduan-transaksi')}}">Panduan Transaksi</a></li> -->
</ul>  
</li>
<li class=""><a href="#">Laporan Transaksi</a>
<ul class="dropdown">
    <li><a href="{{route('laporan-pulsa')}}">Laporan Pulsa</a></li>
    <!-- <li><a href="">Laporan Paket Data</a></li>
    <li><a href="">Laporan Paket SMS</a></li> -->
				<li><a href="{{route('laporan-token-listrik')}}">Laporan Voucher Listrik</a></li>
				<li><a href="{{route('anggota-laporan-deposit')}}">Laporan Deposit</a></li>
    <!-- <li><a href="">Laporan Voucher Game</a></li>
    <li><a href="">Laporan Saldo Grab</a></li>
    <li><a href="">Laporan Saldo Gojek</a></li>
				<li><a href="">Laporan Saldo OVO</a></li> -->
				<li><a href="{{route('anggota-sumbangan-terkumpul')}}">Sumbangan Terkumpul</a></li>
</ul>
</li>
<li class="{{setActive(['anggota-data-anggota','anggota-detail-anggota'])}}"><a href="#">Data Anggota</a>
    <ul class="dropdown">
        <li><a href="{{route('anggota-data-anggota')}}">Anggota Arisan</a></li>
        <!-- <li><a href="{{route('anggota-data-anggota','tidak-ikut-arisan')}}">Tidak Ikut Arisan</a></li> -->
    </ul>
</li>
<li class="{{setActive(['profil','ganti-password'])}}"><a href="#">Akun</a>
    <ul class="dropdown">
								<li><a href="{{route('anggota-profil-saya')}}">Profil Saya</a></li>
								<li><a href="{{route('anggota-toko-saya')}}">Toko Saya</a></li>
        <li><a href="{{route('anggota-buku-saldo')}}">Buku Saldo</a></li>
    </ul>
</li>