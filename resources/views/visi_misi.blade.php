@extends('layouts.app_umum')
@section('css')
@endsection
@section('content')  
<div class="main-content">

    <!-- Section: inner-header -->
    <section id="pendek" class="inner-header divider parallax layer-overlay overlay-dark-5" data-bg-img="{{asset('images/slider/bg.jpg')}}">
      <div class="container pt-70 pb-20">
        <!-- Section Content -->
        <div class="section-content">
          <div class="row">
            <div class="col-md-12">
              <h2 class="title text-white">Visi & Misi</h2>
              <ol class="breadcrumb text-left text-black mt-10">
              <li><a href="{{url('/')}}">Beranda</a></li>
                <li class="active text-gray-silver">Visi & Misi</li>
              </ol>
            </div>
          </div>
        </div>
      </div>
    </section>

    <!-- Section: About -->
    <section class="">
      <div class="container">
        <div class="section-content">
          @if(Auth::guard('admin')->check())
          <a href="{{route('admin-edit-visi-misi')}}" class="btn btn-success">Edit</a>
          @endif
          <div class="row">
          @include('flash::message')
            <div class="col-md-12">
              {!!$data->isi!!}
            </div>
            
          </div>
        </div>
      </div>
    </section>
  </div>
@endsection
@section('js')
<script>
	window.onload = function() {
		$.browser.device = (/android|webos|iphone|ipad|ipod|blackberry|iemobile|opera mini/i.test(navigator.userAgent.toLowerCase()));
		console.log($.browser.device);
		if ($.browser.device) {
			console.log('Masuk : '+$.browser.device);
			$('.note-float-left').addClass('responsive');
			$('.note-float-left').css({"width":"100%", "height":"100%"});
		}
		
};
</script>

<script>
$('div.alert').not('.alert-important').delay(3000).fadeOut(350);
</script>
@endsection