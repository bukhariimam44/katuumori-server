@extends('layouts.app_summernote')
@section('css')
      <meta property="fb:app_id" content="136752056415945" />
      <meta property="og:type" content="article" />
      <meta property="og:url" content="{{url('berita/'.str_replace(' ','_',$detail->judul))}}" />
      <meta property="og:title" content="{{$detail->judul}}" />
      <meta property="og:image" content="{{asset(config('katuumori-config')['berita_dir'].$detail->gambar)}}" />
      <!-- <meta property="og:description" content="{!! $detail->isi !!}" /> -->
@endsection
@section('content')  
<div class="main-content">
    <!-- Section: inner-header -->
    <section id="pendek" class="inner-header divider parallax layer-overlay overlay-dark-5" data-bg-img="{{asset('images/slider/bg.jpg')}}">
      <div class="container pt-70 pb-20">
        <!-- Section Content -->
        <div class="section-content">
          <div class="row">
            <div class="col-md-12">
              <h2 class="title text-white">Berita</h2>
              <ol class="breadcrumb text-left text-black mt-10">
              <li><a href="{{url('/')}}">Beranda</a></li>
                <li class="active text-gray-silver">Berita</li>
              </ol>
            </div>
          </div>
        </div>
      </div>
    </section>

    <section>
      <div class="container">
        <div class="row">
          <div class="col-sm-12 col-md-3">
            <div class="sidebar sidebar-left mt-sm-30">
              <div class="widget">
                <h5 class="widget-title line-bottom">Cari Berita</h5>
                <div class="search-form">
                  <form>
                    <div class="input-group">
                      <input type="text" placeholder="Judul Berita / Isi" class="form-control search-input">
                      <span class="input-group-btn">
                      <button type="submit" class="btn search-button"><i class="fa fa-search"></i></button>
                      </span>
                    </div>
                  </form>
                </div>
              </div>
              <div class="widget">
                <h5 class="widget-title line-bottom">Kategori</h5>
                <div class="categories">
                  <ul class="list list-border angle-double-right">
                    <li><a href="#">Berita Terbaru</a></li>
                    <li><a href="#">Berita Terpopuler</a></li>
                    <li><a href="#">Berita Umum</a></li>
                    <li><a href="#">Berita Kesehatan</a></li>
                    <li><a href="#">Berita Bisnis</a></li>
                  </ul>
                </div>
              </div>
              {{--<div class="widget">
                <h5 class="widget-title line-bottom">Berita Terbaru</h5>
                <div class="latest-posts">
                  <article class="post media-post clearfix pb-0 mb-10">
                    <a class="post-thumb" href="#"><img src="https://placehold.it/75x75" alt=""></a>
                    <div class="post-right">
                      <h5 class="post-title mt-0"><a href="#">Sustainable Construction</a></h5>
                      <p>Lorem ipsum dolor sit amet adipisicing elit...</p>
                    </div>
                  </article>
                  <article class="post media-post clearfix pb-0 mb-10">
                    <a class="post-thumb" href="#"><img src="https://placehold.it/75x75" alt=""></a>
                    <div class="post-right">
                      <h5 class="post-title mt-0"><a href="#">Industrial Coatings</a></h5>
                      <p>Lorem ipsum dolor sit amet adipisicing elit...</p>
                    </div>
                  </article>
                  <article class="post media-post clearfix pb-0 mb-10">
                    <a class="post-thumb" href="#"><img src="https://placehold.it/75x75" alt=""></a>
                    <div class="post-right">
                      <h5 class="post-title mt-0"><a href="#">Storefront Installations</a></h5>
                      <p>Lorem ipsum dolor sit amet adipisicing elit...</p>
                    </div>
                  </article>
                </div>
              </div>--}}
              {{--<div class="widget">
                <h5 class="widget-title line-bottom">Photos from Flickr</h5>
                <div id="flickr-feed" class="clearfix">
                  <!-- Flickr Link -->
                  <script type="text/javascript" src="http://www.flickr.com/badge_code_v2.gne?count=9&amp;display=latest&amp;size=s&amp;layout=x&amp;source=user&amp;user=52617155@N08">
                  </script>
                </div>
              </div>--}}
            </div>
          </div>
          <div class="col-md-9 blog-pull-right">
            <div class="blog-posts single-post">
              <article class="post clearfix mb-0">
                <div class="entry-header">
                  <div class="post-thumb thumb"> <img src="{{asset(config('katuumori-config')['berita_dir'].$detail->gambar)}}" alt="" class="img-responsive img-fullwidth"> </div>
                </div>  
                <div class="entry-title pt-10 pl-15">
                  <h4><a class="text-uppercase" href="#">{{$detail->judul}}</a></h4>
                </div>
                <div class="entry-meta pl-15">
                  <ul class="list-inline">
                    <li>Posting: <span class="text-theme-color-2"> {{date('d-m-Y',strtotime($detail->created_at))}}</span></li>
                    <li>By: <span class="text-theme-color-2">{{$detail->anggotaId->nama}}</span></li>
                    @if(Auth::guard('anggota')->check())
                    <?php $cek = App\PeringkatBerita::where('berita_id',$detail->id)->where('anggota_id',Auth::guard('anggota')->user()->id)->get(); ?> 
                    <li><i class="fa @if(count($cek) > 0) fa-heart @else fa-heart-o @endif ml-5 mr-5"></i> {{count($detail->jumlahSukai)}} @if(count($cek) > 0) Suka @else <a href="{{route('like',$detail->id)}}">Sukai</a> @endif</li>
                    @endif
                    <li><i class="fa fa-comments-o ml-5 mr-5"></i> {{count($detail->jumlahKomentar)}} Komentar</li>
                  </ul>
                </div>
                <div class="entry-content mt-10">
                  <p class="mb-15">{!! $detail->isi !!}</p>
                  {{--<p class="mb-15">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer posuere erat a ante. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                  <blockquote class="theme-colored pt-20 pb-20">
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer posuere erat a ante.</p>
                    <footer>Someone famous in <cite title="Source Title">Source Title</cite></footer>
                  </blockquote>
                  <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna et sed aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p>--}}
                  <div class="mt-30 mb-0">
                    <h5 class="pull-left mt-10 mr-20 text-theme-color-2">Bagikan ke :</h5>
                    <ul class="styled-icons icon-circled m-0">
                      <li><a href="https://www.facebook.com/sharer/sharer.php?u={{url('berita/'.str_replace(' ','_',$detail->judul))}}&display=popup" data-bg-color="#3A5795"><i class="fa fa-facebook text-white"></i></a></li>
                      <li><a href="https://wa.me/?text={{url('berita/'.str_replace(' ','_',$detail->judul))}}" data-bg-color="#31bb47"><i class="fa fa-whatsapp text-white"></i></a></li>
                      <li><a href="https://twitter.com/intent/tweet?text={{$detail->judul}}&url={{url('berita/'.str_replace(' ','_',$detail->judul))}}" data-bg-color="#2aa9e0"><i class="fa fa-twitter text-white"></i></a></li>
                    </ul>
                  </div>
                </div>
              </article>
              {{--<div class="tagline p-0 pt-20 mt-5">
                <div class="row">
                  <div class="col-md-8">
                    <div class="tags">
                      <p class="mb-0"><i class="fa fa-tags text-theme-color-2"></i> <span>Tags:</span> Engine, Wheel, Oil, Brake</p>
                    </div>
                  </div>
                  <div class="col-md-4">
                    <div class="share text-right">
                    <!-- <a href="https://wa.me/?text=http://jorenvanhocht.be" class="social-button " id=""><span class="fa fa-whatsapp">DADA</span></a> -->
                      <a href="https://wa.me/?text=http://jorenvanhocht.be"></a><p><i class="fa fa-share-alt text-theme-color-2"></i> Share</p>
                    </div>
                  </div>
                </div>
              </div>--}}
              {{--<div class="author-details media-post">
                <a href="#" class="post-thumb mb-0 pull-left flip pr-20"><img class="img-thumbnail" alt="" src="http://placehold.it/110x110"></a>
                <div class="post-right">
                  <h5 class="post-title mt-0 mb-0"><a href="#" class="font-18">John Doe</a></h5>
                  <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna et sed aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
                  <ul class="styled-icons square-sm m-0">
                    <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                    <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                    <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                  </ul>
                </div>
                <div class="clearfix"></div>
              </div>--}}
              <div class="comments-area">
                <h5 class="comments-title">Komentar</h5>
                <ul class="comment-list">
                    @foreach($detail->jumlahKomentar as $key => $km)
                  <li>
                    <div class="media comment-author"> 
                      <a class="media-left pull-left flip" href="#">
                      @if($km->anggota_id == null || $km->anggota_id == '')
                        <img class="img-thumbnail" src="{{asset('images/user_komen.png')}}" width="45px" alt="">
                      </a>
                      <div class="media-body">
                        <h5 class="media-heading comment-heading">{{$km->nama}}:</h5>
                        @else
                        <img class="img-thumbnail" src="{{asset('images/anggota/'.$km->anggotaKomenId->foto)}}" width="45px" alt="">
                      </a>
                      <div class="media-body">
                        <h5 class="media-heading comment-heading">{{$km->anggotaKomenId->nama}}:</h5>
                        @endif
                        <div class="comment-date">{{date('d-m-Y',strtotime($km->created_at))}}</div>
                        <p>{!!$km->komentar!!}</p>
                         </div>
                    </div>
                  </li>
                  @endforeach
                  
                </ul>
              </div>
              <div class="comment-box">
                <div class="row">
                  <div class="col-sm-12">
                    <h5>Tinggalkan komentar</h5>
                    <div class="row">
                      @if(Auth::guard('anggota')->check())
                      <form role="form" action="{{route('anggota-detail-berita',$detail->id)}}" method="post" id="comment-form">
                      @else
                      <form role="form" action="{{route('detail-berita',$detail->id)}}" method="post" id="comment-form">
                      @endif
                      @csrf
                      <div class="row">
                      @if(Auth::guard('anggota')->check())
                      @else
                        <div class="col-sm-12">
                          <div class="form-group col-md-6">
                            <input type="text" class="form-control @error('nama') error @enderror" required name="nama" placeholder="Nama Lengkap">
                            @error('nama')
                              <p class="error">{{ $message }}</p>
                            @enderror
                          </div>
                          <div class="form-group col-md-6">
                            <input type="text" required class="form-control @error('no_hp') error @enderror" name="no_hp" placeholder="Nomor HP">
                            @error('no_hp')
                              <p class="error">{{ $message }}</p>
                            @enderror
                          </div>
                        </div>
                      @endif
                        <div class="col-sm-12 ">
                          <div class="form-group col-md-12">
                            <textarea class="form-control @error('komentar') error @enderror" required name="komentar"  placeholder="Isi Komentar" rows="7"></textarea>
                            @error('komentar')
                              <p class="error">{{ $message }}</p>
                            @enderror
                          </div>
                          <div class="form-group col-md-12">
                            <button type="submit" class="btn btn-dark btn-flat pull-right m-0" data-loading-text="Please wait...">Kirim</button>
                          </div>
                        </div>
                      </div>
                        
                      </form>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  </div>
@endsection
@section('js')
<script>
	window.onload = function() {
		$.browser.device = (/android|webos|iphone|ipad|ipod|blackberry|iemobile|opera mini/i.test(navigator.userAgent.toLowerCase()));
		console.log($.browser.device);
		if ($.browser.device) {
			console.log('Masuk : '+$.browser.device);
			$('.note-float-left').addClass('responsive');
			$('.note-float-left').css({"width":"100%", "height":"100%"});
			// var element = document.getElementsByClassName('note-float-left')
			// element.removeAttr("style");
			// element.removeProperty('width');
			// var element = document.getElementsByClassName('note-float-left'); 
			// element.style.width ="";
			// element.style.height=null;
		}
		
};
</script>
@endsection