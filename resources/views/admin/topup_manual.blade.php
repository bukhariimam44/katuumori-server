@extends('layouts.app')
@section('css')
        <!-- <script src="https://unpkg.com/vue@2.1.10/dist/vue.js"></script> -->
		<meta name="_token" id="token" value="{{csrf_token()}}">
    	<!-- <meta http-equiv="X-UA-Compatible" content="IE=edge"> -->
@endsection
@section('content')  
<div class="main-content" id="transaksi_pulsa">

    <!-- Section: inner-header -->
    <section id="pendek" class="inner-header divider parallax layer-overlay overlay-dark-5" data-bg-img="{{asset('images/slider/bg.jpg')}}">
      <div class="container pt-70 pb-20">
        <!-- Section Content -->
        <div class="section-content">
          <div class="row">
            <div class="col-md-12">
              <h2 class="title text-white">Topup Manual</h2>
              <ol class="breadcrumb text-left text-black mt-10">
              <li><a href="{{url('/')}}">Beranda</a></li>
                <li class="active text-gray-silver">Topup Manual</li>
              </ol>
            </div>
          </div>
        </div>
      </div>
    </section>

    <!-- Divider: Google Map -->
    <section>
    <div class="container">
      <div class="container-fluid p-10">
        <div class="row">
        <div class="panel panel-warning">
              <div class="panel-heading">
                <h3 class="panel-title">Form Topup Manual</h3>
              </div>
              @include('flash::message')
              <div class="panel-body"> 
															<form action="{{route('admin-topup-manual')}}" method="post">
															@csrf
															<div class="form-group col-md-4">
															<label for="">Rekening Bank</label>
															<select name="bank" id="" class="form-control">
															@foreach($banks as $bank)
															<option value="{{$bank->id}}">{{$bank->nama}}</option>
															@endforeach
															</select>
																</div>
																<div class="form-group col-md-4">
															<label for="">Nomor HP</label>
															<input type="text" name="hp" class="form-control">
																</div>
																<div class="form-group col-md-4">
															<label for="">Nominal</label>
															<input type="text" name="nominal" class="form-control">
																</div>
																<div class="form-group col-md-12">
															<button class="btn btn-primary form-control">PROSES</button>
																</div>
															</form>
                  
                </div>

            </div>
        
        </div>
      </div>
      </div>
    </section>
  </div>
@endsection
@section('js')

@endsection