@extends('layouts.app')
@section('css')
@endsection
@section('content')  
<div class="main-content">

    <!-- Section: inner-header -->
    <section class="inner-header divider parallax layer-overlay overlay-dark-5" data-bg-img="{{asset('images/slider/bg.jpg')}}">
      <div class="container pt-70 pb-20">
        <!-- Section Content -->
        <div class="section-content">
          <div class="row">
            <div class="col-md-12">
              <h2 class="title text-white">Profil Saya</h2>
              <ol class="breadcrumb text-left text-black mt-10">
                <li><a href="#">Beranda</a></li>
                <li class="active text-gray-silver">Profil Saya</li>
              </ol>
            </div>
          </div>
        </div>
      </div>
    </section>
      
    <!-- Section: Doctor Details -->
    <section class="">
      <div class="container">
        <div class="section-content">
          <div class="row">
            <div class="col-md-12 col-sm-4">
              <div class="doctor-thumb col-md-4 row">
                <img src="{{asset('images/anggota/'.Auth::guard('admin')->user()->foto)}}" alt="">
              </div>
              <div class="info p-20 bg-black-333">
                <h4 class="text-uppercase text-white">{{Auth::guard('admin')->user()->nama}}</h4>
                <ul class="angle-double-right m-20">
                  <li class="mt-0 text-gray-silver"><strong class="text-gray-lighter">Email</strong><br> {{Auth::guard('admin')->user()->email}}</li>
                  <li class="text-gray-silver"><strong class="text-gray-lighter">Nomor HP</strong><br> {{Auth::guard('admin')->user()->no_hp}}</li>
                </ul>
                <!-- <p class="text-gray-silver">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Labore atque officiis maxime suscipit expedita obcaecati nulla in ducimus.</p> -->
                
                <ul class="styled-icons icon-gray icon-circled icon-sm mt-15 mb-15">
                  <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                  <li><a href="#"><i class="fa fa-skype"></i></a></li>
                  <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                  <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                </ul>
                <a class="btn btn-info btn-flat mt-10 mb-sm-30" href="#">Edit Profile</a>
                <a class="btn btn-danger btn-flat mt-10 mb-sm-30" href="{{url('/logout/admin')}}">Logout</a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>

  </div>
@endsection