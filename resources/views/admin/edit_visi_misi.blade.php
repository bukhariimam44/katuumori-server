@extends('layouts.app_summernote')
@section('css')
@endsection
@section('content')  
<div class="main-content">

    <!-- Section: inner-header -->
    <section class="inner-header divider parallax layer-overlay overlay-dark-5" data-bg-img="{{asset('images/slider/bg.jpg')}}">
      <div class="container pt-70 pb-20">
        <!-- Section Content -->
        <div class="section-content">
          <div class="row">
            <div class="col-md-12">
              <h2 class="title text-white">Form Edit Visi & Misi</h2>
              <ol class="breadcrumb text-left text-black mt-10">
              <li><a href="{{url('/')}}">Beranda</a></li>
                <li class="active text-gray-silver">Form Edit Visi & Misi</li>
              </ol>
            </div>
          </div>
        </div>
      </div>
    </section>

    <!-- Divider: Google Map -->
    <section>
    <div class="container">
      <div class="container-fluid p-10">
        <div class="row">
        <div class="panel panel-success">
              <div class="panel-heading">
                <h3 class="panel-title">Form Edit Profile Katu'umori</h3>
              </div>
              @include('flash::message')
              <form action="{{route('admin-edit-visi-misi')}}" method="post" enctype="multipart/form-data">
              @csrf
              <div class="panel-body"> 
                  <div class="form-group col-md-12">
                    <textarea class="form-control @error('isi') error @enderror" id="summernote" name="isi">{{$data->isi}}</textarea>
                    @error('isi')
                      <p class="error">{{ $message }}</p>
                    @enderror
                  </div>
                  <div class="form-group col-md-12">
                    <button class="btn btn-primary form-control">Simpan Berita</button>
                  </div>
                
                </div>
              </form>
              
            </div>
        
        </div>
      </div>
      </div>
    </section>
  </div>
@endsection
@section('js')
@endsection