@extends('layouts.app')
@section('css')
        <!-- <script src="https://unpkg.com/vue@2.1.10/dist/vue.js"></script> -->
		<meta name="_token" id="token" value="{{csrf_token()}}">
    	<!-- <meta http-equiv="X-UA-Compatible" content="IE=edge"> -->
@endsection
@section('content')  
<div class="main-content" id="transaksi_pulsa">

    <!-- Section: inner-header -->
    <section id="pendek" class="inner-header divider parallax layer-overlay overlay-dark-5" :data-bg-img="images">
      <div class="container pt-70 pb-20">
        <!-- Section Content -->
        <div class="section-content">
          <div class="row">
            <div class="col-md-12">
              <h2 class="title text-white">Topup Supplier</h2>
              <ol class="breadcrumb text-left text-black mt-10">
              <li><a href="{{url('/')}}">Beranda</a></li>
                <li class="active text-gray-silver">Topup Supplier</li>
              </ol>
            </div>
          </div>
        </div>
      </div>
    </section>

    <!-- Divider: Google Map -->
    <section>
    <div class="container">
      <div class="container-fluid p-10">
        <div class="row">
        <div class="panel panel-success">
              <div class="panel-heading">
                <h3 class="panel-title">Form Topup Supplier</h3>
              </div>
              @include('flash::message')
              <div class="panel-body"> 
                  <div class="form-group col-md-6">
                    <label for="">Rekening Bank</label>
                    <select v-model="selected" class="form-control">
                        <option v-for="bank in banks" v-bind:value="bank.id">
                            @{{ bank.nama }}
                        </option>
                    </select>
                  </div>
                  <div class="form-group col-md-6">
                    <label for="">Nominal</label>
                    <input type="text" v-model="nominal" class="form-control">
                  </div>
    
                  <div class="form-group col-md-12">
                    <button class="btn btn-primary form-control" @click="checkForm()">Proses</button>
                  </div>
                <table class="table">
																<thead>
																<tr>
																<th>No.</th><th>Tgl Transaksi</th><th>No. Transaksi</th><th>Nominal Transafer</th><th>Status</th><th>Action</th>
																</tr>
																</thead>
																<tbody>
																<tr v-for="(transaksi,index) in transaksis" key="transaksi.id"> 
																<td v-if="transaksi.status === 'berhasil' " class="success">@{{index+1}}</td><td v-else-if="transaksi.status === 'batal' " class="danger">@{{index+1}}</td><td v-else>@{{index+1}}.</td>
																<td v-if="transaksi.status === 'berhasil' " class="success">@{{frontEndDateFormat(transaksi.tgl_trx)}}</td><td v-else-if="transaksi.status === 'batal' " class="danger">@{{frontEndDateFormat(transaksi.tgl_trx)}}</td><td v-else>@{{frontEndDateFormat(transaksi.tgl_trx)}}</td>
																<td v-if="transaksi.status === 'berhasil' " class="success">@{{transaksi.no_trx}}</td><td v-else-if="transaksi.status === 'batal' " class="danger">@{{transaksi.no_trx}}</td><td v-else>@{{transaksi.no_trx}}</td>
																<td v-if="transaksi.status === 'berhasil' " class="success">Rp @{{formatPrice(transaksi.nominal)}}</td><td v-else-if="transaksi.status === 'batal' " class="danger">Rp @{{formatPrice(transaksi.nominal)}}</td><td v-else>Rp @{{formatPrice(transaksi.nominal)}}</td>
																<td v-if="transaksi.status === 'berhasil' " class="success">@{{transaksi.status}}</td><td v-else-if="transaksi.status === 'batal' " class="danger">@{{transaksi.status}}</td><td v-else>@{{transaksi.status}}</td>
																<td v-if="transaksi.status === 'berhasil' " class="success"><button class="btn btn-success">Detail</button></td><td v-else><button class="btn btn-warning">Info</button></td><td v-else></td>
																</tr>
							
																</tbody>
																</table>
                </div>

            </div>
        
        </div>
      </div>
      </div>
    </section>
  </div>
@endsection
@section('js')
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script src="https://unpkg.com/axios/dist/axios.min.js"></script>
<script type="text/javascript" src="https://unpkg.com/vue@2.5.6/dist/vue.js"></script>
<script type="module">
  import Swal from 'https://unpkg.com/sweetalert/dist/sweetalert.min.js'
</script>
<script>
    var data_product = new Vue({
        el:'#transaksi_pulsa',
        data:{
            images : "<?php echo url('/images/slider/bg.jpg')?>",
												selected: '',
												transaksis:[],
            banks: [],
            nominal:'',
        },
        mounted() {
            this.load()
        },
        methods:{
            formatPrice(value) {
                let val = (value/1).toFixed(0).replace('.', ',')
                return val.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".")
												},
												frontEndDateFormat: function(date) {
													return moment(date, 'YYYY-MM-DD').format('DD-MM-YYYY');
												},
            async load(){
                await axios.get(window.url_bank_supplier).then((response) =>{
                    console.log('Berhasil'+JSON.stringify(response.data));
																				this.banks = response.data.bank;
																				this.transaksis = response.data.transaksi;
                    this.selected = response.data.bank[0].id;
                },(response)=>{
                    console.log('ERROR: '+response);
                });
            },
           async checkForm() {
              if (this.selected && this.nominal >= 10000) {
                let request = {bank_id : this.selected,nominal : this.nominal};
                request['token'] = document.querySelector('#token').getAttribute('value');
                await axios.post(window.url_proses_topup_supplier,request).then((response) =>{
                    console.log('Respon server'+JSON.stringify(response.data));
                    if (response.data.code === 200) {
																					this.load()
                      swal({
                        title: "INFO",
                        text: response.data.data,
                        icon: "warning",
                        buttons: ['Tidak','Ya'],
                        dangerMode: true,
                      })
                      .then((willDelete) => {
                        if (willDelete) {
                          swal("Terimakasih, Silahkan", {
                            icon: "success",
                          });
                        } else {
                          window.location.href = "#";
                        }
                      });
																						this.nominal =''
                    }else{
                      swal("Transaksi Gagal!", response.data.message, "error");
                    }
                    
                },(response)=>{
                    console.log('ERROR: '+response);
                    swal("Gagal!", response, "error");
                });
                
              }else{
															this.errors = [];
															if (!this.selected) {
																	swal("Cek Kembali!", "Bank harus dipilih!", "error");
															}
															if (this.nominal  < 500000) {
																	swal("Cek Kembali!", "Nominal Minimal Rp 500,000 ", "error");
															}
														}
              
              
            }

        }
    });
</script>
@endsection