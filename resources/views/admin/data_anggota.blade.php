@extends('layouts.app')
@section('css')
@endsection
@section('content')  
<div class="main-content">
    <!-- Section: inner-header -->
    <section class="inner-header divider parallax layer-overlay overlay-dark-5" data-bg-img="{{asset('images/slider/bg.jpg')}}">
      <div class="container pt-70 pb-20">
        <!-- Section Content -->
        <div class="section-content">
          <div class="row">
            <div class="col-md-12">
              <h2 class="title text-white">Data Anggota {{$ikut}}</h2>
              <ol class="breadcrumb text-left text-black mt-10">
                <li><a href="#">Home</a></li>
                <li class="active text-gray-silver">Data Anggota</li>
              </ol>
            </div>
          </div>
        </div>
      </div>
    </section>    
    <section>
      <div class="container">
 
        <div class="row">
          <div class="col-md-12 mt-40">
          <button class="btn btn-dark btn-theme-colored btn-sm" data-toggle="modal" data-target="#myModal2"><i class="fa fa-plus"></i> Tambah</button>
          <div class="modal fade" id="myModal2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                <div class="modal-dialog" role="document">
                  <div class="modal-content">
                    <div class="modal-header">
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                      <h4 class="modal-title" id="myModalLabel">Tambah Anggota</h4>
                    </div>
                    @include('flash::message')
                    <form action="{{route('admin-cari-data-anggota')}}" method="post" enctype="multipart/form-data">
                    @csrf
                    <div class="modal-body">
                      
                    <div class="row">
                <div class="form-group col-md-6">
                  <label for="form_name">Nama Lengkap</label>
                  <input name="nama" class="form-control @error('nama') error @enderror" type="text" value="{{old('nama')}}">
                  @error('nama')
                      <p class="error">{{ $message }}</p>
                    @enderror
                </div>
                <div class="form-group col-md-6">
                  <label>Nomor HP/WA</label>
                  <input name="no_hp" class="form-control @error('no_hp') error @enderror" type="text" value="{{old('no_hp')}}">
                  @error('no_hp')
                      <p class="error">{{ $message }}</p>
                    @enderror
                </div>
              </div>
              <div class="row">
                <div class="form-group col-md-6">
                  <label for="form_name">Email</label>
                  <input name="email" class="form-control @error('email') error @enderror" type="email" value="{{old('email')}}">
                  @error('email')
                      <p class="error">{{ $message }}</p>
                    @enderror
                </div>
                <div class="form-group col-md-6">
                  <label>Password</label>
                  <input name="password" class="form-control @error('password') error @enderror" type="text" value="{{old('password')}}">
                  @error('password')
                      <p class="error">{{ $message }}</p>
                    @enderror
                </div>
              </div>
              <div class="row">
                <div class="form-group col-md-6">
                  <label for="form_name">Jenis Kelamin</label>
                  <!-- <input name="jenis_kelamin" class="form-control" type="text"> -->
                  <select name="jenis_kelamin" class="form-control @error('jenis_kelamin') error @enderror" id="" value="{{old('jenis_kelamin')}}">
                    @foreach($jenkels as $jenkel)
                    <option value="{{$jenkel->id}}">{{$jenkel->name}}</option>
                    @endforeach
                  </select>
                </div>
                <div class="form-group col-md-6">
                  <label>Tempat Lahir</label>
                  <input name="tempat_lahir" class="form-control @error('tempat_lahir') error @enderror" type="text" value="{{old('tempat_lahir')}}">
                  @error('tempat_lahir')
                      <p class="error">{{ $message }}</p>
                    @enderror
                </div>
              </div>
              <div class="row"  id="example-component">
                <div class="form-group col-md-6" id="example-component">
                <label>Tanggal Lahir</label><br>
                    <!-- Datepicker Markup -->
                    <div class="input-group date">
                    <input type="text" class="form-control @error('tgl_lahir') error @enderror date-picker" name="tgl_lahir" value="{{old('tgl_lahir')}}">
                    <div class="input-group-addon">
                        <span class="glyphicon glyphicon-th"></span>
                    </div>
                    </div>
                    @error('tgl_lahir')
                      <p class="error">{{ $message }}</p>
                    @enderror
                    <!-- Datepicker Script -->
                    <script type="text/javascript">
                    $('#example-component .input-group.date').datepicker({
                    });
                    </script>
                </div>
                <div class="form-group col-md-6">
                  <label>Foto</label>
                  <input id="form_re_enter_password" name="foto"  class="form-control @error('foto') error @enderror" type="file"  value="{{old('foto')}}">
                  @error('foto')
                      <p class="error">{{ $message }}</p>
                    @enderror
                </div>
              </div>
              <div class="row">
                <div class="form-group col-md-12">
                  <label for="form_choose_username">Alamat Lengkap</label>
                  <textarea name="alamat" id="" cols="30" class="form-control @error('alamat') error @enderror" rows="6">{{old('alamat')}}</textarea>
                  @error('alamat')
                      <p class="error">{{ $message }}</p>
                    @enderror
                </div>
              </div>
                      
                    </div>
                    <div class="modal-footer">
                      <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                      <button type="submit" class="btn btn-primary text-white">S I M P A N</button>
                    </div>
                    </form>
                  </div>
                </div>
              </div>
            <hr>
            <!-- <p>Use <code>.table-striped</code> to add zebra-striping to any table row within the <code>&lt;tbody&gt;</code>.</p> -->
              <div data-example-id="striped-table" class="bs-example"> 
                  <table class="table table-striped"> 
                      <thead> 
                          <tr> 
                              <th>No.</th> 
                              <th>Foto</th>
                              <th>Nama</th> 
                              <th>No. HP</th> 
                              <th>Alamat</th>
                              <th>Action</th> 
                            </tr> 
                        </thead> 
                        <tbody> 
                          @foreach($datas as $key => $data)
                            <tr> 
                                <th scope="row">{{$key+1}}.</th> 
                                <td> <img src="{{asset('images/anggota/'.$data->foto)}}" width="40px" alt=""></td>
                                <td>{{$data->nama}}</td> 
                                <td>{{$data->no_hp}}</td> 
                                <td>{{$data->alamat_lengkap}}</td> 
                                <td><button type="button" class="btn btn-gray btn-theme-colored btn-sm" data-toggle="modal" data-target="#myModal{{$data->no_hp}}"> Detail <i class="fa fa-arrow-circle-right"></i></button>
</td>
                            </tr> 
                            @endforeach
                            
                        </tbody> 
                    </table>
                    @foreach($datas as $key => $dt)
                    <div class="modal fade" id="myModal{{$dt->no_hp}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                      <div class="modal-dialog" role="document">
                        <div class="modal-content">
                          <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title" id="myModalLabel">Detail Anggota {{$key}}</h4>
                          </div>
                          <div class="modal-body">
                            <h4>Text in a modal</h4>
                            <p>Duis mollis, est non commodo luctus, nisi erat porttitor ligula.</p>
                            <h4>Popover in a modal</h4>
                            <p>This <a data-content="And here's some amazing content. It's very engaging. right?" title="" class="btn btn-default popover-test" role="button" href="#" data-original-title="A Title">button</a> should trigger a popover on click.</p>
                            <h4>Tooltips in a modal</h4>
                            <p><a title="" class="tooltip-test" href="#" data-original-title="Tooltip">This link</a> and <a title="" class="tooltip-test" href="#" data-original-title="Tooltip">that link</a> should have tooltips on hover.</p>
                            <hr>
                            <h4>Overflowing text to show scroll behavior</h4>
                            <p>Cras mattis consectetur purus sit amet fermentum. Cras justo odio, dapibus ac facilisis in, egestas eget quam. Morbi leo risus, porta ac consectetur ac, vestibulum at eros.</p>
                            <p>Praesent commodo cursus magna, vel scelerisque nisl consectetur et. Vivamus sagittis lacus vel augue laoreet rutrum faucibus dolor auctor.</p>
                            <p>Aenean lacinia bibendum nulla sed consectetur. Praesent commodo cursus magna, vel scelerisque nisl consectetur et. Donec sed odio dui. Donec ullamcorper nulla non metus auctor fringilla.</p>
                            <p>Cras mattis consectetur purus sit amet fermentum. Cras justo odio, dapibus ac facilisis in, egestas eget quam. Morbi leo risus, porta ac consectetur ac, vestibulum at eros.</p>
                            <p>Praesent commodo cursus magna, vel scelerisque nisl consectetur et. Vivamus sagittis lacus vel augue laoreet rutrum faucibus dolor auctor.</p>
                            <p>Aenean lacinia bibendum nulla sed consectetur. Praesent commodo cursus magna, vel scelerisque nisl consectetur et. Donec sed odio dui. Donec ullamcorper nulla non metus auctor fringilla.</p>
                            <p>Cras mattis consectetur purus sit amet fermentum. Cras justo odio, dapibus ac facilisis in, egestas eget quam. Morbi leo risus, porta ac consectetur ac, vestibulum at eros.</p>
                            <p>Praesent commodo cursus magna, vel scelerisque nisl consectetur et. Vivamus sagittis lacus vel augue laoreet rutrum faucibus dolor auctor.</p>
                            <p>Aenean lacinia bibendum nulla sed consectetur. Praesent commodo cursus magna, vel scelerisque nisl consectetur et. Donec sed odio dui. Donec ullamcorper nulla non metus auctor fringilla.</p>
                          </div>
                          <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            <button type="button" class="btn btn-primary text-white">Save changes</button>
                          </div>
                        </div>
                      </div>
                    </div> 
                    @endforeach
                </div> 
          </div>
        </div>
      </div>
    </section> 
  </div>
@endsection