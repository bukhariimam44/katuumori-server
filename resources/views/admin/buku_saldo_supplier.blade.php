@extends('layouts.app')
@section('css')
        <!-- <script src="https://unpkg.com/vue@2.1.10/dist/vue.js"></script> -->
		<meta name="_token" id="token" value="{{csrf_token()}}">
    	<!-- <meta http-equiv="X-UA-Compatible" content="IE=edge"> -->
@endsection
@section('content')  
<div class="main-content" id="transaksi_pulsa">

    <!-- Section: inner-header -->
    <section id="pendek" class="inner-header divider parallax layer-overlay overlay-dark-5" :data-bg-img="images">
      <div class="container pt-70 pb-20">
        <!-- Section Content -->
        <div class="section-content">
          <div class="row">
            <div class="col-md-12">
              <h2 class="title text-white">Buku Saldo Supplier</h2>
              <ol class="breadcrumb text-left text-black mt-10">
              <li><a href="{{url('/')}}">Beranda</a></li>
                <li class="active text-gray-silver">Buku Saldo Supplier</li>
              </ol>
            </div>
          </div>
        </div>
      </div>
    </section>

    <!-- Divider: Google Map -->
    <section>
						<div class="container">
								<div class="container-fluid p-10">
										<div class="row">
													<div class="panel panel-primary">
																<div class="panel-heading">
																		<h3 class="panel-title">Form Topup Supplier</h3>
																</div>
																@include('flash::message')
																<div class="panel-body"> 
																				<table class="table">
																							<thead>
																								<th>No</th>
																								<th>Tgl. Trx</th>
																								<th>No. Trx</th>
																								<th>Debet</th>
																								<th>Kredit</th>
																								<th>Saldo</th>
																								<th>Keterangan</th>
																							</thead>
																							<tbody>
																								@foreach($datas['data'] as $key => $dt)
																								<tr>
																									<td>{{$key+1}}.</td>
																									<td>{{$dt['created_at']}}</td>
																									<td>{{$dt['no_trx']}}</td>
																									@if($dt['mutasi'] == 'Kredit')
																									<td>0</td>
																									<td>{{number_format($dt['nominal'])}}</td>
																									@else
																									<td>{{number_format($dt['nominal'])}}</td>
																									<td>0</td>
																									@endif
																									<td>{{number_format($dt['saldo_akhir'])}}</td>
																									<td>{{$dt['keterangan']}}</td>
																								</tr>
																								@endforeach
																							</tbody>
																				</table>
																</div>
										
												</div>
										</div>
								</div>
      </div>
    </section>
  </div>
@endsection
@section('js')
@endsection