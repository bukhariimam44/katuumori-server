@extends('layouts.app_summernote')
@section('css')
@endsection
@section('content')  
<div class="main-content">

    <!-- Section: inner-header -->
    <section class="inner-header divider parallax layer-overlay overlay-dark-5" data-bg-img="{{asset('images/slider/bg.jpg')}}">
      <div class="container pt-70 pb-20">
        <!-- Section Content -->
        <div class="section-content">
          <div class="row">
            <div class="col-md-12">
              <h2 class="title text-white">Permintaan Topup</h2>
              <ol class="breadcrumb text-left text-black mt-10">
              <li><a href="{{url('/')}}">Beranda</a></li>
                <li class="active text-gray-silver">Permintaan Topup</li>
              </ol>
            </div>
          </div>
        </div>
      </div>
    </section>
				<section>
      <div class="container">
 
        <div class="row">
          <div class="col-md-12 mt-0">
       
            <div data-example-id="striped-table" class="bs-example"> 
												@include('flash::message')
            <div class="panel panel-primary">
              <div class="panel-heading">
                <h3 class="panel-title">Permintaan Topup </h3>
              </div>
              <div class="panel-body">
              
                <div class="table-responsive">
                <table class="table table-striped"> 
                      <thead> 
                          <tr> 
                              <th>No.</th> 
                              <th>Tgl Transaksi</th>
																														<th>Anggota</th> 
																														<th>Rekening</th> 
                              <th>Nominal Transfer</th> 
                              <th>Status</th>
																														<th>Action</th>
                            </tr> 
                        </thead> 
                        <tbody> 
                        		@foreach($datas as $key => $data)
                            <tr class="@if($data->status == 'menunggu') active @elseif($data->status == 'berhasil') success @else danger  @endif"> 
                                <th scope="row">{{$key+1}}.</th> 
                                <td>{{date('d-m-Y',strtotime($data->tgl_trx))}}</td> 
																																<td>{{$data->anggotaId->nama}}</td> 
																																<td>{{$data->bankId->nama}}<br>{{$data->bankId->no_rek}} </td> 
                                <td>Rp {{number_format($data->nominal,0,0,".")}}</td> 
                                <td>{{$data->status}}</td> 
																																<td>@if($data->status == 'menunggu')<button type="button" class="btn btn-gray btn-theme-colored btn-sm" data-toggle="modal" data-target=".bs-example-modal-sm{{$data->id}}"> Proses <i class="fa fa-arrow-circle-right"></i></button> 
																																<button type="button" class="btn btn-danger btn-sm" onclick="event.preventDefault();
                document.getElementById('batalkan{{$data->id}}').submit();"> Batalkan <i class="fa fa-arrow-circle-right"></i></button> @else <button type="button" class="btn btn-success btn-sm" data-toggle="modal" data-target=".bs-example-modal-sm-admin{{$data->id}}"> Admin <i class="fa fa-arrow-circle-right"></i></button> @endif</td>
																												</tr>
																												<form action="{{route('admin-batalkan-topup')}}" method="post" id="batalkan{{$data->id}}">
																													<input type="hidden" name="ids" value="{{$data->id}}">
																													@csrf
																												</form> 
																										@endforeach
                        </tbody> 
                    </table>
                </div>
														@foreach($datas as $modal)
														<div class="modal fade bs-example-modal-sm-admin{{$modal->id}}" tabindex="-1" role="dialog">
                <div class="modal-dialog modal-sm">
                  <div class="modal-content">
                    <div class="modal-header">
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                      <h4 class="modal-title" id="myModalLabel3">Admin</h4>
                    </div>
                    <div class="modal-body">
																					@if($modal->admin_id != 0)
																						<p>Di Proses Oleh : {{$modal->adminId->nama}}</p>
																						@endif
																				</div>
                    <div class="modal-footer">
                      <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                  </div>
                </div>
														</div>

														<div class="modal fade bs-example-modal-sm{{$modal->id}}" tabindex="-1" role="dialog">
                <div class="modal-dialog modal-sm">
                  <div class="modal-content">
                    <div class="modal-header">
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                      <h4 class="modal-title" id="myModalLabel3">Proses Deposit</h4>
                    </div>
                    <div class="modal-body">
                      <p>Yakin nominal Rp {{number_format($modal->nominal,0,0,".")}} sudah masuk keRekening ?</p>
																				</div>
																				<form action="{{route('admin-proses-topup')}}" method="post" id="proses{{$modal->id}}">
																					<input type="hidden" name="ids" value="{{$modal->id}}">
																				@csrf
																				</form>
                    <div class="modal-footer">
                      <button type="button" class="btn btn-default" data-dismiss="modal">Tidak</button>
                      <button type="button" class="btn btn-primary text-white" onclick="event.preventDefault();
                document.getElementById('proses{{$modal->id}}').submit();">Ya</button>
                    </div>
                  </div>
                </div>
														</div>
														@endforeach
              </div>
            </div>
                  
                </div> 
          </div>
        </div>
      </div>
    </section> 
    
  </div>
@endsection
@section('js')
@endsection