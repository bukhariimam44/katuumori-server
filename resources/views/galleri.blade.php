@extends('layouts.app_umum')
@section('css')
@endsection
@section('content')  
<div class="main-content">

    <!-- Section: inner-header -->
    <section id="pendek" class="inner-header divider parallax layer-overlay overlay-dark-5" data-bg-img="{{asset('images/slider/bg.jpg')}}">
      <div class="container pt-70 pb-20">
        <!-- Section Content -->
        <div class="section-content">
          <div class="row">
            <div class="col-md-12">
              <h2 class="title text-white">Galleri</h2>
              <ol class="breadcrumb text-left text-black mt-10">
              <li><a href="{{url('/')}}">Beranda</a></li>
                <li class="active text-gray-silver">Galleri</li>
              </ol>
            </div>
          </div>
        </div>
      </div>
    </section>
    <section>
      <div class="container">
        <div class="row">
          <div class="col-md-12">
          @include('flash::message')
            <!-- Works Filter -->
            <div class="portfolio-filter font-alt align-center">
              <a href="#" class="active" data-filter="*">All</a>
              <a href="#select1" class="" data-filter=".select1">Kegiatan</a>
              <a href="#select2" class="" data-filter=".select2">Hiburan</a>
              <a href="#select3" class="" data-filter=".select3">Makan-Makan</a>
														@if(Auth::guard('admin')->check())
														<button type="button" class="btn btn-primary text-white pull-right" data-toggle="modal" data-target=".bs-example-modal-sm"><i class="fa fa-plus"></i> Tambah</button>
														@elseif(Auth::guard('anggota')->check())
															@if(Auth::guard('anggota')->user()->sebagai == 'pengurus')

															<button type="button" class="btn btn-primary text-white pull-right" data-toggle="modal" data-target=".bs-example-modal-sm"><i class="fa fa-plus"></i> Tambah</button>

															@endif
              @endif
            </div>
            <!-- End Works Filter -->
            <!-- MODAL -->
            <div class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog">
                <div class="modal-dialog modal-sm">
                  <div class="modal-content">
                    <div class="modal-header">
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                      <h4 class="modal-title" id="myModalLabel3">Tambah Galleri</h4>
																				</div>
																				@if(Auth::guard('admin')->check())
																				<form action="{{route('tambah-galleri')}}" method="post" enctype="multipart/form-data">
																				@elseif(Auth::guard('anggota')->check())
																					@if(Auth::guard('anggota')->user()->sebagai == 'pengurus')
																					<form action="{{route('pengurus-tambah-galleri')}}" method="post" enctype="multipart/form-data">
																					@endif
																				@endif
                    
                    @csrf
                    <div class="modal-body">
                      <div class="form-group">
                      <input type="file" class="form-control" name="gambar" required> 
                      </div>
                      <div class="form-group">
                      <select name="kategori" id="" class="form-control">
                        <option value="">Pilih</option>
                        <option value="1">Kegiatan</option>
                        <option value="2">Hiburan</option>
                        <option value="3">Makan - Makan</option>
                      </select>
                      </div>   
                      
                    </div>
                    <div class="modal-footer">
                      <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                      <button type="submit" class="btn btn-primary text-white">S I M P A N</button>
                    </div>
                    </form>
                  </div>
                </div>
              </div>
              <!-- END modal -->
            <!-- Portfolio Gallery Grid -->
            <div id="grid" class="gallery-isotope grid-3 gutter clearfix">
              @foreach($datas as $key => $data)
              <!-- Portfolio Item Start -->
              <div class="gallery-item select{{$data->kategori}}">
                <div class="thumb">
                    <img class="img-fullwidth" src="{{config('katuumori-config')['gallery_dir'].$data->gambar}}" alt="project">
                  <div class="overlay-shade"></div>
                  <div class="icons-holder">
                    <div class="icons-holder-inner">
                      <div class="styled-icons icon-sm icon-dark icon-circled icon-theme-colored">
                        <a data-lightbox="image" href="{{config('katuumori-config')['gallery_dir'].$data->gambar}}"><i class="fa fa-plus"></i></a>
                        <a href="#"><i class="fa fa-link"></i></a>
                      </div>
                    </div>
                  </div>
                  <a class="hover-link" data-lightbox="image" href="{{config('katuumori-config')['gallery_dir'].$data->gambar}}">View more</a>
                </div>
              </div>
              <!-- Portfolio Item End -->
              @endforeach

            </div>
            <!-- End Portfolio Gallery Grid -->
          </div>
        </div>
      </div>
    </section>
  </div>
@endsection
@section('js')
@endsection