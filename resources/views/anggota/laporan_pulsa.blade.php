@extends('layouts.app')
@section('css')
@endsection
@section('content')  
<div class="main-content">
    <!-- Section: inner-header -->
    <section id="pendek" class="inner-header divider parallax layer-overlay overlay-dark-5" data-bg-img="{{asset('images/slider/bg.jpg')}}">
      <div class="container pt-70 pb-20">
        <!-- Section Content -->
        <div class="section-content">
          <div class="row">
            <div class="col-md-12">
              <h2 class="title text-white">Laporan Pulsa</h2>
              <ol class="breadcrumb text-left text-black mt-10">
                <li><a href="#">Home</a></li>
                <li class="active text-gray-silver">Laporan Pulsa</li>
              </ol>
            </div>
          </div>
        </div>
      </div>
    </section>    
    <section>
      <div class="container">
 
        <div class="row">
          <div class="col-md-12 mt-0">
       
            <div data-example-id="striped-table" class="bs-example"> 
            <div class="panel panel-success">
              <div class="panel-heading">
                <h3 class="panel-title">Laporan Pulsa</h3>
              </div>
              <div class="panel-body">
                <div class="table-responsive">
                <table class="table table-striped"> 
                      <thead> 
                          <tr> 
                              <th>No.</th> 
                              <th>Tgl <br> No.Trx</th>
                              <th>Product <br>HP</th> 
                              <th>Harga<br>Nyumbang</th>
                              <th>Status</th> 
                              <th>Action</th>
                            </tr> 
                        </thead> 
                        <tbody> 
                         @foreach($datas as $key => $data)
                          <tr> 
                              <td>{{$key+1}}.</td> 
                              <td>{{$data->created_at}}<br>{{$data->no_trx}}</td> 
                              <td>{{$data->productId->description}} <br>{{$data->nomor}} @if($data->sn_token != null || $data->sn_token != '')<br>{{'SN:'.$data->sn_token}} @endif</td> 
                              <td>Rp {{number_format($data->harga)}} <br>Rp {{number_format($data->untung)}}</td> 
                              <td>{{$data->statusPpob->name}}</td> 
                              <td>@if($data->status_ppob_id == 1) <button class="btn btn-warning" onclick="event.preventDefault();
                document.getElementById('cek-status{{$data->id}}').submit();">Refresh</button> @else   <button class="btn btn-success">Detail</button> @endif</td> 
																										</tr>
																										<form action="{{route('anggota-cek-status-ppob')}}" method="post" id="cek-status{{$data->id}}">
																										@csrf
																										<input type="hidden" name="ids" value="{{$data->no_trx}}">
																										</form> 
                          @endforeach
                        </tbody> 
                    </table>
                </div>
              
              </div>
            </div>
                  
                </div> 
          </div>
        </div>
      </div>
    </section> 
  </div>
@endsection