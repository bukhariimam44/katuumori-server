@extends('layouts.app')
@section('css')
        <!-- <script src="https://unpkg.com/vue@2.1.10/dist/vue.js"></script> -->
		<meta name="_token" id="token" value="{{csrf_token()}}">
    	<!-- <meta http-equiv="X-UA-Compatible" content="IE=edge"> -->
@endsection
@section('content')  
<div class="main-content" id="transaksi_pulsa">

    <!-- Section: inner-header -->
    <section id="pendek" class="inner-header divider parallax layer-overlay overlay-dark-5" :data-bg-img="images">
      <div class="container pt-70 pb-20">
        <!-- Section Content -->
        <div class="section-content">
          <div class="row">
            <div class="col-md-12">
              <h2 class="title text-white">Isi Saldo</h2>
              <ol class="breadcrumb text-left text-black mt-10">
              <li><a href="{{url('/')}}">Beranda</a></li>
                <li class="active text-gray-silver">Isi Saldo</li>
              </ol>
            </div>
          </div>
        </div>
      </div>
    </section>

    <!-- Divider: Google Map -->
    <section>
    <div class="container">
      <div class="container-fluid p-10">
        <div class="row">
        <div class="panel panel-success">
              <div class="panel-heading">
                <h3 class="panel-title">Form Isi Saldo</h3>
              </div>
              @include('flash::message')
              <div class="panel-body"> 
                  <div class="form-group col-md-6">
                    <label for="">Rekening Bank</label>
                    <select v-model="selected" class="form-control">
                        <option v-for="bank in banks" v-bind:value="bank.id">
                            @{{ bank.nama }} -@{{ bank.no_rek }}, An. @{{ bank.atas_nama }}
                        </option>
                    </select>
                  </div>
                  <div class="form-group col-md-6">
                    <label for="">Nominal</label>
                    <input type="text" v-model="nominal" class="form-control">
                  </div>
    
                  <div class="form-group col-md-12">
                    <button class="btn btn-primary form-control proses" @click="checkForm()">Proses</button>
																		</div>
																		<!-- <div class="table-responsive"> -->
																		{{--<table class="table">
																			<thead>
																			<tr>
																			<th>No.</th>
																			<th>Tgl Transaksi</th>
																			<th>No. Transaksi</th>
																			<th>Ke Rekening</th>
																			<th>Nominal Transfer</th>
																			<th>Status</th>
																			</tr>
																			</thead>
																			<tbody>
																			<tr v-for="(transaksi,index) in transaksis" key="transaksi.id"> 
																			<td v-if="transaksi.status === 'berhasil' " class="success">@{{index+1}}</td><td v-else-if="transaksi.status === 'batal' " class="danger">@{{index+1}}</td><td v-else>@{{index+1}}.</td>
																			<td v-if="transaksi.status === 'berhasil' " class="success">@{{transaksi.tgl_trx}}</td><td v-else-if="transaksi.status === 'batal' " class="danger">@{{transaksi.tgl_trx}}</td><td v-else>@{{transaksi.tgl_trx}}</td>
																			<td v-if="transaksi.status === 'berhasil' " class="success">@{{transaksi.no_trx}}</td><td v-else-if="transaksi.status === 'batal' " class="danger">@{{transaksi.no_trx}}</td><td v-else>@{{transaksi.no_trx}}</td>
																			<td v-if="transaksi.status === 'berhasil' " class="success">@{{transaksi.bank_id.nama}} <br>@{{transaksi.bank_id.no_rek}}<br>An:@{{transaksi.bank_id.atas_nama}}</td><td v-else-if="transaksi.status === 'batal' " class="danger">@{{transaksi.bank_id.nama}}<br>@{{transaksi.bank_id.no_rek}}<br>An:@{{transaksi.bank_id.atas_nama}}</td><td v-else>@{{transaksi.bank_id.nama}}<br>@{{transaksi.bank_id.no_rek}}<br>An:@{{transaksi.bank_id.atas_nama}}</td>
																			<td v-if="transaksi.status === 'berhasil' " class="success">@{{formatPrice(transaksi.nominal)}}</td><td v-else-if="transaksi.status === 'batal' " class="danger">@{{formatPrice(transaksi.nominal)}}</td><td v-else>@{{formatPrice(transaksi.nominal)}}</td>
																			<td v-if="transaksi.status === 'berhasil' " class="success">@{{transaksi.status}}</td><td v-else-if="transaksi.status === 'batal' " class="danger">@{{transaksi.status}}</td><td v-else>@{{transaksi.status}}</td>
																			</tr>
										
																			</tbody>
																			</table>--}}
																		<!-- </div> -->
                
                </div>

            </div>
        
        </div>
      </div>
      </div>
    </section>
  </div>
@endsection
@section('js')
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script src="https://unpkg.com/axios/dist/axios.min.js"></script>
<script type="text/javascript" src="https://unpkg.com/vue@2.5.6/dist/vue.js"></script>
<!-- <script type="module">
  import Swal from 'https://unpkg.com/sweetalert/dist/sweetalert.min.js'
</script> -->
<script>
    var data_product = new Vue({
        el:'#transaksi_pulsa',
        data:{
            images : "<?php echo url('/images/slider/bg.jpg')?>",
												selected: '',
												transaksis:[],
            banks: [],
												nominal:'',
												visible: false
        },
        mounted() {
            this.load()
								},
        methods:{
            formatPrice(value) {
                // let val = (value/1).toFixed(0).replace('.', ',')
                return value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".")
            },
            async load(){
                await axios.get(window.url_bank).then((response) =>{
                    console.log('Berhasil'+JSON.stringify(response.data));
																				this.banks = response.data.bank;
																				this.transaksis = response.data.transaksi;
                    this.selected = response.data.bank[0].id;
                },(response)=>{
                    console.log('ERROR: '+response);
                });
            },
           async checkForm() {
              if (this.selected && this.nominal >= 10000) {
                let request = {bank_id : this.selected,nominal : this.nominal};
                request['token'] = document.querySelector('#token').getAttribute('value');
                await axios.post(window.url_proses_deposit,request).then((response) =>{
                    console.log('Berhasil'+JSON.stringify(response.data));
                    if (response.data.code === 200) {
																					this.load()
                      swal({
                        title: "INFO",
																								text: "Nominal yang harus ditransfer Rp "+response.data.nominal+"_ ke Nomor rekening : "+response.data.bank.no_rek+" ("+response.data.bank.nama+"), atas nama : "+response.data.bank.atas_nama,
                        icon: "warning",
                        Buttons: ['OK'],
                        dangerMode: true,
                      })
                      .then((willDelete) => {
                        if (willDelete) {
                          // swal("Silahkan Transfer, \n Terimakasih... ", {
                          //   icon: "success",
																										// });
																										window.location.href = "<?php echo route('anggota-laporan-deposit'); ?>";
                        } else {
                          window.location.href = "<?php echo route('anggota-laporan-deposit'); ?>";
                        }
                      });
                    }else{
                      swal("Transaksi Gagal!", response.data.message, "error");
                    }
                    
                },(response)=>{
                    console.log('ERROR: '+response);
                    swal("Gagal!", response, "error");
                });
                
              }
              
              this.errors = [];
              if (!this.selected) {
                swal("Cek Kembali!", "Bank harus dipilih!", "error");
              }
              if (this.nominal  < 10000) {
                swal("Cek Kembali!", "Nominal Minimal Rp 10,000 ", "error");
              }
            }

        }
    });
</script>
@endsection