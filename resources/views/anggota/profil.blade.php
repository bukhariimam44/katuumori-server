@extends('layouts.app')
@section('css')
@endsection
@section('content')  
<div class="main-content">

    <!-- Section: inner-header -->
    <section  id="pendek" class="inner-header divider parallax layer-overlay overlay-dark-5" data-bg-img="{{asset('images/slider/bg.jpg')}}">
      <div class="container pt-70 pb-20">
        <!-- Section Content -->
        <div class="section-content">
          <div class="row">
            <div class="col-md-12">
              <h2 class="title text-white">Profil Saya</h2>
              <ol class="breadcrumb text-left text-black mt-10">
                <li><a href="#">Beranda</a></li>
                <li class="active text-gray-silver">Profil Saya</li>
              </ol>
            </div>
          </div>
        </div>
      </div>
    </section>
      
    <!-- Section: Doctor Details -->
    <section class="">
      <div class="container">
        <div class="section-content">
          <div class="row">
            <div class="col-md-12 bg-black-333 col-sm-4"><br>
            @include('flash::message')
              <div class="doctor-thumb col-md-3 row">
                <img src="{{asset('images/anggota/'.Auth::guard('anggota')->user()->foto)}}" class="align-center" alt=""><br>
                <ul class="styled-icons icon-gray icon-circled icon-sm mt-15 mb-15">
                  <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                  <li><a href="#"><i class="fa fa-skype"></i></a></li>
                  <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                  <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                </ul>
              </div>
              <div class="info p-20 bg-black-333 col-md-9">
                <h4 class="text-uppercase text-white">{{Auth::guard('anggota')->user()->nama}}</h4>
                <ul class="angle-double-right m-10">
                  @if(Auth::guard('anggota')->user()->email != null)<li class="mt-0 text-gray-silver"><strong class="text-gray-lighter">Email</strong><br> {{Auth::guard('anggota')->user()->email}}</li>@endif
                  <li class="mt-0 text-gray-silver"><strong class="text-gray-lighter">Nomor HP</strong><br> {{Auth::guard('anggota')->user()->no_hp}}</li>
                  <li class="mt-0 text-gray-silver"><strong class="text-gray-lighter">Tempat,Tanggal Lahir</strong><br> <?php 
                  $biday = new DateTime(Auth::guard('anggota')->user()->tgl_lahir);
                  $today = new DateTime();
                  $diff = $today->diff($biday);
                  ?> {{Auth::guard('anggota')->user()->tempat_lahir}}, {{date('d-m-Y',strtotime(Auth::guard('anggota')->user()->tgl_lahir))}}  &nbsp;&nbsp;&nbsp;({{$diff->y}} Tahun)</li>
                  <li class="mt-0 text-gray-silver"><strong class="text-gray-lighter">Bidang Pekerjaan</strong><br> {{Auth::guard('anggota')->user()->pekerjaan}}</li>
                  @if(Auth::guard('anggota')->user()->kerja_di != null)<li class="mt-0 text-gray-silver"><strong class="text-gray-lighter">Bekerja di</strong><br> {{Auth::guard('anggota')->user()->kerja_di}}</li>@endif
                  <li class="mt-0 text-gray-silver"><strong class="text-gray-lighter">Alamat</strong><br> {{Auth::guard('anggota')->user()->alamat_lengkap}}</li>
                </ul>
                <a class="btn btn-info btn-flat mt-10 mb-sm-30" data-toggle="modal" data-target="#editprofil">Edit Profile</a>
                <a class="btn btn-danger btn-flat mt-10 mb-sm-30" data-toggle="modal" data-target=".editpassword" href="">Ganti Password</a>
																<div class="modal fade editpassword" tabindex="-1" role="dialog">
                <div class="modal-dialog modal-sm">
                  <div class="modal-content">
                    <div class="modal-header">
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                      <h4 class="modal-title" id="myModalLabel3">Ganti Password</h4>
                    </div>
                    <div class="modal-body">
																					<form action="{{route('anggota-profil-saya')}}" method="post" id="update">
																					@csrf
																					<input type="hidden" name="action" value="edit_password">
																						<div class="form-group">
																							<input type="password" name="ps_lm" class="form-control @error('ps_lm') is-invalid @enderror" placeholder="Password Lama" required autocomplete="new-password">
																							@error('ps_lm')
																								<p class="error">{{ $message }}</p>
																							@enderror
																						</div>
																						<div class="form-group">
																							<input type="password" name="password" class="form-control @error('password') is-invalid @enderror" placeholder="Password Baru" required autocomplete="new-password">
																							@error('password')
																									<p class="error">{{ $message }}</p>
																							@enderror
																						</div>
																						<div class="form-group">
																							<input type="password" name="password_confirmation" class="form-control" placeholder="Ulangi Password Baru" required autocomplete="new-password">
																						</div>
																					</form>
																					
																				</div>
                    <div class="modal-footer">
																						<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
																						<button type="button" class="btn btn-primary text-white" onclick="event.preventDefault();
                document.getElementById('update').submit();">Simpan</button>
                    </div>
                  </div>
                </div>
														</div>
                <!-- <p class="text-gray-silver">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Labore atque officiis maxime suscipit expedita obcaecati nulla in ducimus.</p> -->
                
                
              </div>
              <div class="col-xs-12 col-sm-8 col-md-12"><br>
              <div>
                <!-- Nav tabs -->
                <ul class="nav nav-tabs" role="tablist">
                  <li role="presentation" class="active"><a href="#keluarga" aria-controls="orders" role="tab" data-toggle="tab" class="font-15 text-uppercase">Keluarga</a></li>
                  <!-- <li role="presentation"> </li> -->
                  <a class="btn btn-success btn-xs pull-right" data-toggle="modal" data-target="#add">Tambah Keluarga</a>
                </ul>
              
                <!-- Tab panes -->
                <div class="tab-content">
                  <div role="tabpanel" class="tab-pane active" id="keluarga">
                 
                    <div class="table-responsive">
                      <table class="table table-hover">
                         <thead>
                            <tr>
                              <th>No.</th>
                               <th>Foto</th>
                               <th>Nama</th>
                               <th>No. HP</th>
                               <th>Tmpt,Tanggal Lahir</th>
                               <th>Status</th>
                               <th>Action</th>
                            </tr>
                         </thead>
                         <tbody>
                         <?php 
                         $datas = App\Keluarga::where('aktif','yes')->where('anggota_id',Auth::guard('anggota')->user()->id)->get();
                         ?>
                         @foreach($datas as $key => $data)
                            <tr>
                               <th scope="row" width="15">{{$key+1}}.</th>
                               <td> <img src="{{asset(config('katuumori-config')['keluarga_dir'].$data->foto)}}" alt="" width="50px"> </td>
                               <td>{{$data->nama}}</td>
                               <td>{{$data->no_hp}}</td>
                               <td>{{$data->tempat_lahir}}<br>{{date('d-m-Y',strtotime($data->tgl_lahir))}}</td>
                               <td>{{$data->statusId->name}}</td>
                               <td width="200"><a class="btn btn-primary btn-xs" data-toggle="modal" data-target="#detail{{$data->id}}">Detail</a> <a class="btn btn-warning btn-xs" data-toggle="modal" data-target="#edit{{$data->id}}">Edit</a> <a class="btn btn-danger btn-xs" href="{{route('hapus-keluarga-saya',$data->id)}}">Hapus</a></td>
                            </tr>
                          @endforeach
                         </tbody>
                      </table>
                    </div>
                  </div>
                  @foreach($datas as $key => $dataa)
                  <div class="modal fade" id="detail{{$dataa->id}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                    <div class="modal-dialog" role="document">
                      <div class="modal-content">
                        <div class="modal-header">
                          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                          <h4 class="modal-title" id="myModalLabel">Detail Keluarga</h4>
                        </div>
                        
                        <div class="modal-body col-md-12 bg-black-333 col-sm-4">

                          <div class="doctor-thumb col-md-4 row">
                            <img src="{{asset('images/keluarga/'.$dataa->foto)}}" class="align-center" alt=""><br>
                            <ul class="styled-icons icon-gray icon-circled icon-sm mt-15 mb-15">
                              <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                              <li><a href="#"><i class="fa fa-skype"></i></a></li>
                              <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                              <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                            </ul>
                          </div>
                          <div class="info p-10 bg-black-333 col-md-8">
                            <h4 class="text-uppercase text-white">{{$dataa->nama}}</h4>
                            <ul class="angle-double-right m-10">
                              @if($dataa->email != null)<li class="mt-0 text-gray-silver"><strong class="text-gray-lighter">Email</strong><br> {{$dataa->email}}</li>@endif
                              <li class="mt-0 text-gray-silver"><strong class="text-gray-lighter">Nomor HP</strong><br> {{$dataa->no_hp}}</li>
                              <li class="mt-0 text-gray-silver"><strong class="text-gray-lighter">Tanggal Lahir</strong><br> <?php 
                              $biday = new DateTime($dataa->tgl_lahir);
                              $today = new DateTime();
                              $diff = $today->diff($biday);
                              ?> {{date('d-m-Y',strtotime($dataa->tgl_lahir))}}  &nbsp;&nbsp;&nbsp;({{$diff->y}} Tahun)</li>
                              <li class="mt-0 text-gray-silver"><strong class="text-gray-lighter">Bidang Pekerjaan</strong><br> {{$dataa->pekerjaan}}</li>
                              <li class="mt-0 text-gray-silver"><strong class="text-gray-lighter">Alamat</strong><br> {{$dataa->alamat}}</li>
                            </ul>
                          </div>
                          
                        </div>
                        <div class="modal-footer">
                          <button type="button" class="btn btn-default" data-dismiss="modal">X</button>
                          <!-- <button type="submit" class="btn btn-primary text-white">Simpan</button> -->
                        </div>
                        </form>
                      </div>
                    </div>
                  </div>
                  @endforeach
                  <div class="modal fade" id="add" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                    <div class="modal-dialog" role="document">
                      <div class="modal-content">
                        <div class="modal-header">
                          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                          <h4 class="modal-title" id="myModalLabel">Tambah Keluarga</h4>
                        </div>
                        <form action="{{route('anggota-profil-saya')}}" method="post" id="proses" enctype="multipart/form-data">
                        <div class="modal-body">
                          
                            <input type="hidden" name="action" value="keluarga">
                          @csrf
                            <div class="row">
                              <div class="form-group col-md-6">
                                <label for="">Nama Lengkap</label>
                                <input type="text" name="nama" value="{{old('nama')}}" required class="form-control @error('nama') error @enderror">
                                @error('nama')
                                  <p class="error">{{ $message }}</p>
                                @enderror
                              </div>
                              <div class="form-group col-md-6">
                                <label for="">Nomor HP</label>
                                <input type="text" name="no_hp" value="{{old('no_hp')}}" required class="form-control @error('no_hp') error @enderror">
                                @error('no_hp')
                                  <p class="error">{{ $message }}</p>
                                @enderror
                              </div>
                              <div class="form-group col-md-6">
                                <label for="">Tempat Lahir</label>
                                <input type="text" name="tempat_lahir" value="{{old('tempat_lahir')}}" required class="form-control @error('tempat_lahir') error @enderror">
                                @error('tempat_lahir')
                                  <p class="error">{{ $message }}</p>
                                @enderror
                              </div>
      
                              <div class="form-group col-md-6">
                                <label for="">Jenis Kelamin</label>
                                <select name="jenis_kelamin" required id="" class="form-control @error('jenis_kelamin') error @enderror">
                                  <option value="">Pilih</option>
                                  @foreach(App\JenisKelamin::get() as $key => $jk)
                                    @if(old('jenis_kelamin') == $jk->id)
                                    <option value="{{$jk->id}}" selected>{{$jk->name}}</option>
                                    @else
                                    <option value="{{$jk->id}}">{{$jk->name}}</option>
                                    @endif
                                  @endforeach
                                </select>
                                @error('jenis_kelamin')
                                  <p class="error">{{ $message }}</p>
                                @enderror
                              </div>
                              <div  id="example-component">
                                <div class="form-group col-md-6" id="example-component">
                                <label>Tanggal Lahir</label><br>
                                    <!-- Datepicker Markup -->
                                    <div class="input-group date">
                                    <input type="text" class="form-control @error('tgl_lahir') error @enderror date-picker" name="tgl_lahir" value="{{old('tgl_lahir')}}">
                                    <div class="input-group-addon">
                                        <span class="glyphicon glyphicon-th"></span>
                                    </div>
                                    </div>
                                    @error('tgl_lahir')
                                      <p class="error">{{ $message }}</p>
                                    @enderror
                                    <!-- Datepicker Script -->
                                    <script type="text/javascript">
                                    $('#example-component .input-group.date').datepicker({
                                    });
                                    </script>
                                </div>
                                <div class="form-group col-md-6">
                                  <label>Foto</label>
                                  <input id="form_re_enter_password" name="foto"  class="form-control @error('foto') error @enderror" type="file"  value="{{old('foto')}}">
                                  @error('foto')
                                      <p class="error">{{ $message }}</p>
                                    @enderror
                                </div>
                              </div>
                              <div class="form-group col-md-6">
                                <label for="">Bidang Pekerjaan</label>
                                <input type="text" name="pekerjaan" value="{{old('pekerjaan')}}" class="form-control @error('pekerjaan') error @enderror">
                                @error('pekerjaan')
                                  <p class="error">{{ $message }}</p>
                                @enderror
                              </div>
                         
                              <div class="form-group col-md-6">
                                <label for="">Status Keluarga</label>
                                <select name="status_keluarga" value="{{old('status_keluarga')}}" required id="" class="form-control @error('status_keluarga') error @enderror">
                                  <option value="">Pilih</option>
                                  @foreach(App\StatusKeluarga::get() as $key => $data)
                                    @if(old('status_keluarga') == $data->id)
                                    <option value="{{$data->id}}" selected>{{$data->name}}</option>
                                    @else
                                    <option value="{{$data->id}}">{{$data->name}}</option>
                                    @endif
                                  @endforeach
                                </select>
                                @error('status_keluarga')
                                  <p class="error">{{ $message }}</p>
                                @enderror
                              </div>
                              <div class="form-group col-md-12">
                                <label for="">Alamat</label>
                                <textarea name="alamat" id="" cols="30" rows="4" class="form-control @error('alamat') error @enderror">{{old('alamat')}}</textarea>
                                @error('alamat')
                                  <p class="error">{{ $message }}</p>
                                @enderror
                              </div>
                            </div>
                          
                        </div>
                        <div class="modal-footer">
                          <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
                          <button type="submit" class="btn btn-primary text-white">Simpan</button>
                        </div>
                        </form>
                      </div>
                    </div>
                  </div>
                  @foreach($datas as $key => $dta)
                  <div class="modal fade" id="edit{{$dta->id}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                    <div class="modal-dialog" role="document">
                      <div class="modal-content">
                        <div class="modal-header">
                          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                          <h4 class="modal-title" id="myModalLabel">Edit Keluarga</h4>
                        </div>
                        <form action="{{route('anggota-profil-saya')}}" method="post" id="proses" enctype="multipart/form-data">
                        <div class="modal-body">
                          
                          <input type="hidden" name="action" value="edit_keluarga">
                          <input type="hidden" name="ids" value="{{$dta->id}}">
                          @csrf
                            <div class="row">
                              <div class="form-group col-md-6">
                                <label for="">Nama Lengkap</label>
                                <input type="text" name="nama" value="{{$dta->nama}}" required class="form-control @error('nama') error @enderror">
                                @error('nama')
                                  <p class="error">{{ $message }}</p>
                                @enderror
                              </div>
                              <div class="form-group col-md-6">
                                <label for="">Nomor HP</label>
                                <input type="text" name="no_hp" value="{{$dta->no_hp}}" required class="form-control @error('no_hp') error @enderror">
                                @error('no_hp')
                                  <p class="error">{{ $message }}</p>
                                @enderror
                              </div>
                              <div class="form-group col-md-6">
                                <label for="">Tempat Lahir</label>
                                <input type="text" name="tempat_lahir" value="{{$dta->tempat_lahir}}" required class="form-control @error('tempat_lahir') error @enderror">
                                @error('tempat_lahir')
                                  <p class="error">{{ $message }}</p>
                                @enderror
                              </div>
                              <div class="form-group col-md-6">
                                <label for="">Tanggal Lahir</label>
                                <input type="text" name="tgl_lahir" value="{{$dta->tgl_lahir}}" required class="form-control @error('tgl_lahir') error @enderror">
                                @error('tgl_lahir')
                                  <p class="error">{{ $message }}</p>
                                @enderror
                              </div>
                              <div class="form-group col-md-6">
                                <label for="">Jenis Kelamin</label>
                                <select name="jenis_kelamin" required id="" class="form-control @error('jenis_kelamin') error @enderror">
                                  <option value="">Pilih</option>
                                  @foreach(App\JenisKelamin::get() as $key => $jk)
                                    @if($jk->id == $dta->jenis_kelamin)
                                    <option value="{{$jk->id}}" selected>{{$jk->name}}</option>
                                    @else
                                    <option value="{{$jk->id}}">{{$jk->name}}</option>
                                    @endif
                                  @endforeach
                                </select>
                                @error('jenis_kelamin')
                                  <p class="error">{{ $message }}</p>
                                @enderror
                              </div>
                              <div class="form-group col-md-6">
                                <label for="">Bidang Pekerjaan</label>
                                <input type="text" name="pekerjaan" value="{{$dta->pekerjaan}}" class="form-control @error('pekerjaan') error @enderror">
                                @error('pekerjaan')
                                  <p class="error">{{ $message }}</p>
                                @enderror
                              </div>
                              <div class="form-group col-md-6">
                                <label for="">Foto</label>
                                <input type="file" name="foto" value="{{old('foto')}}" class="form-control @error('foto') error @enderror">
                                @error('foto')
                                  <p class="error">{{ $message }}</p>
                                @enderror
                              </div>
                              <div class="form-group col-md-6">
                                <label for="">Status Keluarga</label>
                                <select name="status_keluarga" required id="" class="form-control @error('status_keluarga') error @enderror">
                                  <option value="">Pilih</option>
                                  @foreach(App\StatusKeluarga::get() as $key => $data)
                                    @if($data->id == $dta->status_keluarga)
                                    <option value="{{$data->id}}" selected>{{$data->name}}</option>
                                    @else
                                    <option value="{{$data->id}}">{{$data->name}}</option>
                                    @endif
                                  @endforeach
                                </select>
                                @error('status_keluarga')
                                  <p class="error">{{ $message }}</p>
                                @enderror
                              </div>
                              <div class="form-group col-md-12">
                                <label for="">Alamat</label>
                                <textarea name="alamat" id="" cols="30" rows="4" class="form-control @error('alamat') error @enderror">{{$dta->alamat}}</textarea>
                                @error('alamat')
                                  <p class="error">{{ $message }}</p>
                                @enderror
                              </div>
                            </div>
                          
                        </div>
                        <div class="modal-footer">
                          <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
                          <button type="submit" class="btn btn-primary text-white">Simpan</button>
                        </div>
                        </form>
                      </div>
                    </div>
                  </div>
                  @endforeach
                  <div class="modal fade" id="editprofil" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                  <div class="modal-dialog" role="document">
                      <div class="modal-content">
                        <div class="modal-header">
                          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                          <h4 class="modal-title" id="myModalLabel">Edit Profil</h4>
                        </div>
                        <form action="{{route('anggota-profil-saya')}}" method="post" id="proses" enctype="multipart/form-data">
                        <div class="modal-body">
                          
                          <input type="hidden" name="action" value="edit_profil">
                          <input type="hidden" name="ids" value="{{Auth::guard('anggota')->user()->id}}">
                          @csrf
                            <div class="row">
                              <div class="form-group col-md-6">
                                <label for="">Nama Lengkap</label>
                                <input type="text" name="nama" value="{{Auth::guard('anggota')->user()->nama}}" required class="form-control @error('nama') error @enderror">
                                @error('nama')
                                  <p class="error">{{ $message }}</p>
                                @enderror
                              </div>
                              <div class="form-group col-md-6">
                                <label for="">Nomor HP</label>
                                <input type="text" name="no_hp" value="{{Auth::guard('anggota')->user()->no_hp}}" required class="form-control @error('no_hp') error @enderror">
                                @error('no_hp')
                                  <p class="error">{{ $message }}</p>
                                @enderror
                              </div>
                              <div class="form-group col-md-6">
                                <label for="">Jenis Kelamin</label>
                                <select name="jenis_kelamin" required id="" class="form-control @error('jenis_kelamin') error @enderror">
                                  <option value="">Pilih</option>
                                  @foreach(App\JenisKelamin::get() as $key => $jk)
                                    @if($jk->id == Auth::guard('anggota')->user()->jenis_kelamin_id)
                                    <option value="{{$jk->id}}" selected>{{$jk->name}}</option>
                                    @else
                                    <option value="{{$jk->id}}">{{$jk->name}}</option>
                                    @endif
                                  @endforeach
                                </select>
                                @error('jenis_kelamin')
                                  <p class="error">{{ $message }}</p>
                                @enderror
                              </div>
                              <div class="form-group col-md-6">
                                <label for="">Tempat Lahir</label>
                                <input type="text" name="tempat_lahir" value="{{Auth::guard('anggota')->user()->tempat_lahir}}" required class="form-control @error('tempat_lahir') error @enderror">
                                @error('tempat_lahir')
                                  <p class="error">{{ $message }}</p>
                                @enderror
                              </div>
                              
                              <div  id="example-component">
                                <div class="form-group col-md-6" id="example-component">
                                <label>Tanggal Lahir</label><br>
                                    <!-- Datepicker Markup -->
                                    <div class="input-group date">
                                    <input type="text" class="form-control @error('tgl_lahir') error @enderror date-picker" name="tgl_lahir" value="{{date('d-m-Y',strtotime(Auth::guard('anggota')->user()->tgl_lahir))}}">
                                    <div class="input-group-addon">
                                        <span class="glyphicon glyphicon-th"></span>
                                    </div>
                                    </div>
                                    @error('tgl_lahir')
                                      <p class="error">{{ $message }}</p>
                                    @enderror
                                    <!-- Datepicker Script -->
                                    <script type="text/javascript">
                                    $('#example-component .input-group.date').datepicker({
                                    });
                                    </script>
                                </div>
                                <div class="form-group col-md-6">
                                  <label>Foto</label>
                                  <input id="form_re_enter_password" name="foto"  class="form-control @error('foto') error @enderror" type="file"  value="{{old('foto')}}">
                                  @error('foto')
                                      <p class="error">{{ $message }}</p>
                                    @enderror
                                </div>
                              </div>
                              <div class="form-group col-md-6">
                                <label for="">Email</label>
                                <input type="text" name="email" value="{{Auth::guard('anggota')->user()->email}}" class="form-control @error('tgl_lahir') error @enderror">
                                @error('tgl_lahir')
                                  <p class="error">{{ $message }}</p>
                                @enderror
                              </div>
                              
                              <div class="form-group col-md-6">
                                <label for="">Bidang Pekerjaan</label>
                                <input type="text" name="pekerjaan" value="{{Auth::guard('anggota')->user()->pekerjaan}}" class="form-control @error('pekerjaan') error @enderror">
                                @error('pekerjaan')
                                  <p class="error">{{ $message }}</p>
                                @enderror
                              </div>
                              <div class="form-group col-md-12">
                                <label for="">Alamat</label>
                                <textarea name="alamat" id="" cols="30" rows="4" class="form-control @error('alamat') error @enderror">{{Auth::guard('anggota')->user()->alamat_lengkap}}</textarea>
                                @error('alamat')
                                  <p class="error">{{ $message }}</p>
                                @enderror
                              </div>
                            </div>
                          
                        </div>
                        <div class="modal-footer">
                          <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
                          <button type="submit" class="btn btn-primary text-white">Simpan</button>
                        </div>
                        </form>
                      </div>
                    </div>
                  </div>
                </div>
                <br>
              </div>
            </div>
            </div>
            
          </div>
        </div>
      </div>
    </section>

  </div>
@endsection
@section('js')
<script>
$('div.alert').not('.alert-important').delay(3000).fadeOut(350);
</script>
@endsection