@extends('layouts.app_summernote')
@section('css')
@endsection
@section('content')  
<div class="main-content">

    <!-- Section: inner-header -->
    <section id="pendek" class="inner-header divider parallax layer-overlay overlay-dark-5" data-bg-img="{{asset('images/slider/bg.jpg')}}">
      <div class="container pt-70 pb-20">
        <!-- Section Content -->
        <div class="section-content">
          <div class="row">
            <div class="col-md-12">
              <h2 class="title text-white">Form Berita</h2>
              <ol class="breadcrumb text-left text-black mt-10">
              <li><a href="{{url('/')}}">Beranda</a></li>
                <li class="active text-gray-silver">Kontak</li>
              </ol>
            </div>
          </div>
        </div>
      </div>
    </section>

    <!-- Divider: Google Map -->
    <section>
    <div class="container">
      <div class="container-fluid p-10">
        <div class="row">
        <div class="panel panel-success">
              <div class="panel-heading">
                <h3 class="panel-title">Form Berita</h3>
              </div>
              @include('flash::message')
              <form action="{{route('anggota-form-berita')}}" method="post" enctype="multipart/form-data">
              @csrf
              <div class="panel-body"> 
                  <div class="form-group col-md-4">
                    <label for="">Judul Berita</label>
                    <input type="text" class="form-control @error('judul') error @enderror" value="{{old('judul')}}" name="judul" required placeholder="Judul Berita">
                    @error('judul')
                      <p class="error">{{ $message }}</p>
                    @enderror
                  </div>
                  <div class="form-group col-md-4">
                    <label for="">Gambar</label>
                    <input type="file" class="form-control @error('gambar') error @enderror" value="{{old('gambar')}}" name="gambar" required placeholder="Gambar">
                    @error('gambar')
                      <p class="error">{{ $message }}</p>
                    @enderror
                  </div>
                  <div class="form-group col-md-4">
                    <label for="">Kategori</label>
                    <select name="kategori" id="" class="form-control @error('kategori') error @enderror">
                      <option value="">-- Pilih --</option>
                      @foreach(App\KategoriBerita::get() as $kt)
                        <option value="{{$kt->id}}">{{$kt->name}}</option>
                      @endforeach
                    </select>
                    @error('kategori')
                      <p class="error">{{ $message }}</p>
                    @enderror
                  </div>
                  <div class="form-group col-md-12">
                    <textarea class="form-control @error('isi') error @enderror" value="{{old('isi')}}" id="summernote" name="isi"></textarea>
                    @error('isi')
                      <p class="error">{{ $message }}</p>
                    @enderror
                  </div>
                  <div class="form-group col-md-12">
                    <button class="btn btn-primary form-control">Simpan Berita</button>
                  </div>
                
                </div>
              </form>
              
            </div>
        
        </div>
      </div>
      </div>
    </section>
  </div>
@endsection
@section('js')
@endsection