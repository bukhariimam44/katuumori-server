@extends('layouts.app')
@section('css')
		<meta name="_token" id="token" value="{{csrf_token()}}">
@endsection
@section('content')  
<div class="main-content" id="transaksi_pulsa">
    <!-- Section: inner-header -->
    <section id="pendek" class="inner-header divider parallax layer-overlay overlay-dark-5" :data-bg-img="images">
      <div class="container pt-70 pb-20">
        <!-- Section Content -->
        <div class="section-content">
          <div class="row">
            <div class="col-md-12">
              <h2 class="title text-white">Transaksi Pulsa</h2>
              <ol class="breadcrumb text-left text-black mt-10">
              <li><a href="{{url('/')}}">Beranda</a></li>
                <li class="active text-gray-silver">Transaksi Pulsa</li>
              </ol>
            </div>
          </div>
        </div>
      </div>
    </section>

    <!-- Divider: Google Map -->
    <section>
    <div class="container">
      <div class="container-fluid p-10">
        <div class="row">
        <div class="panel panel-success">
              <div class="panel-heading">
                <h3 class="panel-title">Form Transaksi Pulsa</h3>
              </div>
              @include('flash::message')
              <div class="panel-body"> 
                  <div class="form-group col-md-4">
                    <label for="">Provider</label>
                    <select v-model="selected" class="form-control" @change="load()">
                        <option v-for="option in options" v-bind:value="option.value">
                            @{{ option.text }}
                        </option>
                    </select>
                    @error('judul')
                      <p class="error">{{ $message }}</p>
                    @enderror
                  </div>
                  <div class="form-group col-md-4">
                    <label for="">Nimonal</label>
                    <select v-model="selected_2" class="form-control" @change="check()">
                        <option v-for="nominal in nominals" v-bind:value="nominal.code">
                            @{{ nominal.description }} = Rp @{{ formatPrice(nominal.price+nominal.markup) }}
                        </option>
                    </select>
                    @error('gambar')
                      <p class="error">{{ $message }}</p>
                    @enderror
                  </div>
                  <div class="form-group col-md-4">
                    <label for="">Nomor HP</label>
                    <input type="number" v-model="no_hp" name="no_hp" class="form-control">
                    @error('kategori')
                      <p class="error">{{ $message }}</p>
                    @enderror
                  </div>
                  <div class="form-group col-md-12">
                    <p>Komisi yang di dapat dari transaksi ini adalah Rp. @{{formatPrice(komisi)}}, dan akan dikumpulkan KATU'U MORI untuk disumbangkan kepada yang lebih membutuhkan.</p>
                  </div>
                  <div class="form-group col-md-12">
                    <button class="btn btn-primary form-control" @click="checkForm()">Proses</button>
                  </div>
                
                </div>              
            </div>
        
        </div>
      </div>
      </div>
    </section>
  </div>
@endsection
@section('js')
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script src="https://unpkg.com/axios/dist/axios.min.js"></script>
<script type="text/javascript" src="https://unpkg.com/vue@2.5.6/dist/vue.js"></script>
<script type="module">
  import Swal from 'https://unpkg.com/sweetalert/dist/sweetalert.min.js'
</script>
<script>
    var transaksi_pulsa = new Vue({
								el:'#transaksi_pulsa',
        data:{
            images : "<?php echo url('/images/slider/bg.jpg')?>",
            selected: 'S',
            options: [
                { text: 'Telkomsel', value: 'S' },
                { text: 'Indosat', value: 'I' },
                { text: 'XL', value: 'X' },
                { text: 'Smartfren', value: 'SM' },
                { text: 'Tree', value: 'T' },
                { text: 'Axis', value: 'AX' }
            ],
            selected_2 :'',
            nominals :[],
            no_hp:'',
            komisi :0,
            errors: [],
        },
        mounted() {
            this.load()
        },
        methods:{
            formatPrice(value) {
                // let val = (value/1).toFixed(0).replace('.', ',')
                return value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".")
            },
            async load(){
                let request = {provider : this.selected};
                request['token'] = document.querySelector('#token').getAttribute('value');
                console.log('LOG :'+JSON.stringify(request));
                console.log('LOG URL:'+window.url_operator);
                await axios.post(window.url_provider,request).then((response) =>{
                    console.log('Berhasil'+JSON.stringify(response.data));
                    this.nominals = response.data.data;
                    this.selected_2 = response.data.data[0].code;
																				this.komisi = response.data.data[0].markup
                    this.check()
                },(response)=>{
                    console.log('ERROR: '+response);
                });
            },
            async check(){
                let request = {code : this.selected_2};
                request['token'] = document.querySelector('#token').getAttribute('value');
                console.log('LOG :'+JSON.stringify(request));
                console.log('LOG URL:'+window.url_operator);
                await axios.post(window.url_operator,request).then((response) =>{
                    console.log('Berhasil'+JSON.stringify(response.data));
                    this.komisi = response.data.data.markup;
                },(response)=>{
                    console.log('ERROR: '+response);
                });
            },
           async checkForm() {
              if (this.selected && this.selected_2 && this.no_hp) {
                let request = {code : this.selected_2,no_hp : this.no_hp};
                request['token'] = document.querySelector('#token').getAttribute('value');
                await axios.post(window.url_proses,request).then((response) =>{
                    console.log('Berhasil'+JSON.stringify(response.data));
                    if (response.data.code === 200) {
                      swal({
                        title: "Transaksi Berhasil",
                        text: "Apakah ingin transaksi lagi ?",
                        icon: "warning",
                        buttons: ['Tidak','Ya'],
                        dangerMode: true,
                      })
                      .then((willDelete) => {
                        if (willDelete) {
                          swal("Terimakasih, Silahkan", {
                            icon: "success",
                          });
                        } else {
                          window.location.href = "<?php echo route('laporan-pulsa'); ?>";
                        }
                      });
                    }else{
                      swal("Transaksi Gagal!", response.data.message, "error");
                    }
                    
                },(response)=>{
                    console.log('ERROR: '+response);
                    swal("Gagal!", response, "error");
                });
                
              }
              
              this.errors = [];
              if (!this.selected) {
                swal("Cek Kembali!", "Provider harus dipilih!", "error");
              }
              if (!this.selected_2) {
                swal("Cek Kembali!", "Nominal harus dipilih!", "error");
              }
              if (this.no_hp.length < 10) {
                swal("Cek Kembali!", "Nomor HP Minimal 10 angka!", "error");
              }
            }

        }
    });
</script>
@endsection