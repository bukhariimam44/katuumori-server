@extends('layouts.app')
@section('css')
@endsection
@section('content')  
<div class="main-content">

    <!-- Section: inner-header -->
    <section id="pendek" class="inner-header divider parallax layer-overlay overlay-dark-5" data-bg-img="{{asset('images/slider/bg.jpg')}}">
      <div class="container pt-70 pb-20">
        <!-- Section Content -->
        <div class="section-content">
          <div class="row">
            <div class="col-md-12">
              <h2 class="title text-white">Detail Anggota</h2>
              <ol class="breadcrumb text-left text-black mt-10">
                <li><a href="#">Beranda</a></li>
                <li class="active text-gray-silver">Detail Anggota</li>
              </ol>
            </div>
          </div>
        </div>
      </div>
    </section>
      
    <!-- Section: Doctor Details -->
    <section class="">
      <div class="container">
        <div class="section-content">
          <div class="row">
            <div class="col-md-12 bg-black-333 col-sm-4"><br>
              <div class="doctor-thumb col-md-3 row">
                <img src="{{asset('images/anggota/'.$datas->foto)}}" class="align-center" alt=""><br>
                <ul class="styled-icons icon-gray icon-circled icon-sm mt-15 mb-15">
                  <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                  <li><a href="#"><i class="fa fa-skype"></i></a></li>
                  <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                  <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                </ul>
              </div>
              <div class="info p-0 bg-black-333 col-md-9">
                <h4 class="text-uppercase text-white">{{$datas->nama}}</h4>
                <ul class="angle-double-right m-10">
                  @if($datas->email != null)<li class="mt-0 text-gray-silver"><strong class="text-gray-lighter">Email</strong><br> {{$datas->email}}</li>@endif
                  <li class="mt-0 text-gray-silver"><strong class="text-gray-lighter">Nomor HP</strong><br> {{$datas->no_hp}}</li>
                  <li class="mt-0 text-gray-silver"><strong class="text-gray-lighter">Tempat,Tanggal Lahir</strong><br> <?php 
                  $biday = new DateTime($datas->tgl_lahir);
                  $today = new DateTime();
                  $diff = $today->diff($biday);
                  ?> {{$datas->tempat_lahir}}, {{date('d-m-Y',strtotime($datas->tgl_lahir))}}  &nbsp;&nbsp;&nbsp;({{$diff->y}} Tahun)</li>
                  <li class="mt-0 text-gray-silver"><strong class="text-gray-lighter">Pekerjaan</strong><br> {{$datas->pekerjaan}}</li>
                  <li class="mt-0 text-gray-silver"><strong class="text-gray-lighter">Alamat</strong><br> {{$datas->alamat_lengkap}}</li>
                </ul>  
              </div>
              <div class="col-xs-12 col-sm-8 col-md-12"><br>
              <div>
                <!-- Nav tabs -->
                <ul class="nav nav-tabs" role="tablist">
                  <li role="presentation" class="active"><a href="#keluarga" aria-controls="orders" role="tab" data-toggle="tab" class="font-15 text-uppercase">Keluarga</a></li>
                  <!-- <li role="presentation"> </li> -->
                </ul>
              
                <!-- Tab panes -->
                <div class="tab-content">
                  <div role="tabpanel" class="tab-pane active" id="keluarga">
                 
                    <div class="table-responsive">
                      <table class="table table-hover">
                         <thead>
                            <tr>
                              <th>No.</th>
                               <th>Foto</th>
                               <th>Nama</th>
                               <th>No. HP</th>
                               <th>Tmpt,Tanggal Lahir</th>
                               <th>Status</th>
                               <th>Action</th>
                            </tr>
                         </thead>
                         <tbody>
                         <?php 
                         $datas = App\Keluarga::where('aktif','yes')->where('anggota_id',$datas->id)->get();
                         ?>
                         @foreach($datas as $key => $data)
                            <tr>
                               <th scope="row" width="15">{{$key+1}}.</th>
                               <td> <img src="{{asset(config('katuumori-config')['keluarga_dir'].$data->foto)}}" alt="" width="50px" data-toggle="modal" data-target="#detail{{$data->id}}"> </td>
                               <td>{{$data->nama}}</td>
                               <td>{{$data->no_hp}}</td>
                               <td>{{$data->tempat_lahir}}<br>{{date('d-m-Y',strtotime($data->tgl_lahir))}}</td>
                               <td>{{$data->statusId->name}}</td>
                               <td width="50"><a class="btn btn-warning btn-xs" data-toggle="modal" data-target="#detail{{$data->id}}">Detail</a> </td>
                            </tr>
                          @endforeach
                         </tbody>
                      </table>
                    </div>
                  </div>
                  @foreach($datas as $key => $dataa)
                  <div class="modal fade" id="detail{{$dataa->id}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                    <div class="modal-dialog" role="document">
                      <div class="modal-content">
                        <div class="modal-header">
                          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                          <h4 class="modal-title" id="myModalLabel">Detail Keluarga</h4>
                        </div>
                        
                        <div class="modal-body col-md-12 bg-black-333 col-sm-4">

                          <div class="doctor-thumb col-md-4 row">
                            <img src="{{asset('images/keluarga/'.$dataa->foto)}}" class="align-center" alt=""><br>
                            <ul class="styled-icons icon-gray icon-circled icon-sm mt-15 mb-15">
                              <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                              <li><a href="#"><i class="fa fa-skype"></i></a></li>
                              <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                              <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                            </ul>
                          </div>
                          <div class="info p-10 bg-black-333 col-md-8">
                            <h4 class="text-uppercase text-white">{{$dataa->nama}}</h4>
                            <ul class="angle-double-right m-10">
                              @if($dataa->email != null)<li class="mt-0 text-gray-silver"><strong class="text-gray-lighter">Email</strong><br> {{$dataa->email}}</li>@endif
                              <li class="mt-0 text-gray-silver"><strong class="text-gray-lighter">Nomor HP</strong><br> {{$dataa->no_hp}}</li>
                              <li class="mt-0 text-gray-silver"><strong class="text-gray-lighter">Tanggal Lahir</strong><br> <?php 
                              $biday = new DateTime($dataa->tgl_lahir);
                              $today = new DateTime();
                              $diff = $today->diff($biday);
                              ?> {{date('d-m-Y',strtotime($dataa->tgl_lahir))}}  &nbsp;&nbsp;&nbsp;({{$diff->y}} Tahun)</li>
                              <li class="mt-0 text-gray-silver"><strong class="text-gray-lighter">Pekerjaan</strong><br> {{$dataa->pekerjaan}}</li>
                              <li class="mt-0 text-gray-silver"><strong class="text-gray-lighter">Alamat</strong><br> {{$dataa->alamat}}</li>
                            </ul>
                          </div>
                          
                        </div>
                        <div class="modal-footer">
                          <button type="button" class="btn btn-default" data-dismiss="modal">X</button>
                          <!-- <button type="submit" class="btn btn-primary text-white">Simpan</button> -->
                        </div>
                        </form>
                      </div>
                    </div>
                  </div>
                  @endforeach
              </div>
            </div>
            </div>
            
          </div>
        </div>
      </div>
    </section>

  </div>
@endsection