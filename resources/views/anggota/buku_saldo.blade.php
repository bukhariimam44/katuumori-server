@extends('layouts.app')
@section('css')
@endsection
@section('content')  
<div class="main-content">
    <!-- Section: inner-header -->
    <section id="pendek" class="inner-header divider parallax layer-overlay overlay-dark-5" data-bg-img="{{asset('images/slider/bg.jpg')}}">
      <div class="container pt-70 pb-20">
        <!-- Section Content -->
        <div class="section-content">
          <div class="row">
            <div class="col-md-12">
              <h2 class="title text-white">Buku Saldo</h2>
              <ol class="breadcrumb text-left text-black mt-10">
                <li><a href="#">Home</a></li>
                <li class="active text-gray-silver">Buku Saldo</li>
              </ol>
            </div>
          </div>
        </div>
      </div>
    </section>    
    <section>
      <div class="container">
 
        <div class="row">
          <div class="col-md-12 mt-0">
       
            <div data-example-id="striped-table" class="bs-example"> 
            <div class="panel panel-success">
              <div class="panel-heading">
                <h3 class="panel-title">Buku Saldo</h3>
              </div>
              <div class="panel-body">
                <div class="table-responsive">
                <table class="table table-striped"> 
                      <thead> 
                          <tr> 
                              <th>No.</th> 
                              <th>Tgl <br> No.Trx</th>
                              <th>Debet</th> 
                              <th>Kredit</th>
                              <th>Saldo</th> 
                              <th>Keterangan</th>
                            </tr> 
                        </thead> 
                        <tbody> 
                         @foreach($datas as $key=> $data)
                          <tr> 
                              <td>{{$key+1}}.</td> 
																														<td>{{$data->created_at}}<br>{{$data->no_trx}}</td>
																														@if($data->mutasi == 'Debet') 
																														<td>{{number_format($data->nominal)}}</td> 
																														<td>0</td>
																														@else
																														<td>0</td>
																														<td>{{number_format($data->nominal)}}</td> 
																														@endif
																														<td> <font @if($key == 0) style="color:red;font-weight:bold;" @endif> {{number_format($data->saldo_akhir)}}</font></td> 
                              <td>{{$data->keterangan}}</td> 
																										</tr>
																										@endforeach
                        </tbody> 
                    </table>
                </div>
              
              </div>
            </div>
                  
                </div> 
          </div>
        </div>
      </div>
    </section> 
  </div>
@endsection