@extends('layouts.app')
@section('css')
@endsection
@section('content')  
<div class="main-content">
    <!-- Section: inner-header -->
    <section id="pendek" class="inner-header divider parallax layer-overlay overlay-dark-5" data-bg-img="{{asset('images/slider/bg.jpg')}}">
      <div class="container pt-70 pb-20">
        <!-- Section Content -->
        <div class="section-content">
          <div class="row">
            <div class="col-md-12">
              <h2 class="title text-white">Data Anggota Arisan</h2>
              <ol class="breadcrumb text-left text-black mt-10">
                <li><a href="#">Home</a></li>
                <li class="active text-gray-silver">Data Anggota Arisan</li>
              </ol>
            </div>
          </div>
        </div>
      </div>
    </section>    
    <section>
      <div class="container">
 
        <div class="row">
          <div class="col-md-12 mt-0">
       
            <div data-example-id="striped-table" class="bs-example"> 
            <div class="panel panel-success">
              <div class="panel-heading">
																@if(Auth::guard('anggota')->check())
																	@if(Auth::guard('anggota')->user()->sebagai == 'pengurus')
																	<button class="btn btn-dark btn-theme-colored btn-sm pull-right" data-toggle="modal" data-target="#myModal2"><i class="fa fa-plus"></i> Tambah</button>
																	<div class="modal fade" id="myModal2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
																			<div class="modal-dialog" role="document">
																					<div class="modal-content">
																							<div class="modal-header">
																									<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
																									<h4 class="modal-title" id="myModalLabel">Tambah Anggota</h4>
																							</div>
																							@include('flash::message')
																							<form action="{{route('anggota-data-anggota')}}" method="post" enctype="multipart/form-data">
																							@csrf
																							<div class="modal-body">
																							<input type="hidden" name="action" value="add">
																							<div class="row">
																			<div class="form-group col-md-12">
																					<label for="form_name">Nama Lengkap</label>
																					<input name="nama" class="form-control @error('nama') error @enderror" type="text" value="{{old('nama')}}">
																					@error('nama')
																									<p class="error">{{ $message }}</p>
																							@enderror
																			</div>
																			<div class="form-group col-md-12">
																					<label>Nomor HP/WA</label>
																					<input name="no_hp" class="form-control @error('no_hp') error @enderror" type="text" value="{{old('no_hp')}}">
																					@error('no_hp')
																									<p class="error">{{ $message }}</p>
																							@enderror
																			</div>
																			<div class="form-group col-md-12">
																					<label>Status Arisan</label>
																					<select name="status" id="" class="form-control">
																						<option value="dapat">Sudah Dapat</option>
																						<option value="belum">Belum Dapat</option>
																					</select>
																					<!-- <input name="no_hp" class="form-control @error('no_hp') error @enderror" type="text" value="{{old('no_hp')}}"> -->
																					@error('status')
																									<p class="error">{{ $message }}</p>
																							@enderror
																			</div>
																	</div>
																	
																	<div class="row">
																			<div class="form-group col-md-12">
																					<label>Foto</label>
																					<input id="form_re_enter_password" name="foto"  class="form-control @error('foto') error @enderror" type="file"  value="{{old('foto')}}">
																					@error('foto')
																									<p class="error">{{ $message }}</p>
																							@enderror
																			</div>
																	</div>
																									
																							</div>
																							<div class="modal-footer">
																									<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
																									<button type="submit" class="btn btn-primary text-white">S I M P A N</button>
																							</div>
																							</form>
																					</div>
																			</div>
																	</div>
																	@endif
																@endif
                <h3 class="panel-title">Data Anggota</h3> 
              </div>
              <div class="panel-body">
                <div class="table-responsive">
																@include('flash::message')
                <table class="table table-striped"> 
                      <thead> 
                          <tr> 
                              <th>No.</th> 
                              <th>Foto</th>
                              <th>Nama</th> 
                              <th>No. HP</th> 
																														<th> Status Arisan</th>
																														@if(Auth::guard('anggota')->check())
																														@if(Auth::guard('anggota')->user()->sebagai == 'pengurus')
																														<th>Klik</th>
																														@endif
																														@endif
                            </tr> 
                        </thead> 
                        <tbody> 
                          @foreach($datas as $key => $data)
                          
                            <tr> 
                                <th width="5px" scope="row">{{$key+1}}.</th> 
																																	
                                <td><img src="{{asset('images/anggota/'.$data->foto)}}" width="40px" alt=""></td>
                                <td>{{$data->nama}}</td> 
                                <td>{{$data->hp}}</td> 
																																<td> @if($data->status == 'dapat') <label class="btn btn-success">Sudah Dapat</label> @else  <button class="btn btn-warning">Belum Dapat</button> @endif</td> 
																																@if(Auth::guard('anggota')->check())
																																@if(Auth::guard('anggota')->user()->sebagai == 'pengurus')
																																<td width="200px"> <button class="btn btn-primary" data-toggle="modal" data-target="#edit{{$data->id}}">Edit</button> <button class="btn btn-danger" data-toggle="modal" data-target=".hapus{{$data->id}}">Hapus</button></td>
																																@endif
																																@endif
                            </tr> 
                            @endforeach
                            
                        </tbody> 
                    </table>
                </div>
              
              </div>
            </div>
                  
                </div> 
          </div>
        </div>
      </div>
				</section> 
				<?php $statuses = [
						[
							'text'=>'Sudah Dapat',
							'value'=>'dapat'
						],
						[
							'text'=>'Belum Dapat',
							'value'=>'belum'
						]
				];?>
				@foreach($datas as $key => $dt)
				<div class="modal fade" id="edit{{$dt->id}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
																			<div class="modal-dialog" role="document">
																					<div class="modal-content">
																							<div class="modal-header">
																									<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
																									<h4 class="modal-title" id="myModalLabel">Edit Anggota</h4>
																							</div>
																							@include('flash::message')
																							<form action="{{route('anggota-data-anggota')}}" method="post" enctype="multipart/form-data">
																							@csrf
																							<div class="modal-body">
																									<input type="hidden" name="action" value="edit">
																									<?php $en = \Illuminate\Support\Facades\Crypt::encrypt($dt->id);?>
																									<input type="hidden" name="kode" value="{{$en}}">
																							<div class="row">
																			<div class="form-group col-md-12">
																					<label for="form_name">Nama Lengkap</label>
																					<input name="nama" class="form-control @error('nama') error @enderror" type="text" value="{{$dt['nama']}}">
																					@error('nama')
																									<p class="error">{{ $message }}</p>
																							@enderror
																			</div>
																			<div class="form-group col-md-12">
																					<label>Nomor HP/WA</label>
																					<input name="no_hp" class="form-control @error('no_hp') error @enderror" type="text" value="{{$dt['hp']}}">
																					@error('no_hp')
																									<p class="error">{{ $message }}</p>
																							@enderror
																			</div>
																			<div class="form-group col-md-12">
																					<label>Status Arisan</label>
																					<select name="status" id="" class="form-control">
																						@foreach($statuses as $st)
																						@if($st['value'] == $dt->status)
																						<option value="{{$st['value']}}" selected>{{$st['text']}}</option>
																						@else
																						<option value="{{$st['value']}}">{{$st['text']}}</option>
																						@endif
																						@endforeach
																					</select>
																					<!-- <input name="no_hp" class="form-control @error('no_hp') error @enderror" type="text" value="{{old('no_hp')}}"> -->
																					@error('status')
																									<p class="error">{{ $message }}</p>
																							@enderror
																			</div>
																	</div>
																	
																	{{--<div class="row">
																			<div class="form-group col-md-12">
																					<label>Foto</label><br>
																					<img src="{{asset('images/anggota/'.$dt->foto)}}" width="40px" alt="">
																					<input id="form_re_enter_password" name="foto"  class="form-control @error('foto') error @enderror" type="file"  value="{{old('foto')}}">
																					@error('foto')
																									<p class="error">{{ $message }}</p>
																							@enderror
																			</div>
																	</div>--}}
																									
																							</div>
																							<div class="modal-footer">
																									<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
																									<button type="submit" class="btn btn-primary text-white">S I M P A N</button>
																							</div>
																							</form>
																					</div>
																			</div>
																	</div>

															<div class="modal fade hapus{{$dt->id}}" tabindex="-1" role="dialog">
                <div class="modal-dialog modal-sm">
                  <div class="modal-content">
                    <div class="modal-header">
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                      <h4 class="modal-title" id="myModalLabel3">Hapus Anggota</h4>
                    </div>
                    <div class="modal-body">
																					<form action="{{route('anggota-data-anggota')}}" method="post" id="hapus{{$dt->id}}">
																					@csrf
																					<input type="hidden" name="action" value="hapus">
																					<label for="">Yakin dihapus ?</label>
																					<?php $en = \Illuminate\Support\Facades\Crypt::encrypt($dt->id);?>
																					<input type="hidden" name="kode" value="{{$en}}">
																							
																					</form>
																					
																				</div>
                    <div class="modal-footer">
																						<button type="button" class="btn btn-default" data-dismiss="modal">Tidak</button>
																						<button type="button" class="btn btn-primary text-white" onclick="event.preventDefault();
                document.getElementById('hapus{{$dt->id}}').submit();">Ya</button>
                    </div>
                  </div>
                </div>
														</div>
				@endforeach
  </div>
@endsection