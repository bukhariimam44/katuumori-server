@extends('layouts.app')
@section('css')
		<meta name="_token" id="token" value="{{csrf_token()}}">
@endsection
@section('content')  
<div class="main-content" id="transaksi_pln_pra">
    <!-- Section: inner-header -->
    <section id="pendek" class="inner-header divider parallax layer-overlay overlay-dark-5" :data-bg-img="images">
      <div class="container pt-70 pb-20">
        <!-- Section Content -->
        <div class="section-content">
          <div class="row">
            <div class="col-md-12">
              <h2 class="title text-white">Transaksi Token Listrik</h2>
              <ol class="breadcrumb text-left text-black mt-10">
              <li><a href="{{url('/')}}">Beranda</a></li>
                <li class="active text-gray-silver">Token Listrik</li>
              </ol>
            </div>
          </div>
        </div>
      </div>
    </section>

    <!-- Divider: Google Map -->
    <section>
    <div class="container">
      <div class="container-fluid p-10">
        <div class="row">
        <div class="panel panel-success">
              <div class="panel-heading">
                <h3 class="panel-title">Form Token Listrik</h3>
              </div>
              @include('flash::message')
              <div class="panel-body"> 
                  <div class="form-group col-md-4">
                    <label for="">Provider</label>
                    <select v-model="selected" class="form-control" @change="check()">
                        <option v-for="option in options" v-bind:value="option.code">
                            @{{ option.description }} = Rp @{{ formatPrice(option.price+option.markup) }}
                        </option>
                    </select>
                  </div>
                  <div class="form-group col-md-4">
                    <label for="">Nomor HP</label>
                    <input type="number" v-model="no_hp" name="no_hp" class="form-control">
                  </div>
                  <div class="form-group col-md-4">
                    <label for="">Nomor Meteran</label>
                    <input type="number" v-model="no_meteran" name="no_meteran" class="form-control">
                  </div>
                  <div class="form-group col-md-12">
                    <p>Komisi yang di dapat dari transaksi ini adalah Rp. @{{formatPrice(komisi)}}, dan akan dikumpulkan KATU'U MORI untuk disumbangkan kepada yang lebih membutuhkan.</p>
                  </div>
                  <div class="form-group col-md-12">
                    <button class="btn btn-primary form-control" @click="checkForm()">Proses</button>
                  </div>
                
                </div>              
            </div>
        
        </div>
      </div>
      </div>
    </section>
  </div>
@endsection
@section('js')
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script src="https://unpkg.com/axios/dist/axios.min.js"></script>
<script type="text/javascript" src="https://unpkg.com/vue@2.5.6/dist/vue.js"></script>
<script type="module">
  import Swal from 'https://unpkg.com/sweetalert/dist/sweetalert.min.js'
</script>
<script>
    var data_product = new Vue({
        el:'#transaksi_pln_pra',
        data:{
            images : "<?php echo url('/images/slider/bg.jpg')?>",
												prov:'PLN',
												selected: '',
            options: [],
            no_hp :'',
            no_meteran:'',
            komisi :0,
            errors: [],
        },
        mounted() {
            this.load()
        },
        methods:{
            formatPrice(value) {
                // let val = (value/1).toFixed(0).replace('.', ',')
                return value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".")
            },
            async load(){
                let request = {provider : this.prov};
                request['token'] = document.querySelector('#token').getAttribute('value');
                await axios.post(window.url_provider,request).then((response) =>{
																				console.log('Berhasil'+JSON.stringify(response.data.data));
																				this.options = response.data.data
																				this.selected = response.data.data[0].code
																				this.komisi = response.data.data[0].markup
                    // this.check()
                },(response)=>{
                    console.log('ERROR: '+response);
                });
												},
												async check(){
																let request = {code : this.selected};
																console.log('SELEC = '+this.selected);
                request['token'] = document.querySelector('#token').getAttribute('value');
                await axios.post(window.url_komisi,request).then((response) =>{
																				console.log('Berhasil'+JSON.stringify(response.data.data));
																				this.komisi = response.data.data.markup
                },(response)=>{
                    console.log('ERROR: '+response);
                });
            },
           async checkForm() {
              if (this.selected && this.no_hp && this.no_meteran) {
                let request = {code : this.selected,no_hp : this.no_hp, no_meteran : this.no_meteran};
                request['token'] = document.querySelector('#token').getAttribute('value');
                await axios.post(window.url_proses,request).then((response) =>{
                    console.log('Berhasil'+JSON.stringify(response.data));
                    if (response.data.code === 200) {
                      swal({
                        title: "Transaksi Berhasil",
                        text: "Apakah ingin transaksi lagi ?",
                        icon: "warning",
                        buttons: ['Tidak','Ya'],
                        dangerMode: true,
                      })
                      .then((willDelete) => {
                        if (willDelete) {
                          swal("Terimakasih, Silahkan", {
                            icon: "success",
                          });
                        } else {
                          window.location.href = "<?php echo route('laporan-token-listrik'); ?>";
                        }
                      });
                    }else{
                      swal("Transaksi Gagal!", response.data.message, "error");
                    }
                    
                },(response)=>{
                    console.log('ERROR: '+response);
                    swal("Gagal!", response, "error");
                });
                
              }
              
              this.errors = [];
              if (!this.selected) {
                swal("Cek Kembali!", "Provider harus dipilih!", "error");
              }
              if (this.nomor_hp.length < 10) {
                swal("Cek Kembali!", "Nomor HP Minimal 10 angka!", "error");
              }
              if (this.no_hp.length < 10) {
                swal("Cek Kembali!", "Nomor Meteran Minimal 10 angka!", "error");
              }
            }

        }
    });
</script>
@endsection