@extends('layouts.app')
@section('css')
@endsection
@section('content')  
<div class="main-content">
    <!-- Section: inner-header -->
    <section id="pendek" class="inner-header divider parallax layer-overlay overlay-dark-5" data-bg-img="{{asset('images/slider/bg.jpg')}}">
      <div class="container pt-70 pb-20">
        <!-- Section Content -->
        <div class="section-content">
          <div class="row">
            <div class="col-md-12">
              <h2 class="title text-white">Sumbangan Terkumpul</h2>
              <ol class="breadcrumb text-left text-black mt-10">
                <li><a href="#">Home</a></li>
                <li class="active text-gray-silver">Sumbangan Terkumpul</li>
              </ol>
            </div>
          </div>
        </div>
      </div>
    </section>    
    <section>
      <div class="container">
 
        <div class="row">
          <div class="col-md-12 mt-0">
       
            <div data-example-id="striped-table" class="bs-example"> 
            <div class="panel panel-success">
              <div class="panel-heading">
                <h3 class="panel-title">Sumbangan Terkumpul</h3>
              </div>
              <div class="panel-body">
                <div class="table-responsive">
                <table class="table table-striped"> 
                      <thead> 
                          <tr> 
                              <th>No.</th> 
																														<th>Bulan </th>
																														<th>Jumlah</th>
                            </tr> 
                        </thead> 
                        <tbody> 
																									<?php $total= 0;?>
																									@foreach($datas as $key => $dt)
																									<?php $total+= $dt->total;?>
                          <tr> 
                              <td>{{$key+1}}.</td> 
																														<td>{{date('M Y', strtotime($dt->tahun_bulan))}}</td>
																														<td>Rp {{number_format($dt->total)}}</td>
																										</tr>
																										@endforeach
																										<tr>
																											<th colspan="2">Total Sumbangan Terkumpul</th>
																											<th>Rp {{number_format($total)}}</th>
																										</tr>
                        </tbody> 
                    </table>
                </div>
              
              </div>
            </div>
                  
                </div> 
          </div>
        </div>
      </div>
    </section> 
  </div>
@endsection