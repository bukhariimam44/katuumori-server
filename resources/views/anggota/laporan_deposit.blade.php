@extends('layouts.app')
@section('css')
@endsection
@section('content')  
<div class="main-content">
    <!-- Section: inner-header -->
    <section id="pendek" class="inner-header divider parallax layer-overlay overlay-dark-5" data-bg-img="{{asset('images/slider/bg.jpg')}}">
      <div class="container pt-70 pb-20">
        <!-- Section Content -->
        <div class="section-content">
          <div class="row">
            <div class="col-md-12">
              <h2 class="title text-white">Laporan Deposit Saldo</h2>
              <ol class="breadcrumb text-left text-black mt-10">
                <li><a href="#">Home</a></li>
                <li class="active text-gray-silver">Laporan Deposit Saldo</li>
              </ol>
            </div>
          </div>
        </div>
      </div>
    </section>    
    <section>
      <div class="container">
 
        <div class="row">
          <div class="col-md-12 mt-0">
       
            <div data-example-id="striped-table" class="bs-example"> 
            <div class="panel panel-success">
              <div class="panel-heading">
                <h3 class="panel-title">Laporan Deposit Saldo</h3>
              </div>
              <div class="panel-body">
                <div class="table-responsive">
                <table class="table table-striped"> 
                      <thead> 
                          <tr> 
                              <th>No.</th> 
                              <th>Tgl <br> No.Trx</th>
                              <th>Nominal Transfer</th> 
                              <th>Bank</th>
                              <th>Status</th> 
                            </tr> 
                        </thead> 
                        <tbody> 
                         @foreach($datas as $key => $data)
                          <tr> 
                              <td>{{$key+1}}.</td> 
                              <td>{{date('d-m-Y',strtotime($data->tgl_trx))}}<br>{{$data->no_trx}}</td> 
																														<td>Rp {{number_format($data->nominal)}}</td> 
																														<td>{{$data->bankId->nama}} <br> {{$data->bankId->no_rek}}</td> 
                              <td @if($data->status == 'berhasil') class="btn-success" @elseif($data->status == 'menunggu') class="btn-warning" @else class="btn-danger" @endif>{{$data->status}}</td> 
																										</tr>

                          @endforeach
                        </tbody> 
                    </table>
                </div>
              
              </div>
            </div>
                  
                </div> 
          </div>
        </div>
      </div>
    </section> 
  </div>
@endsection