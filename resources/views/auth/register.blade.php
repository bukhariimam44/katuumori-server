@extends('layouts.app_umum')
@section('css')
@endsection
@section('content')  
<div class="main-content">

    <!-- Section: inner-header -->
    <section id="pendek" class="inner-header divider parallax layer-overlay overlay-dark-5" data-bg-img="{{asset('images/slider/bg.jpg')}}">
      <div class="container pt-70 pb-20">
        <!-- Section Content -->
        <div class="section-content">
          <div class="row">
            <div class="col-md-12">
              <h2 class="title text-white">Daftar</h2>
              <ol class="breadcrumb text-left text-black mt-10">
              <li><a href="{{url('/')}}">Beranda</a></li>
                <li class="active text-gray-silver">Daftar</li>
              </ol>
            </div>
          </div>
        </div>
      </div>
    </section>

    <section>
      <div class="container">
        <div class="row">
        
          <div class="col-md-6 col-md-push-3">
          @include('flash::message')
            <form name="reg-form" action="{{route('daftar')}}" class="register-form" method="post" enctype="multipart/form-data">
            @csrf
            <!-- <div class="icon-box mb-0 p-0">
                <a href="#" class="icon icon-bordered icon-rounded icon-sm pull-left mb-0 mr-10">
                  <i class="pe-7s-users"></i>
                </a>
                <h4 class="text-gray pt-10 mt-0 mb-30">Don't have an Account? Register Now.</h4>
              </div>
              <hr> -->
              <!-- <p class="text-gray">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Excepturi id perspiciatis facilis nulla possimus quasi, amet qui. Ea rerum officia, aspernatur nulla neque nesciunt alias.</p> -->
              <div class="row">
                <div class="form-group col-md-6">
                  <label for="form_name">Nama Lengkap</label>
                  <input name="nama" class="form-control @error('nama') error @enderror" type="text" value="{{old('nama')}}">
                  @error('nama')
                      <p class="error">{{ $message }}</p>
                    @enderror
                </div>
                <div class="form-group col-md-6">
                  <label>Nomor HP/WA</label>
                  <input name="no_hp" class="form-control @error('no_hp') error @enderror" type="text" value="{{old('no_hp')}}">
                  @error('no_hp')
                      <p class="error">{{ $message }}</p>
                    @enderror
                </div>
              </div>
              <div class="row">
              <div class="form-group col-md-6">
                  <label for="form_name">Bidang Pekerjaan</label>
                  <input name="pekerjaan" class="form-control @error('pekerjaan') error @enderror" type="text" value="{{old('pekerjaan')}}">
                  @error('pekerjaan')
                      <p class="error">{{ $message }}</p>
                    @enderror
                </div>
                <div class="form-group col-md-6">
                  <label>Bekerja di-</label>
                  <input name="kerja_di" class="form-control @error('kerja_di') error @enderror" type="text" value="{{old('kerja_di')}}">
                  @error('kerja_di')
                      <p class="error">{{ $message }}</p>
                    @enderror
                </div>
              </div>
              <div class="row">
              <div class="form-group col-md-6">
                  <label for="form_name">Ikut Arisan ?</label>
                  <?php $arisans = ['Ya','Tidak'];?>
                  <select name="ikut_arisan" class="form-control @error('ikut_arisan') error @enderror" id="">
                    <option value="">-- Pilih --</option>
                    @foreach($arisans as $arisan)
                    @if($arisan == old('ikut_arisan'))
                    <option value="{{$arisan}}" selected>{{$arisan}}</option>
                    @else
                    <option value="{{$arisan}}">{{$arisan}}</option>
                    @endif
                    @endforeach
                  </select>
                  @error('ikut_arisan')
                      <p class="error">{{ $message }}</p>
                    @enderror
                </div>
                <div class="form-group col-md-6">
                  <label>Kata Sandi</label>
                  <input name="password" class="form-control @error('password') error @enderror" type="password" value="{{old('password')}}">
                  @error('password')
                      <p class="error">{{ $message }}</p>
                    @enderror
                </div>
              </div>
              <div class="row">
                <div class="form-group col-md-6">
                  <label for="form_name">Jenis Kelamin</label>
                  <!-- <input name="jenis_kelamin" class="form-control" type="text"> -->
                  <select name="jenis_kelamin" class="form-control @error('jenis_kelamin') error @enderror" id="" value="{{old('jenis_kelamin')}}">
                    <option value="">-- Pilih --</option>
                    @foreach($jenkels as $jenkel)
                    <option value="{{$jenkel->id}}">{{$jenkel->name}}</option>
                    @endforeach
                  </select>
                </div>
                <div class="form-group col-md-6">
                  <label>Tempat Lahir</label>
                  <input name="tempat_lahir" class="form-control @error('tempat_lahir') error @enderror" type="text" value="{{old('tempat_lahir')}}">
                  @error('tempat_lahir')
                      <p class="error">{{ $message }}</p>
                    @enderror
                </div>
              </div>
              <div class="row"  id="example-component">
                <div class="form-group col-md-6" id="example-component">
                <label>Tanggal Lahir</label><br>
                    <!-- Datepicker Markup -->
                    <div class="input-group date">
                    <input type="text" class="form-control @error('tgl_lahir') error @enderror date-picker" name="tgl_lahir" value="{{old('tgl_lahir')}}" placeholder="dd-mm-yyyy">
                    <div class="input-group-addon">
                        <span class="glyphicon glyphicon-th"></span>
                    </div>
                    </div>
                    @error('tgl_lahir')
                      <p class="error">{{ $message }}</p>
                    @enderror
                    <!-- Datepicker Script -->
                    <script type="text/javascript">
                    $('#example-component .input-group.date').datepicker({
                    });
                    </script>
                </div>
                <div class="form-group col-md-6">
                  <label>Foto Max : 1MB</label>
                  <input id="form_re_enter_password" name="foto"  class="form-control @error('foto') error @enderror" type="file"  value="{{old('foto')}}">
                  @error('foto')
                      <p class="error">{{ $message }}</p>
                    @enderror
                </div>
              </div>
              <div class="row">
                <div class="form-group col-md-12">
                  <label for="form_choose_username">Alamat Lengkap</label>
                  <textarea name="alamat" id="" cols="30" class="form-control @error('alamat') error @enderror" rows="6">{{old('alamat')}}</textarea>
                  @error('alamat')
                      <p class="error">{{ $message }}</p>
                    @enderror
                </div>
              </div>
              <div class="form-group">
                <button class="btn btn-dark btn-lg btn-block mt-15" type="submit">D A F T A R</button>
              </div>
            </form>
          </div>
        </div>
      </div>
    </section>
  </div>
@endsection
@section('js')
@endsection