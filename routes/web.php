<?php
Route::post('/callback/server', ['as'=>'callback-server','uses'=>'CallbackController@callback']);

Route::get('/masuk', ['as'=>'masuk','uses'=>'LoginController@getLogin'])->middleware('guest');
Route::post('/masuk', ['before' => 'auth','as'=>'masuk','uses'=>'LoginController@postLogin'])->middleware('guest');
Route::get('/daftar', ['as'=>'daftar','uses'=>'LoginController@getDaftar'])->middleware('guest');
Route::post('/daftar', ['as'=>'daftar','uses'=>'LoginController@postDaftar'])->middleware('guest');

Route::get('/', ['as'=>'index','uses'=>'LoginController@index']);
Route::get('/profil', ['as'=>'tentang','uses'=>'LoginController@tentang']);
Route::get('/admin-edit-profil', ['as'=>'admin-edit-profil','uses'=>'AdminController@edit_profil'])->middleware('auth:admin');
Route::post('/admin-edit-profil', ['as'=>'admin-edit-profil','uses'=>'AdminController@edit_profil'])->middleware('auth:admin');
Route::get('/visi-misi', ['as'=>'visi-misi','uses'=>'LoginController@visi_misi']);
Route::get('/admin-edit-visi-misi', ['as'=>'admin-edit-visi-misi','uses'=>'AdminController@edit_visi_misi'])->middleware('auth:admin');
Route::post('/admin-edit-visi-misi', ['as'=>'admin-edit-visi-misi','uses'=>'AdminController@edit_visi_misi'])->middleware('auth:admin');
//SHOP
Route::get('/shopping', ['as'=>'shopping','uses'=>'UmumController@index']);
Route::get('/shopping/produk', ['as'=>'produk','uses'=>'UmumController@produk']);
Route::get('/shopping/produk/{name}', ['as'=>'detail-produk','uses'=>'UmumController@detail_produk']);
Route::get('/shopping/blog', ['as'=>'blog','uses'=>'UmumController@blog']);
Route::get('/shopping/blog/{name}', ['as'=>'detail-blog','uses'=>'UmumController@detail_blog']);
//
Route::get('/galleri', ['as'=>'galleri','uses'=>'LoginController@galleri']);
Route::get('/berita', ['as'=>'berita','uses'=>'LoginController@berita']);
Route::get('/berita/{judul}', ['as'=>'detail-berita','uses'=>'LoginController@detail_berita']);
Route::post('/berita/{judul}', ['as'=>'detail-berita','uses'=>'LoginController@tambah_komntar']);
Route::get('/kontak', ['as'=>'kontak','uses'=>'LoginController@kontak']);
Route::get('/belanja-{id}', ['as'=>'belanja','uses'=>'LoginController@belanja']);
Route::get('/detail-product-{id}', ['as'=>'umum-detail-product','uses'=>'LoginController@detail_product']);
Route::post('/detail-product-{id}', ['as'=>'add-to-cart','uses'=>'LoginController@add_cart']);
Route::get('/keranjang', ['as'=>'keranjang','uses'=>'LoginController@keranjang']);

Route::get('/cek-ongkir', ['as'=>'cek-ongkir','uses'=>'LoginController@cek_ongkir']);
Route::get('/propinsi', ['as'=>'propinsi','uses'=>'LoginController@propinsi']);
Route::post('/propinsi', ['as'=>'propinsi','uses'=>'LoginController@propinsi']);
Route::get('/city', ['as'=>'city','uses'=>'LoginController@city']);
Route::post('/city', ['as'=>'city','uses'=>'LoginController@city']);


Auth::routes();
//USER
Route::get('/user', ['as'=>'user','uses'=>'UserController@index'])->middleware('auth:user');

//ROTE ADMIN
Route::get('/admin', ['as'=>'admin','uses'=>'AdminController@index'])->middleware('auth:admin');
Route::get('/admin/data-anggota-{ikut}', ['as'=>'admin-data-anggota','uses'=>'AdminController@data_anggota'])->middleware('auth:admin');
Route::post('/admin/data-anggota', ['as'=>'admin-cari-data-anggota','uses'=>'AdminController@tambah_anggota'])->middleware('auth:admin');
Route::post('/galleri', ['as'=>'tambah-galleri','uses'=>'AdminController@galleri'])->middleware('auth:admin');
Route::get('/admin/profil-saya', ['as'=>'admin-profil-saya','uses'=>'AdminController@profil_saya'])->middleware('auth:admin');
Route::get('/admin/data-harga-{id}', ['as'=>'admin-data-harga','uses'=>'AdminController@data_harga'])->middleware('auth:admin');
Route::post('/admin/data-harga-{id}', ['as'=>'admin-data-harga','uses'=>'AdminController@data_harga'])->middleware('auth:admin');
Route::get('/product-toko-online', ['as'=>'product-toko-online','uses'=>'AdminController@toko_online'])->middleware('auth:admin');
Route::get('/product-toko-online-{id}', ['as'=>'detail-product','uses'=>'AdminController@detail_product'])->middleware('auth:admin');
Route::get('/admin/permintaan-topup', ['as'=>'admin-permintaan-topup','uses'=>'AdminController@permintaan_topup'])->middleware('auth:admin');
Route::post('/admin/permintaan-topup', ['as'=>'admin-permintaan-topup','uses'=>'AdminController@permintaan_topup'])->middleware('auth:admin');
Route::post('/admin/proses-topup', ['as'=>'admin-proses-topup','uses'=>'AdminController@proses_topup'])->middleware('auth:admin');
Route::post('/admin/batalkan-topup', ['as'=>'admin-batalkan-topup','uses'=>'AdminController@batalkan_topup'])->middleware('auth:admin');
Route::get('/admin/pengaturan', ['as'=>'admin-pengaturan','uses'=>'AdminController@pengaturan'])->middleware('auth:admin');
Route::get('/admin/topup-supplier', ['as'=>'admin-topup-supplier','uses'=>'AdminController@topup_supplier'])->middleware('auth:admin');
Route::get('/admin/bank', ['as'=>'admin-bank','uses'=>'AdminController@bank'])->middleware('auth:admin');
Route::post('/admin/proses-deposit', ['as'=>'admin-proses-deposit','uses'=>'AdminController@proses_deposit'])->middleware('auth:admin');
Route::get('/admin/buku-saldo-supplier', ['as'=>'admin-buku-saldo-supplier','uses'=>'AdminController@buku_saldo_supplier'])->middleware('auth:admin');

Route::get('/admin/topup-manual', ['as'=>'admin-topup-manual','uses'=>'AdminController@topup_manual'])->middleware('auth:admin');
Route::post('/admin/topup-manual', ['as'=>'admin-topup-manual','uses'=>'AdminController@topup_manual'])->middleware('auth:admin');

//ROUTE ANGGOTA
Route::get('/anggota', ['as'=>'anggota','uses'=>'AnggotaController@index'])->middleware('auth:anggota');
Route::get('/anggota/profil-saya', ['as'=>'anggota-profil-saya','uses'=>'AnggotaController@profil_saya'])->middleware('auth:anggota');
Route::post('/anggota/profil-saya', ['as'=>'anggota-profil-saya','uses'=>'AnggotaController@profil_saya'])->middleware('auth:anggota');
Route::get('/anggota/ganti-katasandi', ['as'=>'anggota-ganti-kata-sandi','uses'=>'AnggotaController@ganti_password'])->middleware('auth:anggota');
Route::get('/anggota/hapus-keluarga-saya/{id}', ['as'=>'hapus-keluarga-saya','uses'=>'AnggotaController@hapus_keluarga'])->middleware('auth:anggota');
Route::get('/anggota/like-berita/{id}', ['as'=>'like','uses'=>'AnggotaController@like'])->middleware('auth:anggota');
Route::get('/anggota/data-anggota-arisan', ['as'=>'anggota-data-anggota','uses'=>'AnggotaController@data_anggota_arisan'])->middleware('auth:anggota');
Route::post('/anggota/data-anggota-arisan', ['as'=>'anggota-data-anggota','uses'=>'PengurusController@data_anggota_arisan'])->middleware('auth:anggota');
Route::get('/anggota/data-anggota/{id}', ['as'=>'anggota-detail-anggota','uses'=>'AnggotaController@detail_anggota'])->middleware('auth:anggota');
Route::get('/anggota/form-berita', ['as'=>'anggota-form-berita','uses'=>'AnggotaController@form_berita'])->middleware('auth:anggota');
Route::post('/anggota/form-berita', ['as'=>'anggota-form-berita','uses'=>'AnggotaController@form_berita'])->middleware('auth:anggota');
Route::post('/anggota/berita/{judul}', ['as'=>'anggota-detail-berita','uses'=>'AnggotaController@tambah_komentar'])->middleware('auth:anggota');
Route::post('/anggota/galleri', ['as'=>'pengurus-tambah-galleri','uses'=>'PengurusController@gallery'])->middleware('auth:anggota');

//TRANSAKSI ANGGOTA
Route::get('/anggota/transaksi-pulsa', ['as'=>'transaksi-pulsa','uses'=>'AnggotaController@transaksi_pulsa'])->middleware('auth:anggota');
Route::post('/provider', ['as'=>'provider','uses'=>'AnggotaController@provider'])->middleware('auth:anggota');
Route::post('/operator', ['as'=>'operator','uses'=>'AnggotaController@operator'])->middleware('auth:anggota');
Route::post('/proses', ['as'=>'proses','uses'=>'AnggotaController@proses'])->middleware('auth:anggota');
Route::get('/anggota/laporan-pulsa', ['as'=>'laporan-pulsa','uses'=>'AnggotaController@laporan_pulsa'])->middleware('auth:anggota');
Route::get('/anggota/transaksi-pln-pra', ['as'=>'transaksi-pln-pra','uses'=>'AnggotaController@transaksi_pln_pra'])->middleware('auth:anggota');
Route::get('/anggota/laporan-token-listrik', ['as'=>'laporan-token-listrik','uses'=>'AnggotaController@laporan_token_listrik'])->middleware('auth:anggota');
Route::get('/anggota/laporan-deposit', ['as'=>'anggota-laporan-deposit','uses'=>'AnggotaController@laporan_deposit'])->middleware('auth:anggota');

Route::post('/anggota/cek-status-ppob', ['as'=>'anggota-cek-status-ppob','uses'=>'AnggotaController@cek_status_ppob'])->middleware('auth:anggota');
Route::post('/provider-pln', ['as'=>'provider-pln','uses'=>'AnggotaController@provider_pln'])->middleware('auth:anggota');
Route::post('/komisi-pln', ['as'=>'komisi-pln','uses'=>'AnggotaController@komisi'])->middleware('auth:anggota');
Route::post('/proses-pln', ['as'=>'proses-pln','uses'=>'AnggotaController@proses_pln'])->middleware('auth:anggota');

//DEPOSIT
Route::get('/anggota/bank', ['as'=>'anggota-bank','uses'=>'AnggotaController@bank'])->middleware('auth:anggota');
Route::post('/anggota/proses-deposit', ['as'=>'proses-deposit','uses'=>'AnggotaController@proses_deposit'])->middleware('auth:anggota');
Route::get('/anggota/isi-saldo', ['as'=>'isi-saldo','uses'=>'AnggotaController@isi_saldo'])->middleware('auth:anggota');
Route::get('/anggota/buku-saldo', ['as'=>'anggota-buku-saldo','uses'=>'AnggotaController@buku_saldo'])->middleware('auth:anggota');
Route::get('/anggota/sumbangan-terkumpul', ['as'=>'anggota-sumbangan-terkumpul','uses'=>'AnggotaController@sumbangan_terkumpul'])->middleware('auth:anggota');
//Akun
Route::get('/anggota/toko-saya', ['as'=>'anggota-toko-saya','uses'=>'AnggotaController@toko_saya'])->middleware('auth:anggota');
Route::get('/anggota/toko-saya/{id}', ['as'=>'anggota-detail-toko-saya','uses'=>'AnggotaController@detail_toko_saya'])->middleware('auth:anggota');
Route::get('/anggota/panduan-transaksi', ['as'=>'anggota-panduan-transaksi','uses'=>'AnggotaController@panduan_transaksi'])->middleware('auth:anggota');

//KELUAR
Route::get('/logout/{guards}', 'LoginController@logout');

